﻿using System;

namespace SyncBD.Controladores
{
    public class Ajustes
    {
        public bool DiferenteSincronizacion { get; set; }
        public bool MinimizarAlIniciar { get; set; }
        public bool IniciarConWindows { get; set; }
        public int TiempoReConexion { get; set; }
        public int TiempoDesconexion { get; set; }
        public string CopiarA { get; set; }
        public bool RutaGuardadoPermanente { get; set; }
        public string Apariencia { get; set; }
        public string Idioma { get; set; }
    }

    public class Remoto
    {
        public string Servidor { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public string Encriptacion { get; set; }
        public string Puerto { get; set; }
        public string Directorio { get; set; }
        public string Anonimo { get; set; }
        public string Modo { get; set; }
        public string Autenticar { get; set; }
        public string Combinado { get; set; }
        public string Protocolo { get; set; }
    }

    public class Local
    {
        public string Servidor { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public string Encriptacion { get; set; }
        public string Puerto { get; set; }
        public string Directorio { get; set; }
        public string Anonimo { get; set; }
        public string Modo { get; set; }
        public string Autenticar { get; set; }
        public string Combinado { get; set; }
        public string Protocolo { get; set; }
    }

    public class Servidores
    {
        public Remoto Remoto { get; set; }
        public Local Local { get; set; }
    }

    public class MovimientoRemoto
    {
        public int Movimiento { get; set; }
        public DateTime Fecha { get; set; }
        public int Archivos { get; set; }
        public string Desde { get; set; }
        public string Hacia { get; set; }
        public string Duracion { get; set; }
        public string Estado { get; set; }
    }

    public class MovimientoLocal
    {
        public int Movimiento { get; set; }
        public DateTime Fecha { get; set; }
        public int Archivos { get; set; }
        public string Desde { get; set; }
        public string Hacia { get; set; }
        public string Duracion { get; set; }
        public string Estado { get; set; }
    }
}
