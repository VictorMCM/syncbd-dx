﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using WinSCP;
using XML = System.Xml;
using SevenZip;

namespace SyncBD.Controladores
{
    class Nucleo
    {
        //Ruta que indica la carpeta donde se ubican los temas para la aplicacion
        private readonly string ajustesTemas = string.Concat(Application.StartupPath, @"\Apariencia");
        //Ruta que indica la direccion del archivo de host conocidos
        private readonly string dirHostsConocidos = string.Concat(Application.StartupPath, @"\General\Hosts.xml");
        //Ruta que indica la carpeta donde se ubican los idiomas para la aplicacion
        private readonly string idiomas = string.Concat(Application.StartupPath, @"\Idiomas");
        //Ruta que indica la carpeta donde se guardan los logs de los servidores
        private readonly string registros = string.Concat(Application.StartupPath, @"\Registros");
        //Ruta que indica la carpeta donde se guardan los ajustes y demás de la aplicacion
        private readonly string general = string.Concat(Application.StartupPath, @"\General");
        //Ruta que indica la direccion del archivo donde se guardan los ajustes de la aplicacion
        private readonly string ajustesJSON = string.Concat(Application.StartupPath, @"\General", @"\ajustes.json");
        //Ruta que indica la direccion del archivo donde se guardan los ajustes de la aplicacion
        private readonly string ajustesServidores = string.Concat(Application.StartupPath, @"\General", @"\servidoresConfig.json");
        //Ruta que indica la direccion del archivo donde se guardan los registros del servidor remoto
        public static readonly string logRemoto = string.Concat(Application.StartupPath, @"\Registros", @"\logRemoto.txt");
        //Ruta que indica la direccion del archivo donde se guardan los registros del servidor local
        public static readonly string logLocal = string.Concat(Application.StartupPath, @"\Registros", @"\logLocal.txt");
        //Ruta predefinida que indica donde realizar la copia de archivos sincronizados
        private readonly string dirCopiado = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"\SyncBD");
        //Ruta temporal donde se almacenan los archivos y carpetas a comprimir para soporte técnico
        private readonly string dirSoporte = string.Concat(Application.StartupPath, @"\SoporteTemp");
        //Ruta que indica la direccion del archivo PDF del manual para el usuario
        public readonly string manualUsuario = string.Concat(Application.StartupPath, @"\Manual_para_el_usuario.pdf");

        Seguridad seguridad = new Seguridad();

        public string ObtenerVersionSO()
        {
            try
            {
                 string windows = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName", null).ToString();

                if (!string.IsNullOrWhiteSpace(windows) || !string.IsNullOrEmpty(windows))
                {
                    return windows.ToUpper();
                }

                else
                {
                    return "Windows XP".ToUpper();
                }
            }
            catch (Exception)
            {
                return "Windows XP".ToUpper();
            }
        }

        public void ComprobarGeneral()
        {
            if (!Directory.Exists(general))
            {
                Directory.CreateDirectory(general);
            }

            if (!Directory.Exists(dirCopiado))
            {
                Directory.CreateDirectory(dirCopiado);
            }

            if (!File.Exists(ajustesJSON))
            {
                using (File.Create(ajustesJSON)) { }

                Ajustes ajustes = new Ajustes { CopiarA = dirCopiado, TiempoDesconexion = 9999000, TiempoReConexion = 3000 };

                string datos = JsonConvert.SerializeObject(ajustes, Formatting.Indented);

                File.WriteAllText(ajustesJSON, datos);
            }

            if (!File.Exists(ajustesServidores))
            {
                using (File.Create(ajustesServidores))
                {

                }
            }
        }

        public void ComprobarRegistros()
        {
            if (!Directory.Exists(registros))
            {
                Directory.CreateDirectory(registros);
            }
        }

        public string ComprobarManual()
        {
            if (!File.Exists(manualUsuario))
            {
                return null;
            }

            else
            {
                return manualUsuario;
            }
        }

        public bool ComprobarArchivosSoporte()
        {
            try
            {
                Directory.CreateDirectory(dirSoporte);

                string dirHistorial = string.Concat(Application.StartupPath, @"\General", @"\Historial.db");

                if (File.Exists(logRemoto))
                {
                    File.Copy(logRemoto, string.Concat(dirSoporte, @"\", Path.GetFileName(logRemoto)), true);
                }

                if (File.Exists(logLocal))
                {
                    File.Copy(logLocal, string.Concat(dirSoporte, @"\", Path.GetFileName(logLocal)), true);
                }

                if (File.Exists(ajustesJSON))
                {
                    File.Copy(ajustesJSON, string.Concat(dirSoporte, @"\", Path.GetFileName(ajustesJSON)), true);
                }

                if (File.Exists(ajustesServidores))
                {
                    File.Copy(ajustesServidores, string.Concat(dirSoporte, @"\", Path.GetFileName(ajustesServidores)), true);
                }

                if (File.Exists(dirHistorial))
                {
                    File.Copy(dirHistorial, string.Concat(dirSoporte, @"\", Path.GetFileName(dirHistorial)), true);
                }

                if (Directory.Exists(string.Concat(Application.StartupPath, @"\Log")))
                {
                    string dirLogSoporte = string.Concat(dirSoporte, @"\Log");

                    Directory.CreateDirectory(dirLogSoporte);

                    string[] logs = Directory.GetFiles(string.Concat(Application.StartupPath, @"\Log"));
                    
                    foreach (string log in logs)
                    {
                        File.Copy(log, string.Concat(dirLogSoporte, @"\", Path.GetFileName(log)), true);
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CrearArchivoSoporte(string pathDestino)
        {
            try
            {
                if (ComprobarArchivosSoporte())
                {
                    if (Directory.Exists(dirSoporte))
                    {
                        if (Environment.Is64BitProcess)
                        {
                            SevenZipCompressor.SetLibraryPath(string.Concat(Application.StartupPath, @"\7zax64.dll"));
                        }

                        else
                        {
                            SevenZipCompressor.SetLibraryPath(string.Concat(Application.StartupPath, @"\7za.dll"));
                        }

                        SevenZipCompressor compresor = new SevenZipCompressor();
                        compresor.CompressionLevel = CompressionLevel.Ultra;
                        compresor.CompressionMethod = CompressionMethod.Lzma;

                        compresor.CompressDirectory(dirSoporte, string.Concat(pathDestino, @"\SyncBD_", DateTime.Now.ToString("yyyyMMddhhmmss"), "_Soporte.7z"), "SyncBD-SolucionesDigitales");

                        Directory.Delete(dirSoporte, true);

                        return true;
                    }

                    else
                    {
                        return false;
                    }
                }

                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string DeterminarTema(string tema)
        {
            try
            {
                string temaSeleccionado = "The Bezier";

                switch (tema)
                {
                    case "Windows 7 Clásico":
                        temaSeleccionado = "Seven Classic";
                        break;
                    case "Office 2010 Azul":
                        temaSeleccionado = "Office 2010 Blue";
                        break;
                    case "Office 2010 Oscuro":
                        temaSeleccionado = "Office 2010 Black";
                        break;
                    case "Office 2010 Plateado":
                        temaSeleccionado = "Office 2010 Silver";
                        break;
                    case "Office 2013":
                        temaSeleccionado = "Office 2013";
                        break;
                    case "Office 2013 Gris Oscuro":
                        temaSeleccionado = "Office 2013 Dark Gray";
                        break;
                    case "Office 2013 Gris Claro":
                        temaSeleccionado = "Office 2013 Light Gray";
                        break;
                    case "Office 2016 Colorido":
                        temaSeleccionado = "Office 2016 Colorful";
                        break;
                    case "Office 2016 Oscuro":
                        temaSeleccionado = "Office 2016 Dark";
                        break;
                    case "Office 2016 Negro":
                        temaSeleccionado = "Office 2016 Black";
                        break;
                    case "Moderno":
                        temaSeleccionado = "The Bezier";
                        break;
                    case "Café":
                        temaSeleccionado = "Coffee";
                        break;
                    case "Azul cielo":
                        temaSeleccionado = "Liquid Sky";
                        break;
                    case "Azul opaco":
                        temaSeleccionado = "London Liquid Sky";
                        break;
                    case "Océano":
                        temaSeleccionado = "Glass Oceans";
                        break;
                    case "Lila":
                        temaSeleccionado = "Stardust";
                        break;
                    case "Invierno":
                        temaSeleccionado = "Xmas 2008 Blue";
                        break;
                    case "Amor y Amistad":
                        temaSeleccionado = "Valentine";
                        break;
                    case "Apple":
                        temaSeleccionado = "McSkin";
                        break;
                    case "Verano":
                        temaSeleccionado = "Summer 2008";
                        break;
                    case "Halloween":
                        temaSeleccionado = "Pumpkin";
                        break;
                    case "Oscuro":
                        temaSeleccionado = "Dark Side";
                        break;
                    case "Primavera":
                        temaSeleccionado = "Springtime";
                        break;
                    case "Neblina":
                        temaSeleccionado = "Foggy";
                        break;
                    case "Alto Contraste":
                        temaSeleccionado = "High Contrast";
                        break;
                    case "Windows 7 Moderno":
                        temaSeleccionado = "Seven";
                        break;
                    case "Oscuro Fino":
                        temaSeleccionado = "Sharp";
                        break;
                    case "Oscuro Extra":
                        temaSeleccionado = "Sharp Plus";
                        break;
                    case "Blanco y Azul":
                        temaSeleccionado = "The Asphalt World";
                        break;
                    case "Pintura Blanca":
                        temaSeleccionado = "Whiteprint";
                        break;
                    case "Caramelo":
                        temaSeleccionado = "Caramel";
                        break;
                    case "Azul Elegante":
                        temaSeleccionado = "Money Twins";
                        break;
                    case "Lila Elegante":
                        temaSeleccionado = "Lilian";
                        break;
                    case "Azul Brilloso":
                        temaSeleccionado = "iMaginary";
                        break;
                    case "Negro Brilloso":
                        temaSeleccionado = "Black";
                        break;
                    case "Office 2007 Azul":
                        temaSeleccionado = "Office 2007 Blue";
                        break;
                    case "Office 2007 Oscuro":
                        temaSeleccionado = "Office 2007 Black";
                        break;
                    case "Office 2007 Plateado":
                        temaSeleccionado = "Office 2007 Silver";
                        break;
                    case "Office 2007 Verde":
                        temaSeleccionado = "Office 2007 Green";
                        break;
                    case "Office 2007 Rosa":
                        temaSeleccionado = "Office 2007 Pink";
                        break;
                    case "Lunar":
                        temaSeleccionado = "Blue";
                        break;
                    case "Oscuro Perfecto":
                        temaSeleccionado = "Darkroom";
                        break;
                    case "Pintura Azul":
                        temaSeleccionado = "Blueprint";
                        break;
                    case "Moderno Oscuro":
                        temaSeleccionado = "Metropolis Dark";
                        break;
                    case "Moderno Claro":
                        temaSeleccionado = "Metropolis";
                        break;
                    default:
                        temaSeleccionado = "The Bezier";
                        break;
                }

                return temaSeleccionado;
            }
            catch (Exception)
            {
                return "The Bezier";
            }
        }

        public bool GuardarAjustes(Ajustes ajustes)
        {
            try
            {
                ComprobarGeneral();

                string datos = JsonConvert.SerializeObject(ajustes, Formatting.Indented);

                File.WriteAllText(ajustesJSON, datos);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Ajustes LeerAjustes()
        {
            try
            {
                ComprobarGeneral();

                string datos = File.ReadAllText(ajustesJSON);

                Ajustes ajustes = JsonConvert.DeserializeObject<Ajustes>(datos);

                return ajustes;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool ObtenerPesoDirectorio(string dirAEvaluar)
        {
            try
            {
                DirectoryInfo informacion = new DirectoryInfo(dirAEvaluar);

                long tamanoDir = informacion.EnumerateFiles("*.*", SearchOption.AllDirectories).Sum(file => file.Length);

                if (tamanoDir > 5368709120)
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool ValidarYGuardarAjustesServidores(Local servidorLocal, Remoto servidorRemoto)
        {
            try
            {
                if (!File.Exists(ajustesServidores))
                {
                    using (File.Create(ajustesServidores)) { }
                }

                string datos = File.ReadAllText(ajustesServidores);

                Servidores servidores = new Servidores { Local = servidorLocal, Remoto = servidorRemoto };

                if (datos == null || string.IsNullOrWhiteSpace(datos))
                {
                    string configuraciones = JsonConvert.SerializeObject(servidores, Formatting.Indented);

                    File.WriteAllText(ajustesServidores, configuraciones);
                }

                else
                {
                    var servidorTemporal = JsonConvert.DeserializeObject<Servidores>(datos);

                    if (servidorTemporal.Local.Servidor == "")
                    {
                        if (servidorLocal.Servidor != "")
                        {
                            servidorTemporal.Local = servidorLocal;

                            string configuraciones = JsonConvert.SerializeObject(servidorTemporal, Formatting.Indented);

                            File.WriteAllText(ajustesServidores, configuraciones);
                        }
                    }

                    else
                    {
                        if (servidorLocal.Servidor != "")
                        {
                            servidorTemporal.Local = servidorLocal;

                            string configuraciones = JsonConvert.SerializeObject(servidorTemporal, Formatting.Indented);

                            File.WriteAllText(ajustesServidores, configuraciones);
                        }
                    }

                    if (servidorTemporal.Remoto.Servidor == "")
                    {
                        if (servidorRemoto.Servidor != "")
                        {
                            servidorTemporal.Remoto = servidorRemoto;

                            string configuraciones = JsonConvert.SerializeObject(servidorTemporal, Formatting.Indented);

                            File.WriteAllText(ajustesServidores, configuraciones);
                        }
                    }

                    else
                    {
                        if (servidorRemoto.Servidor != "")
                        {
                            servidorTemporal.Remoto = servidorRemoto;

                            string configuraciones = JsonConvert.SerializeObject(servidorTemporal, Formatting.Indented);

                            File.WriteAllText(ajustesServidores, configuraciones);
                        }
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Servidores LeerConfiguracionServidores()
        {
            try
            {
                if (!File.Exists(ajustesServidores))
                {
                    using (File.Create(ajustesServidores)) { }

                    return null;
                }

                string datos = File.ReadAllText(ajustesServidores);

                Servidores configuraciones = JsonConvert.DeserializeObject<Servidores>(datos);

                return configuraciones;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public SessionOptions ValidarServidorRemoto(Servidores servidores)
        {
            try
            {
                SessionOptions cliente = new SessionOptions();

                Ajustes ajustes = LeerAjustes();

                int puerto = Convert.ToInt32(seguridad.Desencriptar(servidores.Remoto.Puerto));

                if (puerto <= 0)
                {
                    puerto = 0;
                }
                
                string tipoEncriptacion = seguridad.Desencriptar(servidores.Remoto.Encriptacion);

                string protocolo = seguridad.Desencriptar(servidores.Remoto.Protocolo);

                cliente.HostName = seguridad.Desencriptar(servidores.Remoto.Servidor);
                cliente.UserName = seguridad.Desencriptar(servidores.Remoto.Usuario);
                cliente.Password = seguridad.Desencriptar(servidores.Remoto.Password);
                cliente.PortNumber = puerto;
                cliente.TimeoutInMilliseconds = (Convert.ToInt32(ajustes != null ? ajustes.TiempoDesconexion.ToString() : "9999")) * 1000;
                cliente.AddRawSettings("ProxyPort", "0");

                //Se tira la seguridad y se acepta cualquier certificado
                if (seguridad.Desencriptar(servidores.Remoto.Autenticar) == "True")
                {
                    cliente.GiveUpSecurityAndAcceptAnyTlsHostCertificate = true;
                }

                //Modificar a servidor FTP Activo
                if (seguridad.Desencriptar(servidores.Remoto.Modo) == "True")
                {
                    cliente.FtpMode = FtpMode.Active;
                }

                //Via SFTP
                if (protocolo == "SFTP")
                {
                    cliente.Protocol = Protocol.Sftp;

                    string fingerprint = null;

                    XML.XmlDocument hostconocidos = new XML.XmlDocument();

                    if (File.Exists(dirHostsConocidos))
                    {
                        hostconocidos.Load(dirHostsConocidos);
                    }
                    else
                    {
                        hostconocidos.AppendChild(hostconocidos.CreateElement("KnownHosts"));
                    }

                    string sessionKey = seguridad.Desencriptar(servidores.Remoto.Servidor) + ":" + puerto.ToString();
                    XML.XmlNode fingerprintNode = hostconocidos.DocumentElement.SelectSingleNode("KnownHost[@host='" + sessionKey + "']/@fingerprint");

                    if (fingerprintNode != null)
                    {
                        fingerprint = fingerprintNode.Value;
                    }

                    else
                    {
                        using (Session sesion = new Session())
                        {
                            fingerprint = sesion.ScanFingerprint(cliente, "SHA-256");
                        }

                        XML.XmlElement knownHost = hostconocidos.CreateElement("KnownHost");
                        hostconocidos.DocumentElement.AppendChild(knownHost);
                        knownHost.SetAttribute("host", sessionKey);
                        knownHost.SetAttribute("fingerprint", fingerprint);

                        hostconocidos.Save(dirHostsConocidos);
                    }

                    cliente.SshHostKeyFingerprint = fingerprint;
                }

                ////Via FTP
                else
                {
                    cliente.Protocol = Protocol.Ftp;

                    //Encriptación Implicita
                    if (tipoEncriptacion == "1")
                    {
                        cliente.FtpSecure = FtpSecure.Implicit;
                    }

                    //Encriptacion Explicita
                    if (tipoEncriptacion == "2")
                    {
                        cliente.FtpSecure = FtpSecure.Explicit;
                    }
                }

                using (Session sesion = new Session())
                {
                    sesion.Open(cliente);

                    var infoDirectorio = sesion.ListDirectory(seguridad.Desencriptar(servidores.Remoto.Directorio)).Files.Any();

                    return infoDirectorio ? cliente : null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public SessionOptions ValidarServidorLocal(Servidores servidores)
        {
            try
            {
                SessionOptions cliente = new SessionOptions();

                Ajustes ajustes = LeerAjustes();

                string protocolo = seguridad.Desencriptar(servidores.Local.Protocolo);

                int puerto = Convert.ToInt32(seguridad.Desencriptar(servidores.Local.Puerto));

                if (puerto <= 0)
                {
                    puerto = 0;
                }

                string tipoEncriptacion = seguridad.Desencriptar(servidores.Local.Encriptacion);

                cliente.HostName = seguridad.Desencriptar(servidores.Local.Servidor);
                cliente.UserName = seguridad.Desencriptar(servidores.Local.Usuario);
                cliente.Password = seguridad.Desencriptar(servidores.Local.Password);
                cliente.PortNumber = puerto;
                cliente.TimeoutInMilliseconds = (Convert.ToInt32(ajustes != null ? ajustes.TiempoDesconexion.ToString() : "9999")) * 1000;
                cliente.AddRawSettings("ProxyPort", "0");

                //Se tira la seguridad y se acepta cualquier certificado
                if (seguridad.Desencriptar(servidores.Local.Autenticar) == "True")
                {
                    cliente.GiveUpSecurityAndAcceptAnyTlsHostCertificate = true;
                }

                //Modificar a servidor FTP Activo
                if (seguridad.Desencriptar(servidores.Local.Modo) == "True")
                {
                    cliente.FtpMode = FtpMode.Active;
                }

                //Via SFTP
                if (protocolo == "SFTP")
                {
                    cliente.Protocol = Protocol.Sftp;

                    string fingerprint = null;

                    XML.XmlDocument hostconocidos = new XML.XmlDocument();

                    if (File.Exists(dirHostsConocidos))
                    {
                        hostconocidos.Load(dirHostsConocidos);
                    }
                    else
                    {
                        hostconocidos.AppendChild(hostconocidos.CreateElement("KnownHosts"));
                    }

                    string sessionKey = seguridad.Desencriptar(servidores.Local.Servidor) + ":" + puerto.ToString();
                    XML.XmlNode fingerprintNode = hostconocidos.DocumentElement.SelectSingleNode("KnownHost[@host='" + sessionKey + "']/@fingerprint");

                    if (fingerprintNode != null)
                    {
                        fingerprint = fingerprintNode.Value;
                    }

                    else
                    {
                        using (Session sesion = new Session())
                        {
                            fingerprint = sesion.ScanFingerprint(cliente, "SHA-256");
                        }

                        XML.XmlElement knownHost = hostconocidos.CreateElement("KnownHost");
                        hostconocidos.DocumentElement.AppendChild(knownHost);
                        knownHost.SetAttribute("host", sessionKey);
                        knownHost.SetAttribute("fingerprint", fingerprint);

                        hostconocidos.Save(dirHostsConocidos);
                    }

                    cliente.SshHostKeyFingerprint = fingerprint;
                }

                ////Via FTP
                else
                {
                    cliente.Protocol = Protocol.Ftp;

                    //Encriptación Implicita
                    if (tipoEncriptacion == "1")
                    {
                        cliente.FtpSecure = FtpSecure.Implicit;
                    }

                    //Encriptacion Explicita
                    if (tipoEncriptacion == "2")
                    {
                        cliente.FtpSecure = FtpSecure.Explicit;
                    }
                }

                using (Session sesion = new Session())
                {
                    sesion.Open(cliente);

                    var infoDirectorio = sesion.ListDirectory(seguridad.Desencriptar(servidores.Local.Directorio)).Files.Any();

                    return infoDirectorio ? cliente : null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
