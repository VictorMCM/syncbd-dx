﻿using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using SyncBD.Modelos;

namespace SyncBD.Controladores
{
    class EnlaceModelo
    {
        SyncBDModelo modelo = new SyncBDModelo();

        protected internal bool CrearMovimientoRemoto(MovimientoRemoto movimiento)
        {
            return modelo.CrearMovimientoRemoto(movimiento);
        }

        protected internal bool CrearMovimientoLocal(MovimientoLocal movimiento)
        {
            return modelo.CrearMovimientoLocal(movimiento);
        }

        protected internal void ListaMovimientosServidorRemoto(GridControl aLlenar, GridView formato)
        {
            modelo.ListaMovimientosServidorRemoto(aLlenar, formato);
        }

        protected internal void ListaMovimientosServidorLocal(GridControl aLlenar, GridView formato)
        {
            modelo.ListaMovimientosServidorLocal(aLlenar, formato);
        }

        protected internal bool BorrarHistorial()
        {
            return modelo.BorrarHistorial();
        }

        protected internal bool BorrarHistorialRemoto()
        {
            return modelo.BorrarHistorialRemoto();
        }

        protected internal bool BorrarHistorialLocal()
        {
            return modelo.BorrarHistorialLocal();
        }

        protected internal bool ConsultarError(MemoEdit contenido, LabelControl titulo, string codigo)
        {
            return modelo.ConsultarError(contenido, titulo, codigo);
        }
    }
}
