﻿using DevExpress.Skins;
using DevExpress.UserSkins;
using SyncBD.Controladores;
using SyncBD.Vistas;
using System;
using System.Threading;
using System.Windows.Forms;

namespace SyncBD
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Nucleo nucleo = new Nucleo();

                Ajustes ajustes = nucleo.LeerAjustes();

                if (ajustes != null)
                {
                    if (ajustes.Apariencia != null || !string.IsNullOrWhiteSpace(ajustes.Apariencia) || !string.IsNullOrEmpty(ajustes.Apariencia))
                    {
                        DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = nucleo.DeterminarTema(ajustes.Apariencia);
                    }

                    else
                    {
                        DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = "The Bezier";
                    }
                }

                else
                {
                    DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = "The Bezier";
                }
            }
            catch (Exception)
            {
                DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = "The Bezier";
            }
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-MX");
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-MX");

            BonusSkins.Register();
            SkinManager.EnableFormSkins();
            Application.Run(new SplashSyncBD());
        }
    }
}
