﻿using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using Logger;
using SyncBD.Controladores;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SyncBD.Vistas
{
    public partial class ConfigLocal : XtraForm
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        private static ConfigLocal instancia;

        public static ConfigLocal ObtenerInstancia()
        {
            if (instancia == null || instancia.IsDisposed)
            {
                instancia = new ConfigLocal();
            }

            instancia.BringToFront();

            return instancia;
        }

        private ConfigLocal()
        {
            InitializeComponent();

            LeerConfiguracionesPrevias();
        }

        Nucleo nucleo = new Nucleo();

        Seguridad seguridad = new Seguridad();

        Servidores servidores = new Servidores();

        ToLog log = new ToLog();

        public static bool combinado = false;

        public static DialogResult resultado = DialogResult.Cancel;

        private void MostrarNotificacion(string titulo, string texto, Image imagen, int toast)
        {
            try
            {
                if (nucleo.ObtenerVersionSO().Contains("WINDOWS 8") || nucleo.ObtenerVersionSO().Contains("WINDOWS 10"))
                {
                    try
                    {
                        toastMensaje.ShowNotification(toastMensaje.Notifications[toast]);
                    }
                    catch (Exception)
                    {
                        alertMensaje.Show(this, titulo, texto, "", imagen);
                    }
                }

                else
                {
                    alertMensaje.Show(this, titulo, texto, "", imagen);
                }
            }
            catch (Exception)
            {

            }
        }

        private void LeerConfiguracionesPrevias()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones != null)
                {
                    if (configuraciones.Local.Servidor != null)
                    {
                        txtServidor.Text = configuraciones.Local.Servidor != "" ? seguridad.Desencriptar(configuraciones.Local.Servidor) : "";
                        txtUsuario.Text = "";
                        txtPassword.Text = "";
                        txtPuerto.Text = configuraciones.Local.Puerto != "" ? seguridad.Desencriptar(configuraciones.Local.Puerto) : "";
                        txtDirectorio.Text = configuraciones.Local.Directorio != "" ? seguridad.Desencriptar(configuraciones.Local.Directorio) : "";
                        cbxEncriptacion.SelectedIndex = configuraciones.Local.Encriptacion != "" ? Convert.ToInt32(seguridad.Desencriptar(configuraciones.Local.Encriptacion)) : 0;
                        cbxProtocolo.SelectedItem = configuraciones.Local.Protocolo != "" ? seguridad.Desencriptar(configuraciones.Local.Protocolo) : "FTP";

                        if (seguridad.Desencriptar(configuraciones.Local.Anonimo) == "True")
                        {
                            chkAnonimo.Checked = true;
                        }

                        if (seguridad.Desencriptar(configuraciones.Local.Modo) == "True")
                        {
                            chkModoActivo.Checked = true;
                        }

                        if (seguridad.Desencriptar(configuraciones.Local.Autenticar) == "True")
                        {
                            chkAutenticacion.Checked = true;
                        }

                        if (seguridad.Desencriptar(configuraciones.Local.Combinado) == "True")
                        {
                            combinado = true;
                        }

                        log.AppendLog("[Configuraciones] Ajustes del servidor remoto cargados correctamente");
                    }
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x003, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x003", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 1);
            }
        }

        private void pbxVerPassword_MouseDown(object sender, MouseEventArgs e)
        {
            txtPassword.Properties.PasswordChar = '\0';
            pbxVerPassword.Image = Properties.Resources.Unlock_24px;
        }

        private void pbxVerPassword_MouseUp(object sender, MouseEventArgs e)
        {
            txtPassword.Properties.PasswordChar = '•';
            pbxVerPassword.Image = Properties.Resources.Lock_24px;
        }

        private void ConfigLocal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult = DialogResult.Cancel;

                log.AppendLog("[Configuraciones] El usuario cancelo la configuración");
            }
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            AyudaConfig mostrar = AyudaConfig.ObtenerInstancia();

            Hide();

            mostrar.ShowDialog();

            Show();

            mostrar.Dispose();

            mostrar.Close();
        }

        private void RealizarValidaciones()
        {
            try
            {
                if (txtUsuario.Text != "")
                {
                    txtUsuario.Text = txtUsuario.Text.TrimEnd(' ');
                    txtUsuario.Text = txtUsuario.Text.TrimStart(' ');
                }

                if (txtServidor.Text != "")
                {
                    txtServidor.Text = txtServidor.Text.TrimEnd(' ');
                    txtServidor.Text = txtServidor.Text.TrimStart(' ');
                }

                if (txtPassword.Text != "")
                {
                    txtPassword.Text = txtPassword.Text.TrimEnd(' ');
                    txtPassword.Text = txtPassword.Text.TrimStart(' ');
                }

                if (txtDirectorio.Text != "")
                {
                    txtDirectorio.Text = txtDirectorio.Text.TrimEnd(' ');
                    txtDirectorio.Text = txtDirectorio.Text.TrimStart(' ');
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x005, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x005", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 3);
            }
        }

        private void Ocupar()
        {
            if (!SSMOcupado.IsSplashFormVisible)
            {
                SSMOcupado.ShowWaitForm();
            }
        }

        private void Liberar()
        {
            if (SSMOcupado.IsSplashFormVisible)
            {
                SSMOcupado.CloseWaitForm();
            }
        }

        private void chkAjustesAvanzados_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAjustesAvanzados.Checked)
            {
                gbcAjustesAvanzados.Visible = true;
            }

            else
            {
                gbcAjustesAvanzados.Visible = false;
            }
        }

        private void chkAnonimo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAnonimo.Checked)
            {
                txtUsuario.Text = "anonymous";
                txtUsuario.Enabled = false;
                txtPassword.Text = "anonymous@example.com";
                txtPassword.Enabled = false;
            }

            else
            {
                txtUsuario.Text = "";
                txtUsuario.Enabled = true;
                txtPassword.Text = "";
                txtPassword.Enabled = true;
            }
        }

        private void cbxProtocolo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxProtocolo.SelectedIndex == 1)
            {
                cbxEncriptacion.Visible = false;
                lblEncriptacion.Visible = false;
            }

            else
            {
                cbxEncriptacion.Visible = true;
                lblEncriptacion.Visible = true;
            }
        }

        private void txtServidor_Leave(object sender, EventArgs e)
        {
            RealizarValidaciones();
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            try
            {
                RealizarValidaciones();

                if (txtServidor.Text == "" || txtUsuario.Text == "" || txtPassword.Text == "" || txtPuerto.Text == "" || cbxEncriptacion.SelectedIndex < 0 || cbxProtocolo.SelectedIndex < 0)
                {
                    XtraMessageBox.Show(UserLookAndFeel.Default, "Es necesario que proporcione todos los datos de los campos marcados con \"*\"\npara poder realizar la prueba de conexión con el servidor.", "SyncBD | Faltan datos", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    cbxProtocolo.Focus();
                }

                else
                {
                    string servidorFTP = "";

                    if (txtServidor.Text.Contains("localhost") || txtServidor.Text.Contains("127.0.0.1"))
                    {
                        servidorFTP = "localhost";
                    }

                    else
                    {
                        servidorFTP = txtServidor.Text;
                    }

                    //Usando FTP
                    if (cbxProtocolo.SelectedIndex == 0)
                    {
                        servidores = new Servidores { Local = new Local { Servidor = seguridad.Encriptar(servidorFTP), Anonimo = seguridad.Encriptar(chkAnonimo.Checked.ToString()), Usuario = seguridad.Encriptar(txtUsuario.Text), Password = seguridad.Encriptar(txtPassword.Text), Puerto = seguridad.Encriptar(txtPuerto.Text), Directorio = seguridad.Encriptar(txtDirectorio.Text == "" ? "/" : txtDirectorio.Text), Encriptacion = seguridad.Encriptar(cbxEncriptacion.SelectedIndex.ToString()), Modo = seguridad.Encriptar(chkModoActivo.Checked.ToString()), Autenticar = seguridad.Encriptar(chkAutenticacion.Checked.ToString()), Combinado = seguridad.Encriptar(combinado.ToString()), Protocolo = seguridad.Encriptar("FTP") }, Remoto = new Remoto { Servidor = "", Anonimo = "", Usuario = "", Password = "", Puerto = "", Directorio = "", Encriptacion = "", Modo = "", Autenticar = "", Combinado = seguridad.Encriptar(combinado.ToString()), Protocolo = "" } };
                    }

                    //Usando SFTP
                    else
                    {
                        servidores = new Servidores { Local = new Local { Servidor = seguridad.Encriptar(servidorFTP), Anonimo = seguridad.Encriptar(chkAnonimo.Checked.ToString()), Usuario = seguridad.Encriptar(txtUsuario.Text), Password = seguridad.Encriptar(txtPassword.Text), Puerto = seguridad.Encriptar(txtPuerto.Text), Directorio = seguridad.Encriptar(txtDirectorio.Text == "" ? "/" : txtDirectorio.Text), Encriptacion = "", Modo = seguridad.Encriptar(chkModoActivo.Checked.ToString()), Autenticar = seguridad.Encriptar(chkAutenticacion.Checked.ToString()), Combinado = seguridad.Encriptar(combinado.ToString()), Protocolo = seguridad.Encriptar("SFTP") }, Remoto = new Remoto { Servidor = "", Anonimo = "", Usuario = "", Password = "", Puerto = "", Directorio = "", Encriptacion = "", Modo = "", Autenticar = "", Combinado = seguridad.Encriptar(combinado.ToString()), Protocolo = "" } };
                    }

                    Ocupar();

                    if (nucleo.ValidarServidorLocal(servidores) != null)
                    {
                        if (nucleo.ValidarYGuardarAjustesServidores(servidores.Local, servidores.Remoto))
                        {
                            log.AppendLog("[Configuraciones] Ajustes de conexión con el servidor Local correctas");

                            Liberar();

                            XtraMessageBox.Show(UserLookAndFeel.Default, "Se ha conectado al servidor local correctamente.", "SyncBD | Conexión con servidor local establecida", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            resultado = DialogResult.OK;

                            Close();
                        }
                    }

                    else
                    {
                        log.AppendLog("[Configuraciones] Imposible establecer conexión con el servidor Local");

                        Liberar();

                        XtraMessageBox.Show(UserLookAndFeel.Default, "No se ha podido establecer conexión con el servidor local, verifique si los datos son correctos y reintente", "SyncBD | Imposible conectar con servidor local", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        cbxProtocolo.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x004, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x004", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 2);
            }
        }
    }
}