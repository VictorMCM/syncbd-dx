﻿using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using Logger;
using SyncBD.Controladores;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SyncBD.Vistas
{
    public partial class SplashSyncBD : SplashScreen
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        public SplashSyncBD()
        {
            InitializeComponent();

            lblCopyright.Text = DateTime.Now.ToString("yyyy") + " © Todos los derechos reservados.";
        }

        #region Overrides

        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        #endregion

        public enum SplashScreenCommand
        {
        }

        Nucleo nucleo = new Nucleo();
        Seguridad seguridad = new Seguridad();
        ToLog log = new ToLog();

        private void MostrarNotificacion(string titulo, string texto, Image imagen, int toast)
        {
            try
            {
                if (nucleo.ObtenerVersionSO().Contains("WINDOWS 8") || nucleo.ObtenerVersionSO().Contains("WINDOWS 10"))
                {
                    try
                    {
                        toastMensaje.ShowNotification(toastMensaje.Notifications[toast]);
                    }
                    catch (Exception)
                    {
                        alertMensaje.Show(this, titulo, texto, "", imagen);
                    }
                }

                else
                {
                    alertMensaje.Show(this, titulo, texto, "", imagen);
                }
            }
            catch (Exception)
            {

            }
        }

        private void EstablecerSistema()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones != null)
                {
                    Servidores servidores = new Servidores();

                    if (configuraciones.Remoto.Servidor == "" && seguridad.Desencriptar(configuraciones.Remoto.Combinado) == "True")
                    {
                        log.AppendLog("[Configuración] Faltan ajustes del servidor Remoto");

                        if(XtraMessageBox.Show(UserLookAndFeel.Default, "Parece que no configuró el servidor \"Remoto\"...\n¿Desea ir a la pantalla de configuración?", "SyncBD | Servidor Remoto no configurado", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            MenuServidores abrir = MenuServidores.ObtenerInstancia();

                            Hide();

                            abrir.Show();
                        }

                        else
                        {
                            Principal principal = Principal.ObtenerInstancia();

                            Hide();

                            principal.Show();
                        }
                    }

                    if (configuraciones.Local.Servidor == "" && seguridad.Desencriptar(configuraciones.Local.Combinado) == "True")
                    {
                        log.AppendLog("[Configuración] Faltan ajustes del servidor Local");

                        if (XtraMessageBox.Show(UserLookAndFeel.Default, "Parece que no configuró el servidor \"Local\"...\n¿Desea ir a la pantalla de configuración?", "SyncBD | Servidor Local no configurado", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            MenuServidores abrir = MenuServidores.ObtenerInstancia();

                            Hide();

                            abrir.Show();
                        }

                        else
                        {
                            Principal principal = Principal.ObtenerInstancia();

                            Hide();

                            principal.Show();
                        }
                    }

                    if (configuraciones.Remoto.Servidor != "" || configuraciones.Local.Servidor != "")
                    {
                        log.AppendLog("[Inicio] Verificaciones realizadas, iniciando pantalla principal");

                        Principal principal = Principal.ObtenerInstancia();

                        Hide();

                        principal.Show();
                    }
                }

                else
                {
                    tmpCambio.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x001, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x001", "Por favor dirijase al manual de usuario para detalles", SyncBD.Properties.Resources.Cancel_32px, 4);
            }
        }
        
        private void tmpCambio_Tick(object sender, EventArgs e)
        {
            Bienvenido mostrar = Bienvenido.ObtenerInstancia();

            tmpCambio.Enabled = false;

            Hide();

            mostrar.ShowDialog();
        }

        private void SplashSyncBD_Shown(object sender, EventArgs e)
        {
            nucleo.ComprobarGeneral();

            nucleo.ComprobarRegistros();

            EstablecerSistema();
        }
    }
}