﻿using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using Logger;
using SyncBD.Controladores;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SyncBD.Vistas
{
    public partial class Bienvenido : XtraForm
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        private static Bienvenido instancia;

        public static Bienvenido ObtenerInstancia()
        {
            if(instancia == null || instancia.IsDisposed)
            {
                instancia = new Bienvenido();
            }

            instancia.BringToFront();

            return instancia;
        }

        private Bienvenido()
        {
            InitializeComponent();
        }

        Nucleo nucleo = new Nucleo();

        ToLog log = new ToLog();

        private void Bienvenido_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.WindowsShutDown || e.CloseReason == CloseReason.TaskManagerClosing)
            {
                if (XtraMessageBox.Show(UserLookAndFeel.Default, "¿Desea cancelar las configuraciones y cerrar el programa?\nRecuerde que el asistente empezará nuevamente al iniciar el programa", "SyncBD | Confirme cierre", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                {
                    e.Cancel = true;
                }

                else
                {
                    Application.Exit();
                }
            }
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            MenuServidores mostrar = MenuServidores.ObtenerInstancia();

            log.AppendLog("[Configuraciones] Mostrando ventana de servidores");

            Hide();

            mostrar.Show();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            try
            {
                FormClosingEventArgs args = new FormClosingEventArgs(CloseReason.UserClosing, false);

                Bienvenido_FormClosing(sender, args);
            }
            catch (Exception)
            {

            }
        }
    }
}