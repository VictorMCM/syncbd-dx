﻿namespace SyncBD.Vistas
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem2 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem3 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem4 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem5 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem20 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem21 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem6 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem22 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem23 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem7 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem24 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem25 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem26 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem8 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem27 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem28 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem9 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem29 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem30 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip20 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem31 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem20 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip21 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem32 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem21 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem10 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem33 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip22 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem34 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem22 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip23 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem35 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem23 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip24 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem36 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem24 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem37 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip25 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem38 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem25 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip26 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem39 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem26 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem11 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem40 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip27 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem41 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem27 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip28 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem42 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem28 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem12 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem43 = new DevExpress.Utils.ToolTipTitleItem();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnMenuRemoto = new DevExpress.XtraBars.BarButtonItem();
            this.btnMenuLocal = new DevExpress.XtraBars.BarButtonItem();
            this.btnMenuAjustes = new DevExpress.XtraBars.BarButtonItem();
            this.btnMenuAyuda = new DevExpress.XtraBars.BarButtonItem();
            this.btnMenuSalir = new DevExpress.XtraBars.BarButtonItem();
            this.bsiVersion = new DevExpress.XtraBars.BarStaticItem();
            this.btnSubBorrarHistorialRemoto = new DevExpress.XtraBars.BarButtonItem();
            this.btnSubBorrarHistorialLocal = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.xtabPrincipal = new DevExpress.XtraTab.XtraTabControl();
            this.xtabRemoto = new DevExpress.XtraTab.XtraTabPage();
            this.petInfoRemoto = new DevExpress.XtraEditors.PictureEdit();
            this.lblInfoRemoto = new DevExpress.XtraEditors.LabelControl();
            this.gbcAccionesRemoto = new DevExpress.XtraEditors.GroupControl();
            this.btnAbrirCarpetaFinal = new DevExpress.XtraEditors.SimpleButton();
            this.btnSincronizarRemoto = new DevExpress.XtraEditors.SimpleButton();
            this.gbcPanelInformativoRemoto = new DevExpress.XtraEditors.GroupControl();
            this.btnAbortarRemoto = new DevExpress.XtraEditors.SimpleButton();
            this.lblNombreArchivoRemoto = new DevExpress.XtraEditors.LabelControl();
            this.pbrProgresoArchivoRemoto = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblProgresoArchivoRemoto = new DevExpress.XtraEditors.LabelControl();
            this.pbrProgresoTotalRemoto = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblProgresoTotalRemoto = new DevExpress.XtraEditors.LabelControl();
            this.lblVelocidadRemoto = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.lblTiempoRemoto = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.petAccionesRemoto = new DevExpress.XtraEditors.PictureEdit();
            this.lblAccionesRemoto = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gdcHistorialRemoto = new DevExpress.XtraGrid.GridControl();
            this.gdvHistorialRemoto = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.petLogo = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtabLocal = new DevExpress.XtraTab.XtraTabPage();
            this.petInfoLocal = new DevExpress.XtraEditors.PictureEdit();
            this.lblInfoLocal = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.gbcAccionesLocal = new DevExpress.XtraEditors.GroupControl();
            this.btnCargarArchivosDesde = new DevExpress.XtraEditors.SimpleButton();
            this.btnSincronizarLocal = new DevExpress.XtraEditors.SimpleButton();
            this.gbcPanelInformativoLocal = new DevExpress.XtraEditors.GroupControl();
            this.btnAbortarLocal = new DevExpress.XtraEditors.SimpleButton();
            this.lblNombreArchivoLocal = new DevExpress.XtraEditors.LabelControl();
            this.pbrProgresoArchivoLocal = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblProgresoArchivoLocal = new DevExpress.XtraEditors.LabelControl();
            this.pbrProgresoTotalLocal = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblProgresoTotalLocal = new DevExpress.XtraEditors.LabelControl();
            this.lblVelocidadLocal = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.lblTiempoLocal = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.petAccionesLocal = new DevExpress.XtraEditors.PictureEdit();
            this.lblAccionesLocal = new DevExpress.XtraEditors.LabelControl();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.gdcHistorialLocal = new DevExpress.XtraGrid.GridControl();
            this.gdvHistorialLocal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.xtabAjustes = new DevExpress.XtraTab.XtraTabPage();
            this.txtTiempoReconexion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtTiempoDesconexion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.btnBorrarHistoriales = new DevExpress.XtraEditors.DropDownButton();
            this.popMHistoriales = new DevExpress.XtraBars.PopupMenu(this.components);
            this.btnReestablecerAjustes = new DevExpress.XtraEditors.SimpleButton();
            this.cbxApariencia = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.btnConfigurarLocal = new DevExpress.XtraEditors.SimpleButton();
            this.btnConfigurarRemoto = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.btnRutaCopiaArchivos = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.chkMinimizarAlIniciar = new DevExpress.XtraEditors.CheckEdit();
            this.chkIniciarConWindows = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.xtabAyuda = new DevExpress.XtraTab.XtraTabPage();
            this.btnCrearArchivoSoporte = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnConsultarManual = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit9 = new DevExpress.XtraEditors.PictureEdit();
            this.lblVersion = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.lblCopyright = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit8 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.tmpLocalTiempoTranscurrido = new System.Windows.Forms.Timer(this.components);
            this.tmpRemotoTiempoTranscurrido = new System.Windows.Forms.Timer(this.components);
            this.nicBarraTareas = new System.Windows.Forms.NotifyIcon(this.components);
            this.cmpMenuBarraTareas = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnVerRemotoStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRemotoStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRemotoCancelarStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPauseRemoteStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRemoteTreeStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnVerLocalStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLocalStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLocalCancelarStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPauseLocalStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLocalTreeStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRestaurarStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSalirStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.fbdGuardarEn = new DevExpress.XtraEditors.XtraFolderBrowserDialog(this.components);
            this.toastMensaje = new DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager(this.components);
            this.alertMensaje = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            this.SSMCancelando = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::SyncBD.Vistas.Cancelando), true, true);
            this.SSMDirVerificacion = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::SyncBD.Vistas.DirVerficacion), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtabPrincipal)).BeginInit();
            this.xtabPrincipal.SuspendLayout();
            this.xtabRemoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.petInfoRemoto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbcAccionesRemoto)).BeginInit();
            this.gbcAccionesRemoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbcPanelInformativoRemoto)).BeginInit();
            this.gbcPanelInformativoRemoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbrProgresoArchivoRemoto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbrProgresoTotalRemoto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.petAccionesRemoto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdcHistorialRemoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdvHistorialRemoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.petLogo.Properties)).BeginInit();
            this.xtabLocal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.petInfoLocal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbcAccionesLocal)).BeginInit();
            this.gbcAccionesLocal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbcPanelInformativoLocal)).BeginInit();
            this.gbcPanelInformativoLocal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbrProgresoArchivoLocal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbrProgresoTotalLocal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.petAccionesLocal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdcHistorialLocal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdvHistorialLocal)).BeginInit();
            this.xtabAjustes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiempoReconexion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiempoDesconexion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popMHistoriales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxApariencia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRutaCopiaArchivos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMinimizarAlIniciar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIniciarConWindows.Properties)).BeginInit();
            this.xtabAyuda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).BeginInit();
            this.cmpMenuBarraTareas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toastMensaje)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnMenuRemoto,
            this.btnMenuLocal,
            this.btnMenuAjustes,
            this.btnMenuAyuda,
            this.btnMenuSalir,
            this.bsiVersion,
            this.btnSubBorrarHistorialRemoto,
            this.btnSubBorrarHistorialLocal});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 9;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.MacOffice;
            this.ribbon.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.ShowOnMultiplePages;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1014, 109);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // btnMenuRemoto
            // 
            this.btnMenuRemoto.Caption = "Sincronizar remoto";
            this.btnMenuRemoto.Id = 1;
            this.btnMenuRemoto.ImageOptions.LargeImage = global::SyncBD.Properties.Resources.DownloadFromFTP_96px;
            this.btnMenuRemoto.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R));
            this.btnMenuRemoto.Name = "btnMenuRemoto";
            this.btnMenuRemoto.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMenuRemoto_ItemClick);
            // 
            // btnMenuLocal
            // 
            this.btnMenuLocal.Caption = "Sincronizar local";
            this.btnMenuLocal.Id = 2;
            this.btnMenuLocal.ImageOptions.LargeImage = global::SyncBD.Properties.Resources.UploadToFTP_96px;
            this.btnMenuLocal.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L));
            this.btnMenuLocal.Name = "btnMenuLocal";
            this.btnMenuLocal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMenuLocal_ItemClick);
            // 
            // btnMenuAjustes
            // 
            this.btnMenuAjustes.Caption = "Ajustes";
            this.btnMenuAjustes.Id = 3;
            this.btnMenuAjustes.ImageOptions.LargeImage = global::SyncBD.Properties.Resources.Settings_96px;
            this.btnMenuAjustes.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A));
            this.btnMenuAjustes.Name = "btnMenuAjustes";
            this.btnMenuAjustes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMenuAjustes_ItemClick);
            // 
            // btnMenuAyuda
            // 
            this.btnMenuAyuda.Caption = "Ayuda";
            this.btnMenuAyuda.Id = 4;
            this.btnMenuAyuda.ImageOptions.LargeImage = global::SyncBD.Properties.Resources.Help_96px;
            this.btnMenuAyuda.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F1);
            this.btnMenuAyuda.Name = "btnMenuAyuda";
            this.btnMenuAyuda.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMenuAyuda_ItemClick);
            // 
            // btnMenuSalir
            // 
            this.btnMenuSalir.Caption = "Salir";
            this.btnMenuSalir.Id = 5;
            this.btnMenuSalir.ImageOptions.LargeImage = global::SyncBD.Properties.Resources.CloseWindow_96px;
            this.btnMenuSalir.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4));
            this.btnMenuSalir.Name = "btnMenuSalir";
            this.btnMenuSalir.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMenuSalir_ItemClick);
            // 
            // bsiVersion
            // 
            this.bsiVersion.Caption = "SyncBD v. 0.9";
            this.bsiVersion.Id = 6;
            this.bsiVersion.Name = "bsiVersion";
            // 
            // btnSubBorrarHistorialRemoto
            // 
            this.btnSubBorrarHistorialRemoto.Caption = "Borrar Remoto";
            this.btnSubBorrarHistorialRemoto.Id = 7;
            this.btnSubBorrarHistorialRemoto.ImageOptions.Image = global::SyncBD.Properties.Resources.DownloadFromFTP_16px;
            this.btnSubBorrarHistorialRemoto.Name = "btnSubBorrarHistorialRemoto";
            this.btnSubBorrarHistorialRemoto.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSubBorrarHistorialRemoto_ItemClick);
            // 
            // btnSubBorrarHistorialLocal
            // 
            this.btnSubBorrarHistorialLocal.Caption = "Borrar Local";
            this.btnSubBorrarHistorialLocal.Id = 8;
            this.btnSubBorrarHistorialLocal.ImageOptions.Image = global::SyncBD.Properties.Resources.UploadToFTP_16px;
            this.btnSubBorrarHistorialLocal.Name = "btnSubBorrarHistorialLocal";
            this.btnSubBorrarHistorialLocal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSubBorrarHistorialLocal_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuRemoto);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuLocal);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuAjustes);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuAyuda);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuSalir);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Menú de opciones";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.bsiVersion);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 568);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1014, 31);
            // 
            // xtabPrincipal
            // 
            this.xtabPrincipal.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders;
            this.xtabPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtabPrincipal.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.xtabPrincipal.Location = new System.Drawing.Point(0, 109);
            this.xtabPrincipal.Name = "xtabPrincipal";
            this.xtabPrincipal.SelectedTabPage = this.xtabRemoto;
            this.xtabPrincipal.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.xtabPrincipal.Size = new System.Drawing.Size(1014, 459);
            this.xtabPrincipal.TabIndex = 2;
            this.xtabPrincipal.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtabRemoto,
            this.xtabLocal,
            this.xtabAjustes,
            this.xtabAyuda});
            this.xtabPrincipal.CloseButtonClick += new System.EventHandler(this.xtabPrincipal_CloseButtonClick);
            // 
            // xtabRemoto
            // 
            this.xtabRemoto.Controls.Add(this.petInfoRemoto);
            this.xtabRemoto.Controls.Add(this.lblInfoRemoto);
            this.xtabRemoto.Controls.Add(this.gbcAccionesRemoto);
            this.xtabRemoto.Controls.Add(this.gbcPanelInformativoRemoto);
            this.xtabRemoto.Controls.Add(this.petAccionesRemoto);
            this.xtabRemoto.Controls.Add(this.lblAccionesRemoto);
            this.xtabRemoto.Controls.Add(this.groupControl1);
            this.xtabRemoto.Controls.Add(this.petLogo);
            this.xtabRemoto.Controls.Add(this.labelControl2);
            this.xtabRemoto.Controls.Add(this.labelControl1);
            this.xtabRemoto.Name = "xtabRemoto";
            this.xtabRemoto.PageVisible = false;
            this.xtabRemoto.Size = new System.Drawing.Size(1008, 431);
            this.xtabRemoto.Text = "Servidor Remoto";
            // 
            // petInfoRemoto
            // 
            this.petInfoRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.petInfoRemoto.Cursor = System.Windows.Forms.Cursors.Default;
            this.petInfoRemoto.EditValue = global::SyncBD.Properties.Resources.Info_24px;
            this.petInfoRemoto.Location = new System.Drawing.Point(554, 175);
            this.petInfoRemoto.Name = "petInfoRemoto";
            this.petInfoRemoto.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.petInfoRemoto.Properties.Appearance.Options.UseBackColor = true;
            this.petInfoRemoto.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.petInfoRemoto.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.petInfoRemoto.Properties.ShowMenu = false;
            this.petInfoRemoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.petInfoRemoto.Size = new System.Drawing.Size(24, 24);
            this.petInfoRemoto.TabIndex = 20;
            this.petInfoRemoto.Visible = false;
            // 
            // lblInfoRemoto
            // 
            this.lblInfoRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInfoRemoto.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblInfoRemoto.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblInfoRemoto.Appearance.Options.UseFont = true;
            this.lblInfoRemoto.Appearance.Options.UseForeColor = true;
            this.lblInfoRemoto.Location = new System.Drawing.Point(584, 180);
            this.lblInfoRemoto.Name = "lblInfoRemoto";
            this.lblInfoRemoto.Size = new System.Drawing.Size(249, 19);
            this.lblInfoRemoto.TabIndex = 19;
            this.lblInfoRemoto.Text = "Información de transferencia actual";
            this.lblInfoRemoto.Visible = false;
            // 
            // gbcAccionesRemoto
            // 
            this.gbcAccionesRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbcAccionesRemoto.AppearanceCaption.Options.UseTextOptions = true;
            this.gbcAccionesRemoto.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbcAccionesRemoto.CaptionImageOptions.Image = global::SyncBD.Properties.Resources.DownloadFromFTP_16px;
            this.gbcAccionesRemoto.Controls.Add(this.btnAbrirCarpetaFinal);
            this.gbcAccionesRemoto.Controls.Add(this.btnSincronizarRemoto);
            this.gbcAccionesRemoto.Location = new System.Drawing.Point(554, 80);
            this.gbcAccionesRemoto.Name = "gbcAccionesRemoto";
            this.gbcAccionesRemoto.Size = new System.Drawing.Size(447, 84);
            this.gbcAccionesRemoto.TabIndex = 18;
            this.gbcAccionesRemoto.Text = "Acciones disponibles";
            // 
            // btnAbrirCarpetaFinal
            // 
            this.btnAbrirCarpetaFinal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAbrirCarpetaFinal.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnAbrirCarpetaFinal.Appearance.Options.UseFont = true;
            this.btnAbrirCarpetaFinal.ImageOptions.Image = global::SyncBD.Properties.Resources.OpenFolder_16px;
            this.btnAbrirCarpetaFinal.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAbrirCarpetaFinal.Location = new System.Drawing.Point(245, 34);
            this.btnAbrirCarpetaFinal.Name = "btnAbrirCarpetaFinal";
            this.btnAbrirCarpetaFinal.Size = new System.Drawing.Size(139, 39);
            toolTipTitleItem1.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem1.Text = "Mostrar carpetas de sincronizaciones";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Muestra el directorio configurado en ajustes que muestra todas las carpetas con l" +
    "os archivos sincronizados.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.btnAbrirCarpetaFinal.SuperTip = superToolTip1;
            this.btnAbrirCarpetaFinal.TabIndex = 15;
            this.btnAbrirCarpetaFinal.Text = "Abrir carpeta";
            this.btnAbrirCarpetaFinal.Click += new System.EventHandler(this.btnAbrirCarpetaFinal_Click);
            // 
            // btnSincronizarRemoto
            // 
            this.btnSincronizarRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSincronizarRemoto.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnSincronizarRemoto.Appearance.Options.UseFont = true;
            this.btnSincronizarRemoto.ImageOptions.Image = global::SyncBD.Properties.Resources.Synchronize_16px;
            this.btnSincronizarRemoto.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnSincronizarRemoto.Location = new System.Drawing.Point(49, 34);
            this.btnSincronizarRemoto.Name = "btnSincronizarRemoto";
            this.btnSincronizarRemoto.Size = new System.Drawing.Size(139, 39);
            toolTipTitleItem2.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem2.Text = "Sincronizar con el servidor remoto";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Le permite descargar los archivos desde el servidor remoto siempre y cuando tenga" +
    " acceso al servidor.";
            toolTipTitleItem3.LeftIndent = 6;
            toolTipTitleItem3.Text = "(Nunca se cargarán archivos a este servidor)";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            superToolTip2.Items.Add(toolTipSeparatorItem1);
            superToolTip2.Items.Add(toolTipTitleItem3);
            this.btnSincronizarRemoto.SuperTip = superToolTip2;
            this.btnSincronizarRemoto.TabIndex = 14;
            this.btnSincronizarRemoto.Text = "Sincronizar";
            this.btnSincronizarRemoto.Click += new System.EventHandler(this.btnSincronizarRemoto_Click);
            // 
            // gbcPanelInformativoRemoto
            // 
            this.gbcPanelInformativoRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbcPanelInformativoRemoto.AppearanceCaption.Options.UseTextOptions = true;
            this.gbcPanelInformativoRemoto.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbcPanelInformativoRemoto.CaptionImageOptions.Image = global::SyncBD.Properties.Resources.DownloadFromFTP_16px;
            this.gbcPanelInformativoRemoto.Controls.Add(this.btnAbortarRemoto);
            this.gbcPanelInformativoRemoto.Controls.Add(this.lblNombreArchivoRemoto);
            this.gbcPanelInformativoRemoto.Controls.Add(this.pbrProgresoArchivoRemoto);
            this.gbcPanelInformativoRemoto.Controls.Add(this.lblProgresoArchivoRemoto);
            this.gbcPanelInformativoRemoto.Controls.Add(this.pbrProgresoTotalRemoto);
            this.gbcPanelInformativoRemoto.Controls.Add(this.lblProgresoTotalRemoto);
            this.gbcPanelInformativoRemoto.Controls.Add(this.lblVelocidadRemoto);
            this.gbcPanelInformativoRemoto.Controls.Add(this.pictureEdit3);
            this.gbcPanelInformativoRemoto.Controls.Add(this.lblTiempoRemoto);
            this.gbcPanelInformativoRemoto.Controls.Add(this.pictureEdit2);
            this.gbcPanelInformativoRemoto.Location = new System.Drawing.Point(554, 205);
            this.gbcPanelInformativoRemoto.Name = "gbcPanelInformativoRemoto";
            this.gbcPanelInformativoRemoto.Size = new System.Drawing.Size(447, 213);
            this.gbcPanelInformativoRemoto.TabIndex = 17;
            this.gbcPanelInformativoRemoto.Text = "Información de progreso";
            this.gbcPanelInformativoRemoto.Visible = false;
            // 
            // btnAbortarRemoto
            // 
            this.btnAbortarRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAbortarRemoto.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnAbortarRemoto.Appearance.Options.UseFont = true;
            this.btnAbortarRemoto.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAbortarRemoto.ImageOptions.Image")));
            this.btnAbortarRemoto.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAbortarRemoto.Location = new System.Drawing.Point(282, 34);
            this.btnAbortarRemoto.Name = "btnAbortarRemoto";
            this.btnAbortarRemoto.Size = new System.Drawing.Size(160, 46);
            toolTipTitleItem4.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem4.Text = "Abortar sincronización actual";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Si el programa está enlazado con el servidor, puede cancelar la sincronización ac" +
    "tual. Se le pedirá confirmación.";
            toolTipTitleItem5.LeftIndent = 6;
            toolTipTitleItem5.Text = "(Puede demorar en cancelar la sincronización)";
            superToolTip3.Items.Add(toolTipTitleItem4);
            superToolTip3.Items.Add(toolTipItem3);
            superToolTip3.Items.Add(toolTipTitleItem5);
            this.btnAbortarRemoto.SuperTip = superToolTip3;
            this.btnAbortarRemoto.TabIndex = 28;
            this.btnAbortarRemoto.Text = "Abortar sincronización";
            this.btnAbortarRemoto.Click += new System.EventHandler(this.btnAbortarRemoto_Click);
            // 
            // lblNombreArchivoRemoto
            // 
            this.lblNombreArchivoRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNombreArchivoRemoto.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblNombreArchivoRemoto.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblNombreArchivoRemoto.Appearance.Options.UseFont = true;
            this.lblNombreArchivoRemoto.Appearance.Options.UseForeColor = true;
            this.lblNombreArchivoRemoto.Location = new System.Drawing.Point(14, 184);
            this.lblNombreArchivoRemoto.Name = "lblNombreArchivoRemoto";
            this.lblNombreArchivoRemoto.Size = new System.Drawing.Size(78, 16);
            toolTipTitleItem6.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem6.Text = "Nombre del archivo";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Nombre del archivo con el que el programa está trabajando actualmente.";
            superToolTip4.Items.Add(toolTipTitleItem6);
            superToolTip4.Items.Add(toolTipItem4);
            this.lblNombreArchivoRemoto.SuperTip = superToolTip4;
            this.lblNombreArchivoRemoto.TabIndex = 24;
            this.lblNombreArchivoRemoto.Text = "Descarga.png";
            // 
            // pbrProgresoArchivoRemoto
            // 
            this.pbrProgresoArchivoRemoto.Location = new System.Drawing.Point(14, 161);
            this.pbrProgresoArchivoRemoto.MenuManager = this.ribbon;
            this.pbrProgresoArchivoRemoto.Name = "pbrProgresoArchivoRemoto";
            this.pbrProgresoArchivoRemoto.Size = new System.Drawing.Size(428, 18);
            toolTipTitleItem7.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem7.Text = "Progreso del archivo";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Muestra de forma visual el progreso de descarga del archivo actual.";
            superToolTip5.Items.Add(toolTipTitleItem7);
            superToolTip5.Items.Add(toolTipItem5);
            this.pbrProgresoArchivoRemoto.SuperTip = superToolTip5;
            this.pbrProgresoArchivoRemoto.TabIndex = 23;
            // 
            // lblProgresoArchivoRemoto
            // 
            this.lblProgresoArchivoRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProgresoArchivoRemoto.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblProgresoArchivoRemoto.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblProgresoArchivoRemoto.Appearance.Options.UseFont = true;
            this.lblProgresoArchivoRemoto.Appearance.Options.UseForeColor = true;
            this.lblProgresoArchivoRemoto.Location = new System.Drawing.Point(14, 139);
            this.lblProgresoArchivoRemoto.Name = "lblProgresoArchivoRemoto";
            this.lblProgresoArchivoRemoto.Size = new System.Drawing.Size(89, 16);
            this.lblProgresoArchivoRemoto.TabIndex = 22;
            this.lblProgresoArchivoRemoto.Text = "0% completado";
            // 
            // pbrProgresoTotalRemoto
            // 
            this.pbrProgresoTotalRemoto.Location = new System.Drawing.Point(14, 115);
            this.pbrProgresoTotalRemoto.MenuManager = this.ribbon;
            this.pbrProgresoTotalRemoto.Name = "pbrProgresoTotalRemoto";
            this.pbrProgresoTotalRemoto.Size = new System.Drawing.Size(428, 18);
            toolTipTitleItem8.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem8.Text = "Progreso total";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Muestra de forma visual el avance total de la sincronización.";
            toolTipTitleItem9.LeftIndent = 6;
            toolTipTitleItem9.Text = "(Más información en el manual de usuario)";
            superToolTip6.Items.Add(toolTipTitleItem8);
            superToolTip6.Items.Add(toolTipItem6);
            superToolTip6.Items.Add(toolTipSeparatorItem2);
            superToolTip6.Items.Add(toolTipTitleItem9);
            this.pbrProgresoTotalRemoto.SuperTip = superToolTip6;
            this.pbrProgresoTotalRemoto.TabIndex = 21;
            // 
            // lblProgresoTotalRemoto
            // 
            this.lblProgresoTotalRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProgresoTotalRemoto.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblProgresoTotalRemoto.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblProgresoTotalRemoto.Appearance.Options.UseFont = true;
            this.lblProgresoTotalRemoto.Appearance.Options.UseForeColor = true;
            this.lblProgresoTotalRemoto.Location = new System.Drawing.Point(14, 93);
            this.lblProgresoTotalRemoto.Name = "lblProgresoTotalRemoto";
            this.lblProgresoTotalRemoto.Size = new System.Drawing.Size(118, 16);
            this.lblProgresoTotalRemoto.TabIndex = 20;
            this.lblProgresoTotalRemoto.Text = "0% total completado";
            // 
            // lblVelocidadRemoto
            // 
            this.lblVelocidadRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVelocidadRemoto.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblVelocidadRemoto.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblVelocidadRemoto.Appearance.Options.UseFont = true;
            this.lblVelocidadRemoto.Appearance.Options.UseForeColor = true;
            this.lblVelocidadRemoto.Location = new System.Drawing.Point(44, 64);
            this.lblVelocidadRemoto.Name = "lblVelocidadRemoto";
            this.lblVelocidadRemoto.Size = new System.Drawing.Size(157, 16);
            toolTipTitleItem10.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem10.Text = "Velocidad de transferencia";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Se representa la velocidad de transferencia en Kilobytes / segundo.";
            toolTipTitleItem11.LeftIndent = 6;
            toolTipTitleItem11.Text = "(Varibale según su ancho de banda y el peso del archivo en cuestión)";
            superToolTip7.Items.Add(toolTipTitleItem10);
            superToolTip7.Items.Add(toolTipItem7);
            superToolTip7.Items.Add(toolTipSeparatorItem3);
            superToolTip7.Items.Add(toolTipTitleItem11);
            this.lblVelocidadRemoto.SuperTip = superToolTip7;
            this.lblVelocidadRemoto.TabIndex = 19;
            this.lblVelocidadRemoto.Text = "Velocidad de transferencia:";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit3.EditValue = global::SyncBD.Properties.Resources.Speed_24px;
            this.pictureEdit3.Location = new System.Drawing.Point(14, 56);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit3.Properties.ShowMenu = false;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit3.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit3.TabIndex = 18;
            // 
            // lblTiempoRemoto
            // 
            this.lblTiempoRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTiempoRemoto.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblTiempoRemoto.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblTiempoRemoto.Appearance.Options.UseFont = true;
            this.lblTiempoRemoto.Appearance.Options.UseForeColor = true;
            this.lblTiempoRemoto.Location = new System.Drawing.Point(44, 34);
            this.lblTiempoRemoto.Name = "lblTiempoRemoto";
            this.lblTiempoRemoto.Size = new System.Drawing.Size(121, 16);
            toolTipTitleItem12.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem12.Text = "Tiempo transcurrido";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Muestra el lapso de tiempo de la operación actual";
            superToolTip8.Items.Add(toolTipTitleItem12);
            superToolTip8.Items.Add(toolTipItem8);
            this.lblTiempoRemoto.SuperTip = superToolTip8;
            this.lblTiempoRemoto.TabIndex = 17;
            this.lblTiempoRemoto.Text = "Tiempo transcurrido:";
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = global::SyncBD.Properties.Resources.Clock_24px;
            this.pictureEdit2.Location = new System.Drawing.Point(14, 26);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit2.TabIndex = 16;
            // 
            // petAccionesRemoto
            // 
            this.petAccionesRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.petAccionesRemoto.Cursor = System.Windows.Forms.Cursors.Default;
            this.petAccionesRemoto.EditValue = global::SyncBD.Properties.Resources.AvailableUpdates_24px;
            this.petAccionesRemoto.Location = new System.Drawing.Point(554, 50);
            this.petAccionesRemoto.Name = "petAccionesRemoto";
            this.petAccionesRemoto.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.petAccionesRemoto.Properties.Appearance.Options.UseBackColor = true;
            this.petAccionesRemoto.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.petAccionesRemoto.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.petAccionesRemoto.Properties.ShowMenu = false;
            this.petAccionesRemoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.petAccionesRemoto.Size = new System.Drawing.Size(24, 24);
            this.petAccionesRemoto.TabIndex = 15;
            // 
            // lblAccionesRemoto
            // 
            this.lblAccionesRemoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAccionesRemoto.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblAccionesRemoto.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblAccionesRemoto.Appearance.Options.UseFont = true;
            this.lblAccionesRemoto.Appearance.Options.UseForeColor = true;
            this.lblAccionesRemoto.Location = new System.Drawing.Point(584, 55);
            this.lblAccionesRemoto.Name = "lblAccionesRemoto";
            this.lblAccionesRemoto.Size = new System.Drawing.Size(146, 19);
            this.lblAccionesRemoto.TabIndex = 14;
            this.lblAccionesRemoto.Text = "Acciones disponibles";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.groupControl1.Appearance.Options.UseBackColor = true;
            this.groupControl1.Controls.Add(this.gdcHistorialRemoto);
            this.groupControl1.Location = new System.Drawing.Point(11, 80);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(527, 338);
            this.groupControl1.TabIndex = 13;
            this.groupControl1.Text = "groupControl1";
            // 
            // gdcHistorialRemoto
            // 
            this.gdcHistorialRemoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gdcHistorialRemoto.Location = new System.Drawing.Point(2, 2);
            this.gdcHistorialRemoto.MainView = this.gdvHistorialRemoto;
            this.gdcHistorialRemoto.MenuManager = this.ribbon;
            this.gdcHistorialRemoto.Name = "gdcHistorialRemoto";
            this.gdcHistorialRemoto.Size = new System.Drawing.Size(523, 334);
            this.gdcHistorialRemoto.TabIndex = 0;
            this.gdcHistorialRemoto.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gdvHistorialRemoto});
            // 
            // gdvHistorialRemoto
            // 
            this.gdvHistorialRemoto.GridControl = this.gdcHistorialRemoto;
            this.gdvHistorialRemoto.Name = "gdvHistorialRemoto";
            this.gdvHistorialRemoto.OptionsBehavior.Editable = false;
            // 
            // petLogo
            // 
            this.petLogo.Cursor = System.Windows.Forms.Cursors.Default;
            this.petLogo.EditValue = global::SyncBD.Properties.Resources.Historical_24px;
            this.petLogo.Location = new System.Drawing.Point(11, 50);
            this.petLogo.Name = "petLogo";
            this.petLogo.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.petLogo.Properties.Appearance.Options.UseBackColor = true;
            this.petLogo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.petLogo.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.petLogo.Properties.ShowMenu = false;
            this.petLogo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.petLogo.Size = new System.Drawing.Size(24, 24);
            this.petLogo.TabIndex = 12;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(41, 55);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(154, 19);
            this.labelControl2.TabIndex = 11;
            this.labelControl2.Text = "Historial de descargas";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(11, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(312, 29);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "Opciones del servidor remoto";
            // 
            // xtabLocal
            // 
            this.xtabLocal.Controls.Add(this.petInfoLocal);
            this.xtabLocal.Controls.Add(this.lblInfoLocal);
            this.xtabLocal.Controls.Add(this.pictureEdit7);
            this.xtabLocal.Controls.Add(this.gbcAccionesLocal);
            this.xtabLocal.Controls.Add(this.gbcPanelInformativoLocal);
            this.xtabLocal.Controls.Add(this.petAccionesLocal);
            this.xtabLocal.Controls.Add(this.lblAccionesLocal);
            this.xtabLocal.Controls.Add(this.groupControl6);
            this.xtabLocal.Controls.Add(this.labelControl15);
            this.xtabLocal.Controls.Add(this.labelControl16);
            this.xtabLocal.Name = "xtabLocal";
            this.xtabLocal.PageVisible = false;
            this.xtabLocal.Size = new System.Drawing.Size(1008, 431);
            this.xtabLocal.Text = "Servidor Local";
            // 
            // petInfoLocal
            // 
            this.petInfoLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.petInfoLocal.Cursor = System.Windows.Forms.Cursors.Default;
            this.petInfoLocal.EditValue = global::SyncBD.Properties.Resources.Info_24px;
            this.petInfoLocal.Location = new System.Drawing.Point(553, 176);
            this.petInfoLocal.Name = "petInfoLocal";
            this.petInfoLocal.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.petInfoLocal.Properties.Appearance.Options.UseBackColor = true;
            this.petInfoLocal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.petInfoLocal.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.petInfoLocal.Properties.ShowMenu = false;
            this.petInfoLocal.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.petInfoLocal.Size = new System.Drawing.Size(24, 24);
            this.petInfoLocal.TabIndex = 28;
            this.petInfoLocal.Visible = false;
            // 
            // lblInfoLocal
            // 
            this.lblInfoLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInfoLocal.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblInfoLocal.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblInfoLocal.Appearance.Options.UseFont = true;
            this.lblInfoLocal.Appearance.Options.UseForeColor = true;
            this.lblInfoLocal.Location = new System.Drawing.Point(583, 181);
            this.lblInfoLocal.Name = "lblInfoLocal";
            this.lblInfoLocal.Size = new System.Drawing.Size(249, 19);
            this.lblInfoLocal.TabIndex = 27;
            this.lblInfoLocal.Text = "Información de transferencia actual";
            this.lblInfoLocal.Visible = false;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit7.EditValue = global::SyncBD.Properties.Resources.Historical_24px;
            this.pictureEdit7.Location = new System.Drawing.Point(10, 51);
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit7.Properties.ShowMenu = false;
            this.pictureEdit7.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit7.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit7.TabIndex = 26;
            // 
            // gbcAccionesLocal
            // 
            this.gbcAccionesLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbcAccionesLocal.AppearanceCaption.Options.UseTextOptions = true;
            this.gbcAccionesLocal.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbcAccionesLocal.CaptionImageOptions.Image = global::SyncBD.Properties.Resources.DownloadFromFTP_16px;
            this.gbcAccionesLocal.Controls.Add(this.btnCargarArchivosDesde);
            this.gbcAccionesLocal.Controls.Add(this.btnSincronizarLocal);
            this.gbcAccionesLocal.Location = new System.Drawing.Point(553, 81);
            this.gbcAccionesLocal.Name = "gbcAccionesLocal";
            this.gbcAccionesLocal.Size = new System.Drawing.Size(448, 84);
            this.gbcAccionesLocal.TabIndex = 25;
            this.gbcAccionesLocal.Text = "Acciones disponibles";
            // 
            // btnCargarArchivosDesde
            // 
            this.btnCargarArchivosDesde.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCargarArchivosDesde.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnCargarArchivosDesde.Appearance.Options.UseFont = true;
            this.btnCargarArchivosDesde.ImageOptions.Image = global::SyncBD.Properties.Resources.OpenFolder_16px;
            this.btnCargarArchivosDesde.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnCargarArchivosDesde.Location = new System.Drawing.Point(230, 32);
            this.btnCargarArchivosDesde.Name = "btnCargarArchivosDesde";
            this.btnCargarArchivosDesde.Size = new System.Drawing.Size(181, 39);
            toolTipTitleItem13.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem13.Text = "Cargar archivos";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Le permite seleccionar una carpeta que contenga archivos para cargarlos al servid" +
    "or local. Puede cargar desde un dispositivo externo.";
            toolTipTitleItem14.LeftIndent = 6;
            toolTipTitleItem14.Text = "(Esta acción borrará los archivos previos descargados)";
            superToolTip9.Items.Add(toolTipTitleItem13);
            superToolTip9.Items.Add(toolTipItem9);
            superToolTip9.Items.Add(toolTipSeparatorItem4);
            superToolTip9.Items.Add(toolTipTitleItem14);
            this.btnCargarArchivosDesde.SuperTip = superToolTip9;
            this.btnCargarArchivosDesde.TabIndex = 15;
            this.btnCargarArchivosDesde.Text = "Cargar archivos desde...";
            this.btnCargarArchivosDesde.Click += new System.EventHandler(this.btnCargarArchivosDesde_Click);
            // 
            // btnSincronizarLocal
            // 
            this.btnSincronizarLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSincronizarLocal.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnSincronizarLocal.Appearance.Options.UseFont = true;
            this.btnSincronizarLocal.ImageOptions.Image = global::SyncBD.Properties.Resources.Synchronize_16px;
            this.btnSincronizarLocal.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnSincronizarLocal.Location = new System.Drawing.Point(48, 32);
            this.btnSincronizarLocal.Name = "btnSincronizarLocal";
            this.btnSincronizarLocal.Size = new System.Drawing.Size(139, 39);
            toolTipTitleItem15.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem15.Text = "Sincronizar con el servidor Local";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Le permite cargar los archivos desde este equipo siempre y cuando tenga acceso al" +
    " servidor.";
            toolTipTitleItem16.LeftIndent = 6;
            toolTipTitleItem16.Text = "(Nunca se descargarán archivos de este servidor)";
            superToolTip10.Items.Add(toolTipTitleItem15);
            superToolTip10.Items.Add(toolTipItem10);
            superToolTip10.Items.Add(toolTipSeparatorItem5);
            superToolTip10.Items.Add(toolTipTitleItem16);
            this.btnSincronizarLocal.SuperTip = superToolTip10;
            this.btnSincronizarLocal.TabIndex = 14;
            this.btnSincronizarLocal.Text = "Sincronizar";
            this.btnSincronizarLocal.Click += new System.EventHandler(this.btnSincronizarLocal_Click);
            // 
            // gbcPanelInformativoLocal
            // 
            this.gbcPanelInformativoLocal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbcPanelInformativoLocal.AppearanceCaption.Options.UseTextOptions = true;
            this.gbcPanelInformativoLocal.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbcPanelInformativoLocal.CaptionImageOptions.Image = global::SyncBD.Properties.Resources.DownloadFromFTP_16px;
            this.gbcPanelInformativoLocal.Controls.Add(this.btnAbortarLocal);
            this.gbcPanelInformativoLocal.Controls.Add(this.lblNombreArchivoLocal);
            this.gbcPanelInformativoLocal.Controls.Add(this.pbrProgresoArchivoLocal);
            this.gbcPanelInformativoLocal.Controls.Add(this.lblProgresoArchivoLocal);
            this.gbcPanelInformativoLocal.Controls.Add(this.pbrProgresoTotalLocal);
            this.gbcPanelInformativoLocal.Controls.Add(this.lblProgresoTotalLocal);
            this.gbcPanelInformativoLocal.Controls.Add(this.lblVelocidadLocal);
            this.gbcPanelInformativoLocal.Controls.Add(this.pictureEdit4);
            this.gbcPanelInformativoLocal.Controls.Add(this.lblTiempoLocal);
            this.gbcPanelInformativoLocal.Controls.Add(this.pictureEdit5);
            this.gbcPanelInformativoLocal.Location = new System.Drawing.Point(553, 206);
            this.gbcPanelInformativoLocal.Name = "gbcPanelInformativoLocal";
            this.gbcPanelInformativoLocal.Size = new System.Drawing.Size(448, 213);
            this.gbcPanelInformativoLocal.TabIndex = 24;
            this.gbcPanelInformativoLocal.Text = "Información de progreso";
            this.gbcPanelInformativoLocal.Visible = false;
            // 
            // btnAbortarLocal
            // 
            this.btnAbortarLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAbortarLocal.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnAbortarLocal.Appearance.Options.UseFont = true;
            this.btnAbortarLocal.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAbortarLocal.ImageOptions.Image")));
            this.btnAbortarLocal.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAbortarLocal.Location = new System.Drawing.Point(283, 34);
            this.btnAbortarLocal.Name = "btnAbortarLocal";
            this.btnAbortarLocal.Size = new System.Drawing.Size(160, 46);
            toolTipTitleItem17.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem17.Text = "Abortar sincronización actual";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Si el programa está enlazado con el servidor, puede cancelar la sincronización ac" +
    "tual. Se le pedirá confirmación.";
            toolTipTitleItem18.LeftIndent = 6;
            toolTipTitleItem18.Text = "(Puede demorar en cancelar la sincronización)";
            superToolTip11.Items.Add(toolTipTitleItem17);
            superToolTip11.Items.Add(toolTipItem11);
            superToolTip11.Items.Add(toolTipTitleItem18);
            this.btnAbortarLocal.SuperTip = superToolTip11;
            this.btnAbortarLocal.TabIndex = 27;
            this.btnAbortarLocal.Text = "Abortar sincronización";
            this.btnAbortarLocal.Click += new System.EventHandler(this.btnAbortarLocal_Click);
            // 
            // lblNombreArchivoLocal
            // 
            this.lblNombreArchivoLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNombreArchivoLocal.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblNombreArchivoLocal.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblNombreArchivoLocal.Appearance.Options.UseFont = true;
            this.lblNombreArchivoLocal.Appearance.Options.UseForeColor = true;
            this.lblNombreArchivoLocal.Location = new System.Drawing.Point(14, 184);
            this.lblNombreArchivoLocal.Name = "lblNombreArchivoLocal";
            this.lblNombreArchivoLocal.Size = new System.Drawing.Size(78, 16);
            toolTipTitleItem19.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem19.Text = "Nombre del archivo";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Nombre del archivo con el que el programa está trabajando actualmente.";
            superToolTip12.Items.Add(toolTipTitleItem19);
            superToolTip12.Items.Add(toolTipItem12);
            this.lblNombreArchivoLocal.SuperTip = superToolTip12;
            this.lblNombreArchivoLocal.TabIndex = 24;
            this.lblNombreArchivoLocal.Text = "Descarga.png";
            // 
            // pbrProgresoArchivoLocal
            // 
            this.pbrProgresoArchivoLocal.Location = new System.Drawing.Point(14, 161);
            this.pbrProgresoArchivoLocal.MenuManager = this.ribbon;
            this.pbrProgresoArchivoLocal.Name = "pbrProgresoArchivoLocal";
            this.pbrProgresoArchivoLocal.Size = new System.Drawing.Size(429, 18);
            toolTipTitleItem20.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem20.Text = "Progreso del archivo";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Muestra de forma visual el progreso de carga del archivo actual.";
            superToolTip13.Items.Add(toolTipTitleItem20);
            superToolTip13.Items.Add(toolTipItem13);
            this.pbrProgresoArchivoLocal.SuperTip = superToolTip13;
            this.pbrProgresoArchivoLocal.TabIndex = 23;
            // 
            // lblProgresoArchivoLocal
            // 
            this.lblProgresoArchivoLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProgresoArchivoLocal.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblProgresoArchivoLocal.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblProgresoArchivoLocal.Appearance.Options.UseFont = true;
            this.lblProgresoArchivoLocal.Appearance.Options.UseForeColor = true;
            this.lblProgresoArchivoLocal.Location = new System.Drawing.Point(14, 139);
            this.lblProgresoArchivoLocal.Name = "lblProgresoArchivoLocal";
            this.lblProgresoArchivoLocal.Size = new System.Drawing.Size(89, 16);
            this.lblProgresoArchivoLocal.TabIndex = 22;
            this.lblProgresoArchivoLocal.Text = "0% completado";
            // 
            // pbrProgresoTotalLocal
            // 
            this.pbrProgresoTotalLocal.Location = new System.Drawing.Point(14, 115);
            this.pbrProgresoTotalLocal.MenuManager = this.ribbon;
            this.pbrProgresoTotalLocal.Name = "pbrProgresoTotalLocal";
            this.pbrProgresoTotalLocal.Size = new System.Drawing.Size(429, 18);
            toolTipTitleItem21.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem21.Text = "Progreso total";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Muestra de forma visual el avance total de la sincronización.";
            toolTipTitleItem22.LeftIndent = 6;
            toolTipTitleItem22.Text = "(Más información en el manual de usuario)";
            superToolTip14.Items.Add(toolTipTitleItem21);
            superToolTip14.Items.Add(toolTipItem14);
            superToolTip14.Items.Add(toolTipSeparatorItem6);
            superToolTip14.Items.Add(toolTipTitleItem22);
            this.pbrProgresoTotalLocal.SuperTip = superToolTip14;
            this.pbrProgresoTotalLocal.TabIndex = 21;
            // 
            // lblProgresoTotalLocal
            // 
            this.lblProgresoTotalLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProgresoTotalLocal.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblProgresoTotalLocal.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblProgresoTotalLocal.Appearance.Options.UseFont = true;
            this.lblProgresoTotalLocal.Appearance.Options.UseForeColor = true;
            this.lblProgresoTotalLocal.Location = new System.Drawing.Point(14, 93);
            this.lblProgresoTotalLocal.Name = "lblProgresoTotalLocal";
            this.lblProgresoTotalLocal.Size = new System.Drawing.Size(118, 16);
            this.lblProgresoTotalLocal.TabIndex = 20;
            this.lblProgresoTotalLocal.Text = "0% total completado";
            // 
            // lblVelocidadLocal
            // 
            this.lblVelocidadLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVelocidadLocal.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblVelocidadLocal.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblVelocidadLocal.Appearance.Options.UseFont = true;
            this.lblVelocidadLocal.Appearance.Options.UseForeColor = true;
            this.lblVelocidadLocal.Location = new System.Drawing.Point(44, 64);
            this.lblVelocidadLocal.Name = "lblVelocidadLocal";
            this.lblVelocidadLocal.Size = new System.Drawing.Size(157, 16);
            toolTipTitleItem23.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem23.Text = "Velocidad de transferencia";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Se representa la velocidad de transferencia en Kilobytes / segundo.";
            toolTipTitleItem24.LeftIndent = 6;
            toolTipTitleItem24.Text = "(Varibale según su ancho de banda y el peso del archivo en cuestión)";
            superToolTip15.Items.Add(toolTipTitleItem23);
            superToolTip15.Items.Add(toolTipItem15);
            superToolTip15.Items.Add(toolTipSeparatorItem7);
            superToolTip15.Items.Add(toolTipTitleItem24);
            this.lblVelocidadLocal.SuperTip = superToolTip15;
            this.lblVelocidadLocal.TabIndex = 19;
            this.lblVelocidadLocal.Text = "Velocidad de transferencia:";
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit4.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit4.EditValue = global::SyncBD.Properties.Resources.Speed_24px;
            this.pictureEdit4.Location = new System.Drawing.Point(14, 56);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit4.Properties.ShowMenu = false;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit4.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit4.TabIndex = 18;
            // 
            // lblTiempoLocal
            // 
            this.lblTiempoLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTiempoLocal.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblTiempoLocal.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblTiempoLocal.Appearance.Options.UseFont = true;
            this.lblTiempoLocal.Appearance.Options.UseForeColor = true;
            this.lblTiempoLocal.Location = new System.Drawing.Point(44, 34);
            this.lblTiempoLocal.Name = "lblTiempoLocal";
            this.lblTiempoLocal.Size = new System.Drawing.Size(121, 16);
            toolTipTitleItem25.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem25.Text = "Tiempo transcurrido";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Muestra el lapso de tiempo de la operación actual";
            superToolTip16.Items.Add(toolTipTitleItem25);
            superToolTip16.Items.Add(toolTipItem16);
            this.lblTiempoLocal.SuperTip = superToolTip16;
            this.lblTiempoLocal.TabIndex = 17;
            this.lblTiempoLocal.Text = "Tiempo transcurrido:";
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit5.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit5.EditValue = global::SyncBD.Properties.Resources.Clock_24px;
            this.pictureEdit5.Location = new System.Drawing.Point(14, 26);
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit5.Properties.ShowMenu = false;
            this.pictureEdit5.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit5.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit5.TabIndex = 16;
            // 
            // petAccionesLocal
            // 
            this.petAccionesLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.petAccionesLocal.Cursor = System.Windows.Forms.Cursors.Default;
            this.petAccionesLocal.EditValue = global::SyncBD.Properties.Resources.AvailableUpdates_24px;
            this.petAccionesLocal.Location = new System.Drawing.Point(553, 51);
            this.petAccionesLocal.Name = "petAccionesLocal";
            this.petAccionesLocal.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.petAccionesLocal.Properties.Appearance.Options.UseBackColor = true;
            this.petAccionesLocal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.petAccionesLocal.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.petAccionesLocal.Properties.ShowMenu = false;
            this.petAccionesLocal.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.petAccionesLocal.Size = new System.Drawing.Size(24, 24);
            this.petAccionesLocal.TabIndex = 23;
            // 
            // lblAccionesLocal
            // 
            this.lblAccionesLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAccionesLocal.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblAccionesLocal.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblAccionesLocal.Appearance.Options.UseFont = true;
            this.lblAccionesLocal.Appearance.Options.UseForeColor = true;
            this.lblAccionesLocal.Location = new System.Drawing.Point(583, 56);
            this.lblAccionesLocal.Name = "lblAccionesLocal";
            this.lblAccionesLocal.Size = new System.Drawing.Size(146, 19);
            this.lblAccionesLocal.TabIndex = 22;
            this.lblAccionesLocal.Text = "Acciones disponibles";
            // 
            // groupControl6
            // 
            this.groupControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.groupControl6.Appearance.Options.UseBackColor = true;
            this.groupControl6.Controls.Add(this.gdcHistorialLocal);
            this.groupControl6.Location = new System.Drawing.Point(10, 81);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.ShowCaption = false;
            this.groupControl6.Size = new System.Drawing.Size(527, 338);
            this.groupControl6.TabIndex = 21;
            this.groupControl6.Text = "groupControl6";
            // 
            // gdcHistorialLocal
            // 
            this.gdcHistorialLocal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gdcHistorialLocal.Location = new System.Drawing.Point(2, 2);
            this.gdcHistorialLocal.MainView = this.gdvHistorialLocal;
            this.gdcHistorialLocal.MenuManager = this.ribbon;
            this.gdcHistorialLocal.Name = "gdcHistorialLocal";
            this.gdcHistorialLocal.Size = new System.Drawing.Size(523, 334);
            this.gdcHistorialLocal.TabIndex = 0;
            this.gdcHistorialLocal.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gdvHistorialLocal});
            // 
            // gdvHistorialLocal
            // 
            this.gdvHistorialLocal.GridControl = this.gdcHistorialLocal;
            this.gdvHistorialLocal.Name = "gdvHistorialLocal";
            this.gdvHistorialLocal.OptionsBehavior.Editable = false;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Location = new System.Drawing.Point(40, 56);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(130, 19);
            this.labelControl15.TabIndex = 20;
            this.labelControl15.Text = "Historial de cargas";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Location = new System.Drawing.Point(10, 16);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(283, 29);
            this.labelControl16.TabIndex = 19;
            this.labelControl16.Text = "Opciones del servidor local";
            // 
            // xtabAjustes
            // 
            this.xtabAjustes.Controls.Add(this.txtTiempoReconexion);
            this.xtabAjustes.Controls.Add(this.labelControl4);
            this.xtabAjustes.Controls.Add(this.txtTiempoDesconexion);
            this.xtabAjustes.Controls.Add(this.labelControl3);
            this.xtabAjustes.Controls.Add(this.groupControl7);
            this.xtabAjustes.Controls.Add(this.cbxApariencia);
            this.xtabAjustes.Controls.Add(this.labelControl20);
            this.xtabAjustes.Controls.Add(this.btnConfigurarLocal);
            this.xtabAjustes.Controls.Add(this.btnConfigurarRemoto);
            this.xtabAjustes.Controls.Add(this.labelControl19);
            this.xtabAjustes.Controls.Add(this.btnRutaCopiaArchivos);
            this.xtabAjustes.Controls.Add(this.labelControl18);
            this.xtabAjustes.Controls.Add(this.chkMinimizarAlIniciar);
            this.xtabAjustes.Controls.Add(this.chkIniciarConWindows);
            this.xtabAjustes.Controls.Add(this.labelControl17);
            this.xtabAjustes.Name = "xtabAjustes";
            this.xtabAjustes.PageVisible = false;
            this.xtabAjustes.Size = new System.Drawing.Size(1008, 431);
            this.xtabAjustes.Text = "Ajustes";
            // 
            // txtTiempoReconexion
            // 
            this.txtTiempoReconexion.EditValue = "3";
            this.txtTiempoReconexion.Location = new System.Drawing.Point(446, 165);
            this.txtTiempoReconexion.MenuManager = this.ribbon;
            this.txtTiempoReconexion.Name = "txtTiempoReconexion";
            this.txtTiempoReconexion.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtTiempoReconexion.Properties.Appearance.Options.UseFont = true;
            this.txtTiempoReconexion.Properties.Mask.BeepOnError = true;
            this.txtTiempoReconexion.Properties.Mask.EditMask = "d";
            this.txtTiempoReconexion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTiempoReconexion.Properties.MaxLength = 4;
            this.txtTiempoReconexion.Size = new System.Drawing.Size(139, 22);
            toolTipTitleItem26.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_16px;
            toolTipTitleItem26.Text = "Tiempo a esperar para reconectar";
            toolTipItem17.LeftIndent = 6;
            toolTipItem17.Text = "Cuando se cambia el estado de red o se quedo sin internet, es el tiempo en que Sy" +
    "ncBD tratará de reconectar con el servidor.";
            toolTipTitleItem27.ImageOptions.Image = global::SyncBD.Properties.Resources.Attention_16px;
            toolTipTitleItem27.LeftIndent = 6;
            toolTipTitleItem27.Text = "Nota: Solo funciona para conexiones que no se han cancelado o cerrado completamen" +
    "te.";
            superToolTip17.Items.Add(toolTipTitleItem26);
            superToolTip17.Items.Add(toolTipItem17);
            superToolTip17.Items.Add(toolTipSeparatorItem8);
            superToolTip17.Items.Add(toolTipTitleItem27);
            this.txtTiempoReconexion.SuperTip = superToolTip17;
            this.txtTiempoReconexion.TabIndex = 32;
            this.txtTiempoReconexion.EditValueChanged += new System.EventHandler(this.txtTiempoReconexion_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(446, 143);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(219, 16);
            this.labelControl4.TabIndex = 31;
            this.labelControl4.Text = "Tiempo en segundos para reconectar:";
            // 
            // txtTiempoDesconexion
            // 
            this.txtTiempoDesconexion.EditValue = "9999";
            this.txtTiempoDesconexion.Location = new System.Drawing.Point(446, 92);
            this.txtTiempoDesconexion.MenuManager = this.ribbon;
            this.txtTiempoDesconexion.Name = "txtTiempoDesconexion";
            this.txtTiempoDesconexion.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtTiempoDesconexion.Properties.Appearance.Options.UseFont = true;
            this.txtTiempoDesconexion.Properties.Mask.BeepOnError = true;
            this.txtTiempoDesconexion.Properties.Mask.EditMask = "d";
            this.txtTiempoDesconexion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTiempoDesconexion.Properties.MaxLength = 4;
            this.txtTiempoDesconexion.Size = new System.Drawing.Size(139, 22);
            toolTipTitleItem28.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_16px;
            toolTipTitleItem28.Text = "Tiempo para desconectar";
            toolTipItem18.LeftIndent = 6;
            toolTipItem18.Text = "Es el tiempo que SyncBD espera la creacion del listado de archivos y directorios " +
    "del servidor antes de cerrar por completo la conexión";
            toolTipTitleItem29.ImageOptions.Image = global::SyncBD.Properties.Resources.Attention_16px;
            toolTipTitleItem29.LeftIndent = 6;
            toolTipTitleItem29.Text = "Tiempo interpretado en segundos";
            superToolTip18.Items.Add(toolTipTitleItem28);
            superToolTip18.Items.Add(toolTipItem18);
            superToolTip18.Items.Add(toolTipSeparatorItem9);
            superToolTip18.Items.Add(toolTipTitleItem29);
            this.txtTiempoDesconexion.SuperTip = superToolTip18;
            this.txtTiempoDesconexion.TabIndex = 30;
            this.txtTiempoDesconexion.TextChanged += new System.EventHandler(this.txtTiempoDesconexion_TextChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(446, 70);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(227, 16);
            this.labelControl3.TabIndex = 29;
            this.labelControl3.Text = "Tiempo en segundos para desconectar:";
            // 
            // groupControl7
            // 
            this.groupControl7.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl7.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl7.CaptionImageOptions.Image = global::SyncBD.Properties.Resources.Attention_16px;
            this.groupControl7.Controls.Add(this.btnBorrarHistoriales);
            this.groupControl7.Controls.Add(this.btnReestablecerAjustes);
            this.groupControl7.Location = new System.Drawing.Point(11, 283);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(400, 136);
            this.groupControl7.TabIndex = 28;
            this.groupControl7.Text = "Zona de cuidado";
            // 
            // btnBorrarHistoriales
            // 
            this.btnBorrarHistoriales.DropDownControl = this.popMHistoriales;
            this.btnBorrarHistoriales.ImageOptions.Image = global::SyncBD.Properties.Resources.HighPriority_16px;
            this.btnBorrarHistoriales.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnBorrarHistoriales.Location = new System.Drawing.Point(31, 59);
            this.btnBorrarHistoriales.MenuManager = this.ribbon;
            this.btnBorrarHistoriales.Name = "btnBorrarHistoriales";
            this.btnBorrarHistoriales.Size = new System.Drawing.Size(151, 39);
            toolTipTitleItem30.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem30.Text = "Borrar historiales de sincronización";
            toolTipItem19.LeftIndent = 6;
            toolTipItem19.Text = "Puede borrar los historiales de sincronización o si prefiere puede seleccionar de" +
    " que servidor borrar el historial.";
            superToolTip19.Items.Add(toolTipTitleItem30);
            superToolTip19.Items.Add(toolTipItem19);
            this.btnBorrarHistoriales.SuperTip = superToolTip19;
            this.btnBorrarHistoriales.TabIndex = 17;
            this.btnBorrarHistoriales.Text = "Borrar historial";
            this.btnBorrarHistoriales.Click += new System.EventHandler(this.btnBorrarHistoriales_Click);
            // 
            // popMHistoriales
            // 
            this.popMHistoriales.ItemLinks.Add(this.btnSubBorrarHistorialRemoto);
            this.popMHistoriales.ItemLinks.Add(this.btnSubBorrarHistorialLocal);
            this.popMHistoriales.Name = "popMHistoriales";
            this.popMHistoriales.Ribbon = this.ribbon;
            // 
            // btnReestablecerAjustes
            // 
            this.btnReestablecerAjustes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReestablecerAjustes.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnReestablecerAjustes.Appearance.Options.UseFont = true;
            this.btnReestablecerAjustes.ImageOptions.Image = global::SyncBD.Properties.Resources.HighPriority_16px;
            this.btnReestablecerAjustes.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnReestablecerAjustes.Location = new System.Drawing.Point(221, 59);
            this.btnReestablecerAjustes.Name = "btnReestablecerAjustes";
            this.btnReestablecerAjustes.Size = new System.Drawing.Size(161, 39);
            toolTipTitleItem31.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem31.Text = "Reestablecer ajustes";
            toolTipItem20.LeftIndent = 6;
            toolTipItem20.Text = "Le permite reestablecer los ajustes a como estaban al instalar el programa.";
            superToolTip20.Items.Add(toolTipTitleItem31);
            superToolTip20.Items.Add(toolTipItem20);
            this.btnReestablecerAjustes.SuperTip = superToolTip20;
            this.btnReestablecerAjustes.TabIndex = 16;
            this.btnReestablecerAjustes.Text = "Reestablecer ajustes";
            this.btnReestablecerAjustes.Click += new System.EventHandler(this.btnReestablecerAjustes_Click);
            // 
            // cbxApariencia
            // 
            this.cbxApariencia.EditValue = "";
            this.cbxApariencia.Location = new System.Drawing.Point(181, 233);
            this.cbxApariencia.Name = "cbxApariencia";
            this.cbxApariencia.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbxApariencia.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.cbxApariencia.Properties.Appearance.Options.UseFont = true;
            this.cbxApariencia.Properties.Appearance.Options.UseForeColor = true;
            this.cbxApariencia.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxApariencia.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxApariencia.Size = new System.Drawing.Size(230, 22);
            toolTipTitleItem32.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem32.Text = "Cambiar estilo visual de SyncBD";
            toolTipItem21.LeftIndent = 6;
            toolTipItem21.Text = "Puede seleccionar uno de los temas que tiene disponibles el programa para cambiar" +
    " la apariencia del sistema acorde a su selección, guardadonse de forma automátic" +
    "a al seleccionarlo.";
            toolTipTitleItem33.ImageOptions.Image = global::SyncBD.Properties.Resources.Attention_24px;
            toolTipTitleItem33.LeftIndent = 6;
            toolTipTitleItem33.Text = "(Tenga en cuenta que algunos temas pueden dificultar la lectura o no ser compatib" +
    "les con el sistema operativo que use)";
            superToolTip21.Items.Add(toolTipTitleItem32);
            superToolTip21.Items.Add(toolTipItem21);
            superToolTip21.Items.Add(toolTipSeparatorItem10);
            superToolTip21.Items.Add(toolTipTitleItem33);
            this.cbxApariencia.SuperTip = superToolTip21;
            this.cbxApariencia.TabIndex = 27;
            this.cbxApariencia.SelectedIndexChanged += new System.EventHandler(this.cbxApariencia_SelectedIndexChanged);
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Appearance.Options.UseForeColor = true;
            this.labelControl20.Location = new System.Drawing.Point(11, 236);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(162, 16);
            this.labelControl20.TabIndex = 26;
            this.labelControl20.Text = "Cambiar estilo de SyncBD a:";
            // 
            // btnConfigurarLocal
            // 
            this.btnConfigurarLocal.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnConfigurarLocal.Appearance.Options.UseFont = true;
            this.btnConfigurarLocal.ImageOptions.Image = global::SyncBD.Properties.Resources.UploadToFTP_16px;
            this.btnConfigurarLocal.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnConfigurarLocal.Location = new System.Drawing.Point(272, 165);
            this.btnConfigurarLocal.Name = "btnConfigurarLocal";
            this.btnConfigurarLocal.Size = new System.Drawing.Size(139, 39);
            toolTipTitleItem34.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem34.Text = "Configure los ajustes del servidor Local";
            toolTipItem22.LeftIndent = 6;
            toolTipItem22.Text = "Permite configurar o modificar los ajustes de conexión para enlazar con el servid" +
    "or remoto.";
            superToolTip22.Items.Add(toolTipTitleItem34);
            superToolTip22.Items.Add(toolTipItem22);
            this.btnConfigurarLocal.SuperTip = superToolTip22;
            this.btnConfigurarLocal.TabIndex = 25;
            this.btnConfigurarLocal.Text = "Local";
            this.btnConfigurarLocal.Click += new System.EventHandler(this.btnConfigurarLocal_Click);
            // 
            // btnConfigurarRemoto
            // 
            this.btnConfigurarRemoto.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnConfigurarRemoto.Appearance.Options.UseFont = true;
            this.btnConfigurarRemoto.ImageOptions.Image = global::SyncBD.Properties.Resources.DownloadFromFTP_16px;
            this.btnConfigurarRemoto.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnConfigurarRemoto.Location = new System.Drawing.Point(11, 165);
            this.btnConfigurarRemoto.Name = "btnConfigurarRemoto";
            this.btnConfigurarRemoto.Size = new System.Drawing.Size(139, 39);
            toolTipTitleItem35.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem35.Text = "Configure los ajustes del servidor Remoto";
            toolTipItem23.LeftIndent = 6;
            toolTipItem23.Text = "Permite configurar o modificar los ajustes de conexión para enlazar con el servid" +
    "or remoto.";
            superToolTip23.Items.Add(toolTipTitleItem35);
            superToolTip23.Items.Add(toolTipItem23);
            this.btnConfigurarRemoto.SuperTip = superToolTip23;
            this.btnConfigurarRemoto.TabIndex = 24;
            this.btnConfigurarRemoto.Text = "Remoto";
            this.btnConfigurarRemoto.Click += new System.EventHandler(this.btnConfigurarRemoto_Click);
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Appearance.Options.UseForeColor = true;
            this.labelControl19.Location = new System.Drawing.Point(11, 143);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(256, 16);
            this.labelControl19.TabIndex = 23;
            this.labelControl19.Text = "Reconfigurar datos de conexión del servidor:";
            // 
            // btnRutaCopiaArchivos
            // 
            this.btnRutaCopiaArchivos.EditValue = "";
            this.btnRutaCopiaArchivos.Location = new System.Drawing.Point(11, 92);
            this.btnRutaCopiaArchivos.MenuManager = this.ribbon;
            this.btnRutaCopiaArchivos.Name = "btnRutaCopiaArchivos";
            this.btnRutaCopiaArchivos.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnRutaCopiaArchivos.Properties.Appearance.Options.UseFont = true;
            this.btnRutaCopiaArchivos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnRutaCopiaArchivos.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnRutaCopiaArchivos.Size = new System.Drawing.Size(400, 22);
            toolTipTitleItem36.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem36.Text = "Indique el directorio de sincronizaciones";
            toolTipItem24.LeftIndent = 6;
            toolTipItem24.Text = "Por favor, presione el botón que tiene \"...\", para que pueda seleccionar una carp" +
    "eta en donde se harán copias de los archivos descargados desde el servidor.";
            toolTipTitleItem37.LeftIndent = 6;
            toolTipTitleItem37.Text = "(Se crearán carpetas diferentes por cada sincronización de forma automática)";
            superToolTip24.Items.Add(toolTipTitleItem36);
            superToolTip24.Items.Add(toolTipItem24);
            superToolTip24.Items.Add(toolTipTitleItem37);
            this.btnRutaCopiaArchivos.SuperTip = superToolTip24;
            this.btnRutaCopiaArchivos.TabIndex = 22;
            this.btnRutaCopiaArchivos.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnRutaCopiaArchivos_ButtonClick);
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.Appearance.Options.UseForeColor = true;
            this.labelControl18.Location = new System.Drawing.Point(11, 70);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(216, 16);
            this.labelControl18.TabIndex = 21;
            this.labelControl18.Text = "Guardar los archivos descargados en:";
            // 
            // chkMinimizarAlIniciar
            // 
            this.chkMinimizarAlIniciar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkMinimizarAlIniciar.Location = new System.Drawing.Point(772, 126);
            this.chkMinimizarAlIniciar.MenuManager = this.ribbon;
            this.chkMinimizarAlIniciar.Name = "chkMinimizarAlIniciar";
            this.chkMinimizarAlIniciar.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkMinimizarAlIniciar.Properties.Appearance.Options.UseFont = true;
            this.chkMinimizarAlIniciar.Properties.Caption = "Minimizar SyncBD al iniciar";
            this.chkMinimizarAlIniciar.Size = new System.Drawing.Size(229, 23);
            toolTipTitleItem38.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem38.Text = "Minimizar SyncBD al iniciar";
            toolTipItem25.LeftIndent = 6;
            toolTipItem25.Text = "Si activa la casilla, la próxima vez que inicie SyncBD lo hará de forma minimizad" +
    "a mostrando solamente el icono y opciones en la barra de tareas de Windows™.";
            superToolTip25.Items.Add(toolTipTitleItem38);
            superToolTip25.Items.Add(toolTipItem25);
            this.chkMinimizarAlIniciar.SuperTip = superToolTip25;
            this.chkMinimizarAlIniciar.TabIndex = 20;
            this.chkMinimizarAlIniciar.CheckedChanged += new System.EventHandler(this.chkMinimizarAlIniciar_CheckedChanged);
            // 
            // chkIniciarConWindows
            // 
            this.chkIniciarConWindows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkIniciarConWindows.Location = new System.Drawing.Point(772, 86);
            this.chkIniciarConWindows.MenuManager = this.ribbon;
            this.chkIniciarConWindows.Name = "chkIniciarConWindows";
            this.chkIniciarConWindows.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkIniciarConWindows.Properties.Appearance.Options.UseFont = true;
            this.chkIniciarConWindows.Properties.Caption = "Iniciar SyncBD con Windows";
            this.chkIniciarConWindows.Size = new System.Drawing.Size(229, 23);
            toolTipTitleItem39.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem39.Text = "Iniciar SyncBD con Windows™";
            toolTipItem26.LeftIndent = 6;
            toolTipItem26.Text = "Si activa la casilla, SyncBD iniciará al momento de iniciar sesión Windows™ de fo" +
    "rma automática.";
            toolTipTitleItem40.LeftIndent = 6;
            toolTipTitleItem40.Text = "(Puede no funcionar en sistemas posteriores a Windows™ 7 o dependiendo su configu" +
    "ración UAC).";
            superToolTip26.Items.Add(toolTipTitleItem39);
            superToolTip26.Items.Add(toolTipItem26);
            superToolTip26.Items.Add(toolTipSeparatorItem11);
            superToolTip26.Items.Add(toolTipTitleItem40);
            this.chkIniciarConWindows.SuperTip = superToolTip26;
            this.chkIniciarConWindows.TabIndex = 19;
            this.chkIniciarConWindows.CheckedChanged += new System.EventHandler(this.chkIniciarConWindows_CheckedChanged);
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.Appearance.Options.UseForeColor = true;
            this.labelControl17.Location = new System.Drawing.Point(11, 15);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(309, 29);
            this.labelControl17.TabIndex = 11;
            this.labelControl17.Text = "Ajustes generales de SyncBD";
            // 
            // xtabAyuda
            // 
            this.xtabAyuda.Controls.Add(this.btnCrearArchivoSoporte);
            this.xtabAyuda.Controls.Add(this.labelControl5);
            this.xtabAyuda.Controls.Add(this.btnConsultarManual);
            this.xtabAyuda.Controls.Add(this.labelControl26);
            this.xtabAyuda.Controls.Add(this.pictureEdit9);
            this.xtabAyuda.Controls.Add(this.lblVersion);
            this.xtabAyuda.Controls.Add(this.labelControl24);
            this.xtabAyuda.Controls.Add(this.lblCopyright);
            this.xtabAyuda.Controls.Add(this.labelControl22);
            this.xtabAyuda.Controls.Add(this.pictureEdit8);
            this.xtabAyuda.Controls.Add(this.labelControl21);
            this.xtabAyuda.Name = "xtabAyuda";
            this.xtabAyuda.PageVisible = false;
            this.xtabAyuda.Size = new System.Drawing.Size(1008, 431);
            this.xtabAyuda.Text = "Ayuda";
            // 
            // btnCrearArchivoSoporte
            // 
            this.btnCrearArchivoSoporte.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnCrearArchivoSoporte.Appearance.Options.UseFont = true;
            this.btnCrearArchivoSoporte.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCrearArchivoSoporte.ImageOptions.Image")));
            this.btnCrearArchivoSoporte.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnCrearArchivoSoporte.Location = new System.Drawing.Point(735, 299);
            this.btnCrearArchivoSoporte.Name = "btnCrearArchivoSoporte";
            this.btnCrearArchivoSoporte.Size = new System.Drawing.Size(203, 39);
            toolTipTitleItem41.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem41.Text = "Crear archivo para soporte";
            toolTipItem27.LeftIndent = 6;
            toolTipItem27.Text = "Genera un archivo comprimido que contiene información de diagnostico que permite " +
    "al área de soporte técnico analizar posibles errores.";
            superToolTip27.Items.Add(toolTipTitleItem41);
            superToolTip27.Items.Add(toolTipItem27);
            this.btnCrearArchivoSoporte.SuperTip = superToolTip27;
            this.btnCrearArchivoSoporte.TabIndex = 32;
            this.btnCrearArchivoSoporte.Text = "Crear archivo";
            this.btnCrearArchivoSoporte.Click += new System.EventHandler(this.btnCrearArchivoSoporte_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Location = new System.Drawing.Point(735, 274);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(188, 19);
            this.labelControl5.TabIndex = 31;
            this.labelControl5.Text = "Crear archivo para soporte";
            // 
            // btnConsultarManual
            // 
            this.btnConsultarManual.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnConsultarManual.Appearance.Options.UseFont = true;
            this.btnConsultarManual.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultarManual.ImageOptions.Image")));
            this.btnConsultarManual.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnConsultarManual.Location = new System.Drawing.Point(11, 299);
            this.btnConsultarManual.Name = "btnConsultarManual";
            this.btnConsultarManual.Size = new System.Drawing.Size(203, 39);
            toolTipTitleItem42.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_24px;
            toolTipTitleItem42.Text = "Mostrar manual";
            toolTipItem28.LeftIndent = 6;
            toolTipItem28.Text = "Oculta la pantalla principal y muestra el visor de manuales.";
            toolTipTitleItem43.ImageOptions.Image = global::SyncBD.Properties.Resources.Attention_24px;
            toolTipTitleItem43.LeftIndent = 6;
            toolTipTitleItem43.Text = "En caso de no encontrar un manual valido, se cerrará la ventana.";
            superToolTip28.Items.Add(toolTipTitleItem42);
            superToolTip28.Items.Add(toolTipItem28);
            superToolTip28.Items.Add(toolTipSeparatorItem12);
            superToolTip28.Items.Add(toolTipTitleItem43);
            this.btnConsultarManual.SuperTip = superToolTip28;
            this.btnConsultarManual.TabIndex = 30;
            this.btnConsultarManual.Text = "Mostrar manual";
            this.btnConsultarManual.Click += new System.EventHandler(this.btnConsultarManual_Click);
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl26.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl26.Appearance.Options.UseFont = true;
            this.labelControl26.Appearance.Options.UseForeColor = true;
            this.labelControl26.Location = new System.Drawing.Point(11, 274);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(203, 19);
            this.labelControl26.TabIndex = 29;
            this.labelControl26.Text = "Consultar manual de usuario";
            // 
            // pictureEdit9
            // 
            this.pictureEdit9.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit9.EditValue = global::SyncBD.Properties.Resources.SyncBD;
            this.pictureEdit9.Location = new System.Drawing.Point(735, 138);
            this.pictureEdit9.Name = "pictureEdit9";
            this.pictureEdit9.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit9.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit9.Properties.ShowMenu = false;
            this.pictureEdit9.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit9.Size = new System.Drawing.Size(203, 100);
            this.pictureEdit9.TabIndex = 28;
            // 
            // lblVersion
            // 
            this.lblVersion.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblVersion.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblVersion.Appearance.Options.UseFont = true;
            this.lblVersion.Appearance.Options.UseForeColor = true;
            this.lblVersion.Location = new System.Drawing.Point(308, 213);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(153, 19);
            this.lblVersion.TabIndex = 27;
            this.lblVersion.Text = "Versión del producto:";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl24.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.Appearance.Options.UseForeColor = true;
            this.labelControl24.Location = new System.Drawing.Point(308, 188);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(218, 19);
            this.labelControl24.TabIndex = 26;
            this.labelControl24.Text = "Todos los derechos reservados";
            // 
            // lblCopyright
            // 
            this.lblCopyright.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblCopyright.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblCopyright.Appearance.Options.UseFont = true;
            this.lblCopyright.Appearance.Options.UseForeColor = true;
            this.lblCopyright.Location = new System.Drawing.Point(308, 163);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(354, 19);
            this.lblCopyright.TabIndex = 25;
            this.lblCopyright.Text = "Soluciones Digitales y de Procesos S.A. de C.V. ©";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl22.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Appearance.Options.UseForeColor = true;
            this.labelControl22.Location = new System.Drawing.Point(308, 138);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(67, 19);
            this.labelControl22.TabIndex = 24;
            this.labelControl22.Text = "SyncBD™";
            // 
            // pictureEdit8
            // 
            this.pictureEdit8.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit8.EditValue = global::SyncBD.Properties.Resources.SolucionesDigitales;
            this.pictureEdit8.Location = new System.Drawing.Point(11, 138);
            this.pictureEdit8.Name = "pictureEdit8";
            this.pictureEdit8.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit8.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit8.Properties.ShowMenu = false;
            this.pictureEdit8.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit8.Size = new System.Drawing.Size(203, 100);
            this.pictureEdit8.TabIndex = 13;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl21.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Appearance.Options.UseForeColor = true;
            this.labelControl21.Location = new System.Drawing.Point(11, 15);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(449, 29);
            this.labelControl21.TabIndex = 12;
            this.labelControl21.Text = "Centro de ayuda e información de SyncBD";
            // 
            // tmpLocalTiempoTranscurrido
            // 
            this.tmpLocalTiempoTranscurrido.Interval = 1000;
            this.tmpLocalTiempoTranscurrido.Tick += new System.EventHandler(this.tmpLocalTiempoTranscurrido_Tick);
            // 
            // tmpRemotoTiempoTranscurrido
            // 
            this.tmpRemotoTiempoTranscurrido.Interval = 1000;
            this.tmpRemotoTiempoTranscurrido.Tick += new System.EventHandler(this.tmpRemotoTiempoTranscurrido_Tick);
            // 
            // nicBarraTareas
            // 
            this.nicBarraTareas.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.nicBarraTareas.BalloonTipText = "Trabajando en segundo plano...";
            this.nicBarraTareas.ContextMenuStrip = this.cmpMenuBarraTareas;
            this.nicBarraTareas.Icon = ((System.Drawing.Icon)(resources.GetObject("nicBarraTareas.Icon")));
            this.nicBarraTareas.Text = "SyncBD";
            this.nicBarraTareas.DoubleClick += new System.EventHandler(this.nicBarraTareas_DoubleClick);
            // 
            // cmpMenuBarraTareas
            // 
            this.cmpMenuBarraTareas.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.cmpMenuBarraTareas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnVerRemotoStrip,
            this.btnVerLocalStrip,
            this.btnRestaurarStrip,
            this.btnSalirStrip});
            this.cmpMenuBarraTareas.Name = "cmpMenuSysTray";
            this.cmpMenuBarraTareas.Size = new System.Drawing.Size(171, 124);
            // 
            // btnVerRemotoStrip
            // 
            this.btnVerRemotoStrip.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRemotoStrip,
            this.btnRemotoCancelarStrip,
            this.btnPauseRemoteStrip,
            this.btnRemoteTreeStrip});
            this.btnVerRemotoStrip.Image = global::SyncBD.Properties.Resources.DownloadFromFTP_96px;
            this.btnVerRemotoStrip.Name = "btnVerRemotoStrip";
            this.btnVerRemotoStrip.Size = new System.Drawing.Size(170, 30);
            this.btnVerRemotoStrip.Text = "Servidor Remoto";
            // 
            // btnRemotoStrip
            // 
            this.btnRemotoStrip.Image = global::SyncBD.Properties.Resources.Synchronize_96px;
            this.btnRemotoStrip.Name = "btnRemotoStrip";
            this.btnRemotoStrip.Size = new System.Drawing.Size(193, 22);
            this.btnRemotoStrip.Text = "Sincronizar";
            this.btnRemotoStrip.Click += new System.EventHandler(this.btnRemotoStrip_Click);
            // 
            // btnRemotoCancelarStrip
            // 
            this.btnRemotoCancelarStrip.Image = global::SyncBD.Properties.Resources.Cancel_96px;
            this.btnRemotoCancelarStrip.Name = "btnRemotoCancelarStrip";
            this.btnRemotoCancelarStrip.Size = new System.Drawing.Size(193, 22);
            this.btnRemotoCancelarStrip.Text = "Abortar sincronización";
            this.btnRemotoCancelarStrip.Visible = false;
            this.btnRemotoCancelarStrip.Click += new System.EventHandler(this.btnRemotoCancelarStrip_Click);
            // 
            // btnPauseRemoteStrip
            // 
            this.btnPauseRemoteStrip.Name = "btnPauseRemoteStrip";
            this.btnPauseRemoteStrip.Size = new System.Drawing.Size(193, 22);
            this.btnPauseRemoteStrip.Text = "Pausar";
            this.btnPauseRemoteStrip.Visible = false;
            // 
            // btnRemoteTreeStrip
            // 
            this.btnRemoteTreeStrip.Name = "btnRemoteTreeStrip";
            this.btnRemoteTreeStrip.Size = new System.Drawing.Size(193, 22);
            this.btnRemoteTreeStrip.Text = "Obtener directorio";
            this.btnRemoteTreeStrip.Visible = false;
            // 
            // btnVerLocalStrip
            // 
            this.btnVerLocalStrip.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnLocalStrip,
            this.btnLocalCancelarStrip,
            this.btnPauseLocalStrip,
            this.btnLocalTreeStrip});
            this.btnVerLocalStrip.Image = global::SyncBD.Properties.Resources.UploadToFTP_96px;
            this.btnVerLocalStrip.Name = "btnVerLocalStrip";
            this.btnVerLocalStrip.Size = new System.Drawing.Size(170, 30);
            this.btnVerLocalStrip.Text = "Servidor Local";
            // 
            // btnLocalStrip
            // 
            this.btnLocalStrip.Image = global::SyncBD.Properties.Resources.Synchronize_96px;
            this.btnLocalStrip.Name = "btnLocalStrip";
            this.btnLocalStrip.Size = new System.Drawing.Size(193, 22);
            this.btnLocalStrip.Text = "Sincronizar";
            this.btnLocalStrip.Click += new System.EventHandler(this.btnLocalStrip_Click);
            // 
            // btnLocalCancelarStrip
            // 
            this.btnLocalCancelarStrip.Image = global::SyncBD.Properties.Resources.Cancel_96px;
            this.btnLocalCancelarStrip.Name = "btnLocalCancelarStrip";
            this.btnLocalCancelarStrip.Size = new System.Drawing.Size(193, 22);
            this.btnLocalCancelarStrip.Text = "Abortar sincronización";
            this.btnLocalCancelarStrip.Visible = false;
            this.btnLocalCancelarStrip.Click += new System.EventHandler(this.btnLocalCancelarStrip_Click);
            // 
            // btnPauseLocalStrip
            // 
            this.btnPauseLocalStrip.Name = "btnPauseLocalStrip";
            this.btnPauseLocalStrip.Size = new System.Drawing.Size(193, 22);
            this.btnPauseLocalStrip.Text = "Pausar";
            this.btnPauseLocalStrip.Visible = false;
            // 
            // btnLocalTreeStrip
            // 
            this.btnLocalTreeStrip.Name = "btnLocalTreeStrip";
            this.btnLocalTreeStrip.Size = new System.Drawing.Size(193, 22);
            this.btnLocalTreeStrip.Text = "Obtener directorio";
            this.btnLocalTreeStrip.Visible = false;
            // 
            // btnRestaurarStrip
            // 
            this.btnRestaurarStrip.Image = global::SyncBD.Properties.Resources.SyncBDLogo;
            this.btnRestaurarStrip.Name = "btnRestaurarStrip";
            this.btnRestaurarStrip.Size = new System.Drawing.Size(170, 30);
            this.btnRestaurarStrip.Text = "Mostrar SyncBD";
            this.btnRestaurarStrip.Click += new System.EventHandler(this.btnRestaurarStrip_Click);
            // 
            // btnSalirStrip
            // 
            this.btnSalirStrip.Image = global::SyncBD.Properties.Resources.CloseWindow_96px;
            this.btnSalirStrip.Name = "btnSalirStrip";
            this.btnSalirStrip.Size = new System.Drawing.Size(170, 30);
            this.btnSalirStrip.Text = "Salir";
            this.btnSalirStrip.Click += new System.EventHandler(this.btnSalirStrip_Click);
            // 
            // fbdGuardarEn
            // 
            this.fbdGuardarEn.AllowDragDrop = false;
            this.fbdGuardarEn.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.fbdGuardarEn.Title = "Seleccione donde guardar los archivos...";
            // 
            // toastMensaje
            // 
            this.toastMensaje.ApplicationIconPath = "";
            this.toastMensaje.ApplicationId = "8bd96da6-487e-4618-8479-3129f28bb6f4";
            this.toastMensaje.ApplicationName = "SyncBD";
            this.toastMensaje.CreateApplicationShortcut = DevExpress.Utils.DefaultBoolean.True;
            this.toastMensaje.Notifications.AddRange(new DevExpress.XtraBars.ToastNotifications.IToastNotificationProperties[] {
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("c70f4e9a-079b-4392-bb24-fadcd7d6637f", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x002", "Imposible determinar ajustes de los servidores", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("e7fde073-cab6-4894-be34-3df31dad0cc1", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x003", "Error en lectura de configuraciones previas...", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7c4746bd-9688-48e5-98c5-3fa2f722aac0", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x004", "Ajustes inválidos o datos incorrectos", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("d13c592d-f20c-49a3-85b4-aae22336845f", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x005", "Datos de configuracion inválidos o de formato incorrecto.", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("fec336e8-324b-488e-8a20-60fc4f584782", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x001", "Ajustes previos dañados o incorrectos", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("ff6c06c6-fd79-4194-b90c-6f013533ebd5", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x006", "Imposible leer los ajustes previos del sistema", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("8a944fa0-ffb7-4073-b868-f2c55f8f47e3", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x007", "No se completo el guardado de ajustes", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("2090d200-7ac9-4ca8-9c55-026ef9af8e3b", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x008", "Imposible la restauración de ajustes", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7ae421fc-fa8b-4585-8166-f17641bde57b", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x009", "Ocurrió un problema al obtener el historial", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("38dc66bf-5cf8-4644-8b4f-48179785bdb2", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x010", "Imposible encontrar el destino del historial", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("a598dbf0-8fda-483c-b658-a17c4b71ce3c", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x011", "Ocurrió un error en la transferencia de archivos", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("503a9e59-8116-44c1-9eb8-57af2664e621", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x012", "Se detuvo de forma inesperada la transferencia de archivos", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("30213a7f-c354-4e9c-80bd-d539a236bc75", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x013", "Existe un error en en el sistema que impide el correcto funcionamiento", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7fbfedb5-3de5-4b6d-b506-69ad5af656a7", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x014", "Conexión inestable, sincronización cancelada.", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("ec1fddba-1bf7-4b86-b063-5a5c4e126f94", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x015", "Imposible acceder a los registros de Windows™", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("60d5b5f9-1a0d-4cbf-b476-5416dc7e1aa1", global::SyncBD.Properties.Resources.Attention_96px, "Servidor remoto sin configurar...", "No hay configuraciones del servidor remoto", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7d1f937f-505c-4e27-a3ca-91b15dfe3b7f", global::SyncBD.Properties.Resources.Info_96px, "Iniciando conexión...", "Sincronizando con servidor remoto", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7d71d6f5-9065-462e-be8e-b7689b1286b4", global::SyncBD.Properties.Resources.Ok_96px, "Sincronización finalizada", "Se ha completado la sincronización con el servidor Remoto", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("491ef6d1-c744-4e74-9c71-892871442416", global::SyncBD.Properties.Resources.Ok_96px, "Archivos transferidos", "Se han transferido los archivos al directorio copia", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("da753166-2726-4f36-abc3-447013054625", global::SyncBD.Properties.Resources.Attention_96px, "No se transfirieron los archivos", "Ocurrió un imprevisto al transferir los archivos", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("573fd277-ec9b-4b37-b5ba-5c1a63fea667", global::SyncBD.Properties.Resources.Attention_96px, "Sin directorio de guardado", "No ha configurado la dirección de guardado de archivos", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("b9a553cc-13bb-42d2-b22d-8d5fa3c50f96", global::SyncBD.Properties.Resources.Ok_96px, "Sin copia de respaldo", "Debido a que no hay cambios, puede utilizar la última carpeta copiada", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("1153088b-b9a3-44e5-97a3-c564af28c277", global::SyncBD.Properties.Resources.Attention_96px, "Sincronización incompleta", "No se pudo completar la sincronización con el servidor Remoto", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("162af2bb-3646-4169-b4b8-2f62029ca924", global::SyncBD.Properties.Resources.Attention_96px, "Servidor inaccesible", "Se perdió la comunicación con el servidor Remoto, reintente por favor", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("750a957c-b003-44a9-8719-94e4ea8937f4", global::SyncBD.Properties.Resources.Attention_96px, "Faltan datos de conexión", "El servidor remoto no tiene configuración", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("9a9ab089-a67b-4adf-9889-814a01d0272c", global::SyncBD.Properties.Resources.Attention_96px, "Servidor local sin configurar...", "No hay configuraciones del servidor local", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("9f42577f-c0a3-418f-9335-29a3f21da934", global::SyncBD.Properties.Resources.Info_96px, "Iniciando conexión...", "Sincronizando con servidor local", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("c11b976a-cfdc-4083-9189-95333ae7e817", global::SyncBD.Properties.Resources.Ok_96px, "Sincronización finalizada", "Se ha completado la sincronización con el servidor Local", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7df226a8-c995-4f75-aef5-18c5ee37df87", global::SyncBD.Properties.Resources.Attention_96px, "Verificando ajustes", "Reintente sincronización", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7b0a8260-5b68-4d49-99bf-dd017b3a97ad", global::SyncBD.Properties.Resources.Attention_96px, "Servidor inaccesible", "Se perdió la comunicación con el servidor Local, reintente por favor", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("9f7815a5-0024-423b-9f0e-6997cc7dc8ca", global::SyncBD.Properties.Resources.Attention_96px, "Faltan datos de conexión", "El servidor local no tiene configuración", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("c125ca7a-d06c-4929-9db7-7092be53d953", global::SyncBD.Properties.Resources.Ok_96px, "Historial borrado", "Se ha borrado el todo el historial correctamente", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("ba24a13a-bd76-4921-bd25-943daa99bf83", global::SyncBD.Properties.Resources.Attention_96px, "No se borró historial", "No fue posible borrar todo el historial", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("cf44c41e-cd73-43d9-be56-e455864c91e3", global::SyncBD.Properties.Resources.Info_96px, "Ocultando SyncBD", "SyncBD trabajará en segundo plano, restaurelo en la barra de tareas", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("89ca573d-e9e1-4618-9fdb-7341782c9cff", global::SyncBD.Properties.Resources.Ok_96px, "Historial borrado", "Se ha borrado el historial del servidor Remoto", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("a0a8af7c-0d28-4a95-9a33-4c15f51134f4", global::SyncBD.Properties.Resources.Attention_96px, "No se borró historial", "No fue posible borrar el historial del servidor Remoto", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("fc942a97-627b-43fe-ba36-f8566fe155be", global::SyncBD.Properties.Resources.Ok_96px, "Historial borrado", "Se ha borrado el historial del servidor Local", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("b10313c7-e306-4c05-978d-9ffc018f3abc", global::SyncBD.Properties.Resources.Attention_96px, "No se borró historial", "No fue posible borrar el historial del servidor Local", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("0f721c13-71e8-4e2a-9dd3-c4081d343420", global::SyncBD.Properties.Resources.Ok_96px, "Sincronización cancelada", "Se canceló la sincronización correctamente", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("357ab85e-a57b-4aa1-a88d-39b1c0d1eb3e", global::SyncBD.Properties.Resources.Attention_96px, "Imposible cancelar", "Imposible cancelar la sincronización", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03)});
            // 
            // alertMensaje
            // 
            this.alertMensaje.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.alertMensaje.AppearanceCaption.Options.UseFont = true;
            this.alertMensaje.AppearanceCaption.Options.UseTextOptions = true;
            this.alertMensaje.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.alertMensaje.AppearanceText.Font = new System.Drawing.Font("Tahoma", 9F);
            this.alertMensaje.AppearanceText.Options.UseFont = true;
            this.alertMensaje.AppearanceText.Options.UseTextOptions = true;
            this.alertMensaje.AppearanceText.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.alertMensaje.AutoFormDelay = 4000;
            this.alertMensaje.ControlBoxPosition = DevExpress.XtraBars.Alerter.AlertFormControlBoxPosition.Right;
            this.alertMensaje.ShowPinButton = false;
            // 
            // SSMCancelando
            // 
            this.SSMCancelando.ClosingDelay = 500;
            // 
            // SSMDirVerificacion
            // 
            this.SSMDirVerificacion.ClosingDelay = 500;
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 599);
            this.Controls.Add(this.xtabPrincipal);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1024, 600);
            this.Name = "Principal";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "SyncBD - Sincroniza tus servidores de datos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Principal_FormClosing);
            this.Shown += new System.EventHandler(this.Principal_Shown);
            this.Resize += new System.EventHandler(this.Principal_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtabPrincipal)).EndInit();
            this.xtabPrincipal.ResumeLayout(false);
            this.xtabRemoto.ResumeLayout(false);
            this.xtabRemoto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.petInfoRemoto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbcAccionesRemoto)).EndInit();
            this.gbcAccionesRemoto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbcPanelInformativoRemoto)).EndInit();
            this.gbcPanelInformativoRemoto.ResumeLayout(false);
            this.gbcPanelInformativoRemoto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbrProgresoArchivoRemoto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbrProgresoTotalRemoto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.petAccionesRemoto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdcHistorialRemoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdvHistorialRemoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.petLogo.Properties)).EndInit();
            this.xtabLocal.ResumeLayout(false);
            this.xtabLocal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.petInfoLocal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbcAccionesLocal)).EndInit();
            this.gbcAccionesLocal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbcPanelInformativoLocal)).EndInit();
            this.gbcPanelInformativoLocal.ResumeLayout(false);
            this.gbcPanelInformativoLocal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbrProgresoArchivoLocal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbrProgresoTotalLocal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.petAccionesLocal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdcHistorialLocal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdvHistorialLocal)).EndInit();
            this.xtabAjustes.ResumeLayout(false);
            this.xtabAjustes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiempoReconexion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiempoDesconexion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popMHistoriales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxApariencia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRutaCopiaArchivos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMinimizarAlIniciar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIniciarConWindows.Properties)).EndInit();
            this.xtabAyuda.ResumeLayout(false);
            this.xtabAyuda.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).EndInit();
            this.cmpMenuBarraTareas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.toastMensaje)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraTab.XtraTabControl xtabPrincipal;
        private DevExpress.XtraTab.XtraTabPage xtabRemoto;
        private DevExpress.XtraTab.XtraTabPage xtabLocal;
        private DevExpress.XtraBars.BarButtonItem btnMenuRemoto;
        private DevExpress.XtraBars.BarButtonItem btnMenuLocal;
        private DevExpress.XtraBars.BarButtonItem btnMenuAjustes;
        private DevExpress.XtraBars.BarButtonItem btnMenuAyuda;
        private DevExpress.XtraBars.BarButtonItem btnMenuSalir;
        private DevExpress.XtraBars.BarStaticItem bsiVersion;
        private DevExpress.XtraTab.XtraTabPage xtabAjustes;
        private DevExpress.XtraTab.XtraTabPage xtabAyuda;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PictureEdit petLogo;
        private DevExpress.XtraEditors.PictureEdit petAccionesRemoto;
        private DevExpress.XtraEditors.LabelControl lblAccionesRemoto;
        private DevExpress.XtraGrid.GridControl gdcHistorialRemoto;
        private DevExpress.XtraGrid.Views.Grid.GridView gdvHistorialRemoto;
        private DevExpress.XtraEditors.GroupControl gbcAccionesRemoto;
        private DevExpress.XtraEditors.GroupControl gbcPanelInformativoRemoto;
        private DevExpress.XtraEditors.SimpleButton btnAbrirCarpetaFinal;
        private DevExpress.XtraEditors.SimpleButton btnSincronizarRemoto;
        private DevExpress.XtraEditors.LabelControl lblVelocidadRemoto;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.LabelControl lblTiempoRemoto;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.LabelControl lblNombreArchivoRemoto;
        private DevExpress.XtraEditors.ProgressBarControl pbrProgresoArchivoRemoto;
        private DevExpress.XtraEditors.LabelControl lblProgresoArchivoRemoto;
        private DevExpress.XtraEditors.ProgressBarControl pbrProgresoTotalRemoto;
        private DevExpress.XtraEditors.LabelControl lblProgresoTotalRemoto;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.GroupControl gbcAccionesLocal;
        private DevExpress.XtraEditors.SimpleButton btnCargarArchivosDesde;
        private DevExpress.XtraEditors.SimpleButton btnSincronizarLocal;
        private DevExpress.XtraEditors.GroupControl gbcPanelInformativoLocal;
        private DevExpress.XtraEditors.LabelControl lblNombreArchivoLocal;
        private DevExpress.XtraEditors.ProgressBarControl pbrProgresoArchivoLocal;
        private DevExpress.XtraEditors.LabelControl lblProgresoArchivoLocal;
        private DevExpress.XtraEditors.ProgressBarControl pbrProgresoTotalLocal;
        private DevExpress.XtraEditors.LabelControl lblProgresoTotalLocal;
        private DevExpress.XtraEditors.LabelControl lblVelocidadLocal;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.LabelControl lblTiempoLocal;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.PictureEdit petAccionesLocal;
        private DevExpress.XtraEditors.LabelControl lblAccionesLocal;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraGrid.GridControl gdcHistorialLocal;
        private DevExpress.XtraGrid.Views.Grid.GridView gdvHistorialLocal;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.CheckEdit chkMinimizarAlIniciar;
        private DevExpress.XtraEditors.CheckEdit chkIniciarConWindows;
        private DevExpress.XtraEditors.ButtonEdit btnRutaCopiaArchivos;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.SimpleButton btnConfigurarLocal;
        private DevExpress.XtraEditors.SimpleButton btnConfigurarRemoto;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.ComboBoxEdit cbxApariencia;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.SimpleButton btnReestablecerAjustes;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.PictureEdit pictureEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.PictureEdit pictureEdit9;
        private DevExpress.XtraEditors.LabelControl lblVersion;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl lblCopyright;
        private DevExpress.XtraEditors.SimpleButton btnConsultarManual;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraBars.BarButtonItem btnSubBorrarHistorialRemoto;
        private DevExpress.XtraBars.BarButtonItem btnSubBorrarHistorialLocal;
        private DevExpress.XtraEditors.DropDownButton btnBorrarHistoriales;
        private DevExpress.XtraBars.PopupMenu popMHistoriales;
        private System.Windows.Forms.Timer tmpLocalTiempoTranscurrido;
        private System.Windows.Forms.Timer tmpRemotoTiempoTranscurrido;
        private System.Windows.Forms.NotifyIcon nicBarraTareas;
        private System.Windows.Forms.ContextMenuStrip cmpMenuBarraTareas;
        private System.Windows.Forms.ToolStripMenuItem btnVerRemotoStrip;
        private System.Windows.Forms.ToolStripMenuItem btnRemotoStrip;
        private System.Windows.Forms.ToolStripMenuItem btnPauseRemoteStrip;
        private System.Windows.Forms.ToolStripMenuItem btnRemotoCancelarStrip;
        private System.Windows.Forms.ToolStripMenuItem btnRemoteTreeStrip;
        private System.Windows.Forms.ToolStripMenuItem btnVerLocalStrip;
        private System.Windows.Forms.ToolStripMenuItem btnLocalStrip;
        private System.Windows.Forms.ToolStripMenuItem btnPauseLocalStrip;
        private System.Windows.Forms.ToolStripMenuItem btnLocalCancelarStrip;
        private System.Windows.Forms.ToolStripMenuItem btnLocalTreeStrip;
        private System.Windows.Forms.ToolStripMenuItem btnRestaurarStrip;
        private System.Windows.Forms.ToolStripMenuItem btnSalirStrip;
        private DevExpress.XtraEditors.XtraFolderBrowserDialog fbdGuardarEn;
        private DevExpress.XtraEditors.PictureEdit petInfoRemoto;
        private DevExpress.XtraEditors.LabelControl lblInfoRemoto;
        private DevExpress.XtraEditors.PictureEdit petInfoLocal;
        private DevExpress.XtraEditors.LabelControl lblInfoLocal;
        private DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager toastMensaje;
        private DevExpress.XtraBars.Alerter.AlertControl alertMensaje;
        private DevExpress.XtraEditors.SimpleButton btnAbortarRemoto;
        private DevExpress.XtraEditors.SimpleButton btnAbortarLocal;
        private DevExpress.XtraSplashScreen.SplashScreenManager SSMCancelando;
        private DevExpress.XtraEditors.TextEdit txtTiempoDesconexion;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtTiempoReconexion;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnCrearArchivoSoporte;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraSplashScreen.SplashScreenManager SSMDirVerificacion;
    }
}