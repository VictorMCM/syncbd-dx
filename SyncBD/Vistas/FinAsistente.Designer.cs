﻿namespace SyncBD.Vistas
{
    partial class FinAsistente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FinAsistente));
            this.pnlPrincipal = new DevExpress.XtraEditors.PanelControl();
            this.gbcLocal = new DevExpress.XtraEditors.GroupControl();
            this.lblDirectorio2 = new DevExpress.XtraEditors.LabelControl();
            this.lblUsuario2 = new DevExpress.XtraEditors.LabelControl();
            this.lblServidor2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.gbcRemoto = new DevExpress.XtraEditors.GroupControl();
            this.lblDirectorio1 = new DevExpress.XtraEditors.LabelControl();
            this.lblUsuario1 = new DevExpress.XtraEditors.LabelControl();
            this.lblServidor1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnContinuar = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pnlIzquierdo = new DevExpress.XtraEditors.PanelControl();
            this.petLogo = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPrincipal)).BeginInit();
            this.pnlPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbcLocal)).BeginInit();
            this.gbcLocal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbcRemoto)).BeginInit();
            this.gbcRemoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlIzquierdo)).BeginInit();
            this.pnlIzquierdo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.petLogo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlPrincipal
            // 
            this.pnlPrincipal.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlPrincipal.Controls.Add(this.gbcLocal);
            this.pnlPrincipal.Controls.Add(this.gbcRemoto);
            this.pnlPrincipal.Controls.Add(this.btnContinuar);
            this.pnlPrincipal.Controls.Add(this.labelControl3);
            this.pnlPrincipal.Controls.Add(this.labelControl1);
            this.pnlPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPrincipal.Location = new System.Drawing.Point(180, 0);
            this.pnlPrincipal.Name = "pnlPrincipal";
            this.pnlPrincipal.Size = new System.Drawing.Size(604, 561);
            this.pnlPrincipal.TabIndex = 3;
            // 
            // gbcLocal
            // 
            this.gbcLocal.AppearanceCaption.Options.UseTextOptions = true;
            this.gbcLocal.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbcLocal.CaptionImageOptions.Image = global::SyncBD.Properties.Resources.UploadToFTP_16px;
            this.gbcLocal.Controls.Add(this.lblDirectorio2);
            this.gbcLocal.Controls.Add(this.lblUsuario2);
            this.gbcLocal.Controls.Add(this.lblServidor2);
            this.gbcLocal.Controls.Add(this.labelControl12);
            this.gbcLocal.Controls.Add(this.labelControl13);
            this.gbcLocal.Controls.Add(this.labelControl14);
            this.gbcLocal.Location = new System.Drawing.Point(10, 310);
            this.gbcLocal.Name = "gbcLocal";
            this.gbcLocal.Size = new System.Drawing.Size(584, 163);
            this.gbcLocal.TabIndex = 24;
            this.gbcLocal.Text = "Datos del servidor local";
            // 
            // lblDirectorio2
            // 
            this.lblDirectorio2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDirectorio2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblDirectorio2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblDirectorio2.Appearance.Options.UseFont = true;
            this.lblDirectorio2.Appearance.Options.UseForeColor = true;
            this.lblDirectorio2.Location = new System.Drawing.Point(201, 121);
            this.lblDirectorio2.Name = "lblDirectorio2";
            this.lblDirectorio2.Size = new System.Drawing.Size(80, 16);
            this.lblDirectorio2.TabIndex = 16;
            this.lblDirectorio2.Text = "Sin configurar";
            // 
            // lblUsuario2
            // 
            this.lblUsuario2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUsuario2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblUsuario2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblUsuario2.Appearance.Options.UseFont = true;
            this.lblUsuario2.Appearance.Options.UseForeColor = true;
            this.lblUsuario2.Location = new System.Drawing.Point(201, 83);
            this.lblUsuario2.Name = "lblUsuario2";
            this.lblUsuario2.Size = new System.Drawing.Size(80, 16);
            this.lblUsuario2.TabIndex = 15;
            this.lblUsuario2.Text = "Sin configurar";
            // 
            // lblServidor2
            // 
            this.lblServidor2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblServidor2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblServidor2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblServidor2.Appearance.Options.UseFont = true;
            this.lblServidor2.Appearance.Options.UseForeColor = true;
            this.lblServidor2.Location = new System.Drawing.Point(201, 45);
            this.lblServidor2.Name = "lblServidor2";
            this.lblServidor2.Size = new System.Drawing.Size(80, 16);
            this.lblServidor2.TabIndex = 14;
            this.lblServidor2.Text = "Sin configurar";
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Location = new System.Drawing.Point(23, 118);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(160, 19);
            this.labelControl12.TabIndex = 13;
            this.labelControl12.Text = "Directorio de archivos:";
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Appearance.Options.UseForeColor = true;
            this.labelControl13.Location = new System.Drawing.Point(123, 80);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(60, 19);
            this.labelControl13.TabIndex = 12;
            this.labelControl13.Text = "Usuario:";
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.Location = new System.Drawing.Point(24, 42);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(159, 19);
            this.labelControl14.TabIndex = 11;
            this.labelControl14.Text = "Dirección del servidor:";
            // 
            // gbcRemoto
            // 
            this.gbcRemoto.AppearanceCaption.Options.UseTextOptions = true;
            this.gbcRemoto.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbcRemoto.CaptionImageOptions.Image = global::SyncBD.Properties.Resources.DownloadFromFTP_16px;
            this.gbcRemoto.Controls.Add(this.lblDirectorio1);
            this.gbcRemoto.Controls.Add(this.lblUsuario1);
            this.gbcRemoto.Controls.Add(this.lblServidor1);
            this.gbcRemoto.Controls.Add(this.labelControl5);
            this.gbcRemoto.Controls.Add(this.labelControl4);
            this.gbcRemoto.Controls.Add(this.labelControl2);
            this.gbcRemoto.Location = new System.Drawing.Point(10, 141);
            this.gbcRemoto.Name = "gbcRemoto";
            this.gbcRemoto.Size = new System.Drawing.Size(584, 163);
            this.gbcRemoto.TabIndex = 23;
            this.gbcRemoto.Text = "Datos del servidor remoto";
            // 
            // lblDirectorio1
            // 
            this.lblDirectorio1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDirectorio1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblDirectorio1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblDirectorio1.Appearance.Options.UseFont = true;
            this.lblDirectorio1.Appearance.Options.UseForeColor = true;
            this.lblDirectorio1.Location = new System.Drawing.Point(201, 121);
            this.lblDirectorio1.Name = "lblDirectorio1";
            this.lblDirectorio1.Size = new System.Drawing.Size(80, 16);
            this.lblDirectorio1.TabIndex = 16;
            this.lblDirectorio1.Text = "Sin configurar";
            // 
            // lblUsuario1
            // 
            this.lblUsuario1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUsuario1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblUsuario1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblUsuario1.Appearance.Options.UseFont = true;
            this.lblUsuario1.Appearance.Options.UseForeColor = true;
            this.lblUsuario1.Location = new System.Drawing.Point(201, 83);
            this.lblUsuario1.Name = "lblUsuario1";
            this.lblUsuario1.Size = new System.Drawing.Size(80, 16);
            this.lblUsuario1.TabIndex = 15;
            this.lblUsuario1.Text = "Sin configurar";
            // 
            // lblServidor1
            // 
            this.lblServidor1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblServidor1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblServidor1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblServidor1.Appearance.Options.UseFont = true;
            this.lblServidor1.Appearance.Options.UseForeColor = true;
            this.lblServidor1.Location = new System.Drawing.Point(201, 45);
            this.lblServidor1.Name = "lblServidor1";
            this.lblServidor1.Size = new System.Drawing.Size(80, 16);
            this.lblServidor1.TabIndex = 14;
            this.lblServidor1.Text = "Sin configurar";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Location = new System.Drawing.Point(23, 118);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(160, 19);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "Directorio de archivos:";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(123, 80);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 19);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Usuario:";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(24, 42);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(159, 19);
            this.labelControl2.TabIndex = 11;
            this.labelControl2.Text = "Dirección del servidor:";
            // 
            // btnContinuar
            // 
            this.btnContinuar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnContinuar.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnContinuar.Appearance.Options.UseFont = true;
            this.btnContinuar.ImageOptions.Image = global::SyncBD.Properties.Resources.Ok_16px;
            this.btnContinuar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnContinuar.Location = new System.Drawing.Point(455, 510);
            this.btnContinuar.Name = "btnContinuar";
            this.btnContinuar.Size = new System.Drawing.Size(139, 39);
            this.btnContinuar.TabIndex = 1;
            this.btnContinuar.Text = "Continuar";
            this.btnContinuar.Click += new System.EventHandler(this.btnContinuar_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(10, 87);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(550, 38);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "Ha configurado exitosamente los ajustes, sí posteriormente desea cambiarlos;\r\ndir" +
    "ijase al botón ajustes del menú principal.";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(10, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(448, 29);
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "Configuraciones guardadas correctamente";
            // 
            // pnlIzquierdo
            // 
            this.pnlIzquierdo.Controls.Add(this.petLogo);
            this.pnlIzquierdo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlIzquierdo.Location = new System.Drawing.Point(0, 0);
            this.pnlIzquierdo.Name = "pnlIzquierdo";
            this.pnlIzquierdo.Size = new System.Drawing.Size(180, 561);
            this.pnlIzquierdo.TabIndex = 2;
            // 
            // petLogo
            // 
            this.petLogo.Cursor = System.Windows.Forms.Cursors.Default;
            this.petLogo.EditValue = global::SyncBD.Properties.Resources.SyncBD;
            this.petLogo.Location = new System.Drawing.Point(5, 37);
            this.petLogo.Name = "petLogo";
            this.petLogo.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.petLogo.Properties.Appearance.Options.UseBackColor = true;
            this.petLogo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.petLogo.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.petLogo.Properties.ShowMenu = false;
            this.petLogo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.petLogo.Size = new System.Drawing.Size(170, 96);
            this.petLogo.TabIndex = 0;
            // 
            // FinAsistente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pnlPrincipal);
            this.Controls.Add(this.pnlIzquierdo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "FinAsistente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SyncBD - Asistente de configuración";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FinAsistente_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPrincipal)).EndInit();
            this.pnlPrincipal.ResumeLayout(false);
            this.pnlPrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbcLocal)).EndInit();
            this.gbcLocal.ResumeLayout(false);
            this.gbcLocal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbcRemoto)).EndInit();
            this.gbcRemoto.ResumeLayout(false);
            this.gbcRemoto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlIzquierdo)).EndInit();
            this.pnlIzquierdo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.petLogo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlPrincipal;
        private DevExpress.XtraEditors.SimpleButton btnContinuar;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl pnlIzquierdo;
        private DevExpress.XtraEditors.PictureEdit petLogo;
        private DevExpress.XtraEditors.GroupControl gbcLocal;
        private DevExpress.XtraEditors.LabelControl lblDirectorio2;
        private DevExpress.XtraEditors.LabelControl lblUsuario2;
        private DevExpress.XtraEditors.LabelControl lblServidor2;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.GroupControl gbcRemoto;
        private DevExpress.XtraEditors.LabelControl lblDirectorio1;
        private DevExpress.XtraEditors.LabelControl lblUsuario1;
        private DevExpress.XtraEditors.LabelControl lblServidor1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
    }
}