﻿namespace SyncBD.Vistas
{
    partial class ConfigRemoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigRemoto));
            this.pnlPrincipal = new DevExpress.XtraEditors.PanelControl();
            this.chkAjustesAvanzados = new DevExpress.XtraEditors.CheckEdit();
            this.gbcAjustesAvanzados = new DevExpress.XtraEditors.GroupControl();
            this.chkModoActivo = new DevExpress.XtraEditors.CheckEdit();
            this.chkAnonimo = new DevExpress.XtraEditors.CheckEdit();
            this.chkAutenticacion = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cbxEncriptacion = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblEncriptacion = new DevExpress.XtraEditors.LabelControl();
            this.txtDirectorio = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.pbxVerPassword = new DevExpress.XtraEditors.PictureEdit();
            this.txtPassword = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtUsuario = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtPuerto = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtServidor = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cbxProtocolo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnAyuda = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnContinuar = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pnlIzquierdo = new DevExpress.XtraEditors.PanelControl();
            this.petLogo = new DevExpress.XtraEditors.PictureEdit();
            this.SSMOcupado = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::SyncBD.Vistas.Procesando), true, true);
            this.toastMensaje = new DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager(this.components);
            this.alertMensaje = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPrincipal)).BeginInit();
            this.pnlPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAjustesAvanzados.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbcAjustesAvanzados)).BeginInit();
            this.gbcAjustesAvanzados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkModoActivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAnonimo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutenticacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxEncriptacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirectorio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxVerPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsuario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPuerto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServidor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxProtocolo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlIzquierdo)).BeginInit();
            this.pnlIzquierdo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.petLogo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toastMensaje)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlPrincipal
            // 
            this.pnlPrincipal.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlPrincipal.Controls.Add(this.chkAjustesAvanzados);
            this.pnlPrincipal.Controls.Add(this.gbcAjustesAvanzados);
            this.pnlPrincipal.Controls.Add(this.groupControl1);
            this.pnlPrincipal.Controls.Add(this.cbxProtocolo);
            this.pnlPrincipal.Controls.Add(this.labelControl2);
            this.pnlPrincipal.Controls.Add(this.btnAyuda);
            this.pnlPrincipal.Controls.Add(this.btnCancelar);
            this.pnlPrincipal.Controls.Add(this.btnContinuar);
            this.pnlPrincipal.Controls.Add(this.labelControl3);
            this.pnlPrincipal.Controls.Add(this.labelControl1);
            this.pnlPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPrincipal.Location = new System.Drawing.Point(180, 0);
            this.pnlPrincipal.Name = "pnlPrincipal";
            this.pnlPrincipal.Size = new System.Drawing.Size(604, 561);
            this.pnlPrincipal.TabIndex = 5;
            // 
            // chkAjustesAvanzados
            // 
            this.chkAjustesAvanzados.Location = new System.Drawing.Point(407, 388);
            this.chkAjustesAvanzados.Name = "chkAjustesAvanzados";
            this.chkAjustesAvanzados.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkAjustesAvanzados.Properties.Appearance.Options.UseFont = true;
            this.chkAjustesAvanzados.Properties.Caption = "Mostrar ajustes avanzados";
            this.chkAjustesAvanzados.Size = new System.Drawing.Size(187, 20);
            this.chkAjustesAvanzados.TabIndex = 8;
            this.chkAjustesAvanzados.CheckedChanged += new System.EventHandler(this.chkAjustesAvanzados_CheckedChanged);
            // 
            // gbcAjustesAvanzados
            // 
            this.gbcAjustesAvanzados.AppearanceCaption.Options.UseTextOptions = true;
            this.gbcAjustesAvanzados.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbcAjustesAvanzados.CaptionImageOptions.Image = global::SyncBD.Properties.Resources.Services_16px;
            this.gbcAjustesAvanzados.Controls.Add(this.chkModoActivo);
            this.gbcAjustesAvanzados.Controls.Add(this.chkAnonimo);
            this.gbcAjustesAvanzados.Controls.Add(this.chkAutenticacion);
            this.gbcAjustesAvanzados.Location = new System.Drawing.Point(10, 414);
            this.gbcAjustesAvanzados.Name = "gbcAjustesAvanzados";
            this.gbcAjustesAvanzados.Size = new System.Drawing.Size(584, 90);
            this.gbcAjustesAvanzados.TabIndex = 23;
            this.gbcAjustesAvanzados.Text = "Ajustes avanzados de conexión";
            this.gbcAjustesAvanzados.Visible = false;
            // 
            // chkModoActivo
            // 
            this.chkModoActivo.Location = new System.Drawing.Point(410, 44);
            this.chkModoActivo.Name = "chkModoActivo";
            this.chkModoActivo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkModoActivo.Properties.Appearance.Options.UseFont = true;
            this.chkModoActivo.Properties.Caption = "Establecer modo activo";
            this.chkModoActivo.Size = new System.Drawing.Size(158, 20);
            this.chkModoActivo.TabIndex = 11;
            // 
            // chkAnonimo
            // 
            this.chkAnonimo.Location = new System.Drawing.Point(17, 44);
            this.chkAnonimo.Name = "chkAnonimo";
            this.chkAnonimo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkAnonimo.Properties.Appearance.Options.UseFont = true;
            this.chkAnonimo.Properties.Caption = "Ingresar como anónimo";
            this.chkAnonimo.Size = new System.Drawing.Size(164, 20);
            this.chkAnonimo.TabIndex = 9;
            this.chkAnonimo.CheckedChanged += new System.EventHandler(this.chkAnonimo_CheckedChanged);
            // 
            // chkAutenticacion
            // 
            this.chkAutenticacion.Location = new System.Drawing.Point(227, 44);
            this.chkAutenticacion.Name = "chkAutenticacion";
            this.chkAutenticacion.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkAutenticacion.Properties.Appearance.Options.UseFont = true;
            this.chkAutenticacion.Properties.Caption = "Quitar autenticación";
            this.chkAutenticacion.Size = new System.Drawing.Size(156, 20);
            this.chkAutenticacion.TabIndex = 10;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.CaptionImageOptions.Image = global::SyncBD.Properties.Resources.Settings3_16px;
            this.groupControl1.Controls.Add(this.cbxEncriptacion);
            this.groupControl1.Controls.Add(this.lblEncriptacion);
            this.groupControl1.Controls.Add(this.txtDirectorio);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.pbxVerPassword);
            this.groupControl1.Controls.Add(this.txtPassword);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtUsuario);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txtPuerto);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.txtServidor);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Location = new System.Drawing.Point(10, 197);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(584, 185);
            this.groupControl1.TabIndex = 22;
            this.groupControl1.Text = "Datos de conexión";
            // 
            // cbxEncriptacion
            // 
            this.cbxEncriptacion.EditValue = "Sin encriptación";
            this.cbxEncriptacion.Location = new System.Drawing.Point(354, 127);
            this.cbxEncriptacion.Name = "cbxEncriptacion";
            this.cbxEncriptacion.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbxEncriptacion.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.cbxEncriptacion.Properties.Appearance.Options.UseFont = true;
            this.cbxEncriptacion.Properties.Appearance.Options.UseForeColor = true;
            this.cbxEncriptacion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxEncriptacion.Properties.Items.AddRange(new object[] {
            "Sin encriptación",
            "TLS/SSL Encriptación implícita",
            "TLS/SSL Encriptación explícita"});
            this.cbxEncriptacion.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxEncriptacion.Size = new System.Drawing.Size(196, 22);
            this.cbxEncriptacion.TabIndex = 7;
            // 
            // lblEncriptacion
            // 
            this.lblEncriptacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEncriptacion.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblEncriptacion.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblEncriptacion.Appearance.Options.UseFont = true;
            this.lblEncriptacion.Appearance.Options.UseForeColor = true;
            this.lblEncriptacion.Location = new System.Drawing.Point(274, 130);
            this.lblEncriptacion.Name = "lblEncriptacion";
            this.lblEncriptacion.Size = new System.Drawing.Size(74, 16);
            this.lblEncriptacion.TabIndex = 32;
            this.lblEncriptacion.Text = "Encriptación:";
            // 
            // txtDirectorio
            // 
            this.txtDirectorio.Location = new System.Drawing.Point(76, 127);
            this.txtDirectorio.Name = "txtDirectorio";
            this.txtDirectorio.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDirectorio.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtDirectorio.Properties.Appearance.Options.UseFont = true;
            this.txtDirectorio.Properties.Appearance.Options.UseForeColor = true;
            this.txtDirectorio.Size = new System.Drawing.Size(188, 22);
            this.txtDirectorio.TabIndex = 6;
            this.txtDirectorio.Leave += new System.EventHandler(this.txtServidor_Leave);
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Location = new System.Drawing.Point(10, 130);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 16);
            this.labelControl8.TabIndex = 30;
            this.labelControl8.Text = "Directorio:";
            // 
            // pbxVerPassword
            // 
            this.pbxVerPassword.Cursor = System.Windows.Forms.Cursors.Default;
            this.pbxVerPassword.EditValue = global::SyncBD.Properties.Resources.Lock_24px;
            this.pbxVerPassword.Location = new System.Drawing.Point(556, 88);
            this.pbxVerPassword.Name = "pbxVerPassword";
            this.pbxVerPassword.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pbxVerPassword.Properties.Appearance.Options.UseBackColor = true;
            this.pbxVerPassword.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pbxVerPassword.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pbxVerPassword.Properties.ShowMenu = false;
            this.pbxVerPassword.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pbxVerPassword.Size = new System.Drawing.Size(22, 22);
            this.pbxVerPassword.TabIndex = 29;
            this.pbxVerPassword.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbxVerPassword_MouseDown);
            this.pbxVerPassword.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbxVerPassword_MouseUp);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(354, 88);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPassword.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtPassword.Properties.Appearance.Options.UseFont = true;
            this.txtPassword.Properties.Appearance.Options.UseForeColor = true;
            this.txtPassword.Properties.PasswordChar = '•';
            this.txtPassword.Size = new System.Drawing.Size(196, 22);
            this.txtPassword.TabIndex = 5;
            this.txtPassword.Leave += new System.EventHandler(this.txtServidor_Leave);
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Location = new System.Drawing.Point(270, 91);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(78, 16);
            this.labelControl7.TabIndex = 27;
            this.labelControl7.Text = "*Contraseña:";
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(76, 88);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtUsuario.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtUsuario.Properties.Appearance.Options.UseFont = true;
            this.txtUsuario.Properties.Appearance.Options.UseForeColor = true;
            this.txtUsuario.Size = new System.Drawing.Size(188, 22);
            this.txtUsuario.TabIndex = 4;
            this.txtUsuario.Leave += new System.EventHandler(this.txtServidor_Leave);
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Location = new System.Drawing.Point(14, 91);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(56, 16);
            this.labelControl6.TabIndex = 25;
            this.labelControl6.Text = "*Usuario:";
            // 
            // txtPuerto
            // 
            this.txtPuerto.Location = new System.Drawing.Point(477, 45);
            this.txtPuerto.Name = "txtPuerto";
            this.txtPuerto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPuerto.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtPuerto.Properties.Appearance.Options.UseFont = true;
            this.txtPuerto.Properties.Appearance.Options.UseForeColor = true;
            this.txtPuerto.Properties.Mask.BeepOnError = true;
            this.txtPuerto.Properties.Mask.EditMask = "d";
            this.txtPuerto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPuerto.Properties.MaxLength = 6;
            this.txtPuerto.Size = new System.Drawing.Size(102, 22);
            this.txtPuerto.TabIndex = 3;
            this.txtPuerto.Leave += new System.EventHandler(this.txtServidor_Leave);
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Location = new System.Drawing.Point(421, 48);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(50, 16);
            this.labelControl5.TabIndex = 23;
            this.labelControl5.Text = "*Puerto:";
            // 
            // txtServidor
            // 
            this.txtServidor.Location = new System.Drawing.Point(76, 45);
            this.txtServidor.Name = "txtServidor";
            this.txtServidor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtServidor.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtServidor.Properties.Appearance.Options.UseFont = true;
            this.txtServidor.Properties.Appearance.Options.UseForeColor = true;
            this.txtServidor.Properties.MaxLength = 512;
            this.txtServidor.Size = new System.Drawing.Size(339, 22);
            this.txtServidor.TabIndex = 2;
            this.txtServidor.Leave += new System.EventHandler(this.txtServidor_Leave);
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(9, 48);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(61, 16);
            this.labelControl4.TabIndex = 21;
            this.labelControl4.Text = "*Servidor:";
            // 
            // cbxProtocolo
            // 
            this.cbxProtocolo.EditValue = "FTP";
            this.cbxProtocolo.Location = new System.Drawing.Point(139, 163);
            this.cbxProtocolo.Name = "cbxProtocolo";
            this.cbxProtocolo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbxProtocolo.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.cbxProtocolo.Properties.Appearance.Options.UseFont = true;
            this.cbxProtocolo.Properties.Appearance.Options.UseForeColor = true;
            this.cbxProtocolo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxProtocolo.Properties.Items.AddRange(new object[] {
            "FTP",
            "SFTP"});
            this.cbxProtocolo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxProtocolo.Size = new System.Drawing.Size(105, 22);
            this.cbxProtocolo.TabIndex = 1;
            this.cbxProtocolo.SelectedIndexChanged += new System.EventHandler(this.cbxProtocolo_SelectedIndexChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(10, 166);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(123, 16);
            this.labelControl2.TabIndex = 20;
            this.labelControl2.Text = "Seleccione protocolo:";
            // 
            // btnAyuda
            // 
            this.btnAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAyuda.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnAyuda.Appearance.Options.UseFont = true;
            this.btnAyuda.ImageOptions.Image = global::SyncBD.Properties.Resources.Help_16px;
            this.btnAyuda.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAyuda.Location = new System.Drawing.Point(8, 510);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(139, 39);
            this.btnAyuda.TabIndex = 14;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnCancelar.Appearance.Options.UseFont = true;
            this.btnCancelar.ImageOptions.Image = global::SyncBD.Properties.Resources.Cancel_16px;
            this.btnCancelar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnCancelar.Location = new System.Drawing.Point(308, 510);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(139, 39);
            this.btnCancelar.TabIndex = 13;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Visible = false;
            // 
            // btnContinuar
            // 
            this.btnContinuar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnContinuar.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnContinuar.Appearance.Options.UseFont = true;
            this.btnContinuar.ImageOptions.Image = global::SyncBD.Properties.Resources.Ok_16px;
            this.btnContinuar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnContinuar.Location = new System.Drawing.Point(453, 510);
            this.btnContinuar.Name = "btnContinuar";
            this.btnContinuar.Size = new System.Drawing.Size(139, 39);
            this.btnContinuar.TabIndex = 12;
            this.btnContinuar.Text = "Continuar";
            this.btnContinuar.Click += new System.EventHandler(this.btnContinuar_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(10, 87);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(579, 57);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = resources.GetString("labelControl3.Text");
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(10, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(359, 29);
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "Configuración del servidor remoto";
            // 
            // pnlIzquierdo
            // 
            this.pnlIzquierdo.Controls.Add(this.petLogo);
            this.pnlIzquierdo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlIzquierdo.Location = new System.Drawing.Point(0, 0);
            this.pnlIzquierdo.Name = "pnlIzquierdo";
            this.pnlIzquierdo.Size = new System.Drawing.Size(180, 561);
            this.pnlIzquierdo.TabIndex = 4;
            // 
            // petLogo
            // 
            this.petLogo.Cursor = System.Windows.Forms.Cursors.Default;
            this.petLogo.EditValue = global::SyncBD.Properties.Resources.SyncBD;
            this.petLogo.Location = new System.Drawing.Point(5, 37);
            this.petLogo.Name = "petLogo";
            this.petLogo.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.petLogo.Properties.Appearance.Options.UseBackColor = true;
            this.petLogo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.petLogo.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.petLogo.Properties.ShowMenu = false;
            this.petLogo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.petLogo.Size = new System.Drawing.Size(170, 96);
            this.petLogo.TabIndex = 0;
            // 
            // SSMOcupado
            // 
            this.SSMOcupado.ClosingDelay = 500;
            // 
            // toastMensaje
            // 
            this.toastMensaje.ApplicationIconPath = "";
            this.toastMensaje.ApplicationId = "8bd96da6-487e-4618-8479-3129f28bb6f4";
            this.toastMensaje.ApplicationName = "SyncBD";
            this.toastMensaje.CreateApplicationShortcut = DevExpress.Utils.DefaultBoolean.True;
            this.toastMensaje.Notifications.AddRange(new DevExpress.XtraBars.ToastNotifications.IToastNotificationProperties[] {
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("c70f4e9a-079b-4392-bb24-fadcd7d6637f", global::SyncBD.Properties.Resources.Cancel_24px, "Error 0x002", "Por favor dirijase al manual de usuario para detalles", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("e7fde073-cab6-4894-be34-3df31dad0cc1", global::SyncBD.Properties.Resources.Cancel_24px, "Error 0x003", "Por favor dirijase al manual de usuario para detalles", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7c4746bd-9688-48e5-98c5-3fa2f722aac0", global::SyncBD.Properties.Resources.Cancel_24px, "Error 0x004", "Por favor dirijase al manual de usuario para detalles", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("d13c592d-f20c-49a3-85b4-aae22336845f", global::SyncBD.Properties.Resources.Cancel_24px, "Error 0x005", "Por favor dirijase al manual de usuario para detalles", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03)});
            // 
            // alertMensaje
            // 
            this.alertMensaje.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.alertMensaje.AppearanceCaption.Options.UseFont = true;
            this.alertMensaje.AppearanceCaption.Options.UseTextOptions = true;
            this.alertMensaje.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.alertMensaje.AppearanceText.Font = new System.Drawing.Font("Tahoma", 9F);
            this.alertMensaje.AppearanceText.Options.UseFont = true;
            this.alertMensaje.AppearanceText.Options.UseTextOptions = true;
            this.alertMensaje.AppearanceText.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.alertMensaje.AutoFormDelay = 4000;
            this.alertMensaje.ControlBoxPosition = DevExpress.XtraBars.Alerter.AlertFormControlBoxPosition.Right;
            this.alertMensaje.ShowPinButton = false;
            // 
            // ConfigRemoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pnlPrincipal);
            this.Controls.Add(this.pnlIzquierdo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "ConfigRemoto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SyncBD - Asistente de configuración";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigRemoto_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPrincipal)).EndInit();
            this.pnlPrincipal.ResumeLayout(false);
            this.pnlPrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAjustesAvanzados.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbcAjustesAvanzados)).EndInit();
            this.gbcAjustesAvanzados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkModoActivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAnonimo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutenticacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxEncriptacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirectorio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxVerPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsuario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPuerto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServidor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxProtocolo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlIzquierdo)).EndInit();
            this.pnlIzquierdo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.petLogo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toastMensaje)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlPrincipal;
        private DevExpress.XtraEditors.SimpleButton btnAyuda;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnContinuar;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl pnlIzquierdo;
        private DevExpress.XtraEditors.PictureEdit petLogo;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cbxProtocolo;
        private DevExpress.XtraEditors.CheckEdit chkAjustesAvanzados;
        private DevExpress.XtraEditors.GroupControl gbcAjustesAvanzados;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtPuerto;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtServidor;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.PictureEdit pbxVerPassword;
        private DevExpress.XtraEditors.TextEdit txtPassword;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtUsuario;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtDirectorio;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.CheckEdit chkModoActivo;
        private DevExpress.XtraEditors.CheckEdit chkAnonimo;
        private DevExpress.XtraEditors.CheckEdit chkAutenticacion;
        private DevExpress.XtraEditors.ComboBoxEdit cbxEncriptacion;
        private DevExpress.XtraEditors.LabelControl lblEncriptacion;
        private DevExpress.XtraSplashScreen.SplashScreenManager SSMOcupado;
        private DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager toastMensaje;
        private DevExpress.XtraBars.Alerter.AlertControl alertMensaje;
    }
}