﻿using DevExpress.XtraEditors;
using System;
using System.Windows.Forms;

namespace SyncBD.Vistas
{
    public partial class AyudaConfig : XtraForm
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        private static AyudaConfig instancia;

        public static AyudaConfig ObtenerInstancia()
        {
            if (instancia == null || instancia.IsDisposed)
            {
                instancia = new AyudaConfig();
            }

            instancia.BringToFront();

            return instancia;
        }

        private AyudaConfig()
        {
            InitializeComponent();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbxProtocolo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxTopicos.SelectedIndex == 0)
            {
                lblTitulo.Text = "Información de: " + cbxTopicos.SelectedItem.ToString();
                lblCuerpo.Text = "En el campo \"Servidor\" debe indicar la dirección FTP o SFTP que apunte al servidor, de forma local o remota.\n\nSyncBD permite el uso de tres tipos de formatos válidos, ejemplos:\n\nftp.server.com\n192.168.1.100(IP)\nlocalhost ó 127.0.0.1(Solo para conexiones locales)";
            }

            if (cbxTopicos.SelectedIndex == 1)
            {
                lblTitulo.Text = "Información de: " + cbxTopicos.SelectedItem.ToString();
                lblCuerpo.Text = "El usuario y la contraseña son imprescindibles para acceder al servidor, ya que\npreviene el acceso a personas no autorizadas.\n\nEl usuario y la contraseña se deben proporcionar por el administrador del\nservidor, por lo regular son palabras o un conjunto de letras con símbolos.\n\nSyncBD cuenta con la función \"Anónimo\", evitando escribir el usuario y la\ncontraseña, pero tenga en cuenta que el servidor deberá permitir\naccesos anónimos.";
            }

            if (cbxTopicos.SelectedIndex == 2)
            {
                lblTitulo.Text = "Información de: " + cbxTopicos.SelectedItem.ToString();
                lblCuerpo.Text = "El número de puerto, es el \"punto de entrada\" hacia el servidor, por defecto\nlos servidores utilizan el número 21 en FTP y el 22 en SFTP, pero puede variar.\n\nSi desconoce el número de puerto de su conexión, simplemente escriba un 0 en\nel campo y SyncBD tratará de encontrar el número del puerto automáticamente,\ntenga en cuenta que esta función automática puede ocasionar\nque se tarde más en conectar al servidor.";
            }

            if (cbxTopicos.SelectedIndex == 3)
            {
                lblTitulo.Text = "Información de: " + cbxTopicos.SelectedItem.ToString();
                lblCuerpo.Text = "Necesita indicar un directorio, ya que a partir de esa dirección se descargarán\no subirán archivos.\n\nEn el servidor remoto deberá indicar la ruta desde donde quiere que se descargue\nla información.\n\nEn el servidor local deberá indicar la ruta hacia donde quiere que se cargue\nla información.\n\nSino escribe ningún directorio, SyncBD descargará o cargará todas las carpetas y\narchivos desde o hacia el servidor indicado.";
            }

            if (cbxTopicos.SelectedIndex == 4)
            {
                lblTitulo.Text = "Información de: " + cbxTopicos.SelectedItem.ToString();
                lblCuerpo.Text = "La encriptación es un requermiento de configuración cuando el servidor al que\nintenta conectarse cuenta con configuraciones extras de seguridad.\n\nPuede seleccionar \"Implicita\" en donde SyncBD agregará datos de conexión\nTLS/SSL a la conexión o puede seleccionar \"Explicita\", en donde SyncBD espera\nalgún cifrado en TLS/SSL.";
            }

            if (cbxTopicos.SelectedIndex == 5)
            {
                lblTitulo.Text = "Información de: " + cbxTopicos.SelectedItem.ToString();
                lblCuerpo.Text = "Varios proveedores de servicios FTP agregan certificados de autenticidad para\nvalidar la conexión con sus sistemas internos, en ciertas ocasiones es\nnecesario quitar la autenticación que proporcione el servidor para poder establecer\nconexión.";
            }

            if (cbxTopicos.SelectedIndex == 6)
            {
                lblTitulo.Text = "Información de: " + cbxTopicos.SelectedItem.ToString();
                lblCuerpo.Text = "Cuando acepta utilizar el modo \"Activo\", SyncBD permite utilizar un protocolo TCP\nal establecer la conexión de archivos FTP, en donde el programa empieza a\n\"escuchar\" desde cualquier puerto disponible para informar al servidor desde que\npuerto se establecerá la conexión. (Se usa en ocasiones especificas)";
            }

            if (cbxTopicos.SelectedIndex == 7)
            {
                lblTitulo.Text = "Información de: " + cbxTopicos.SelectedItem.ToString();
                lblCuerpo.Text = "El protocolo es el tipo de conexión que tendra SyncBD con el servidor.\n\nEl cambio entre protocolos no implica cambios en la velocidad de\ntransferencia, pero si influye en la seguridad al transferir archivos ya que\nel protocolo \"SFTP\" permite la transferencia por medio de \"SSH\"\n(Secure SHell) que mantiene un certificado en su equipo asegurando que se\nconecta siempre al mismo servidor.";
            }
        }
    }
}