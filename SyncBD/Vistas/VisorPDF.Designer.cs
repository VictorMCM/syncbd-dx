﻿namespace SyncBD.Vistas
{
    partial class VisorPDF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisorPDF));
            this.pdfVisor = new DevExpress.XtraPdfViewer.PdfViewer();
            this.alertMensaje = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            this.toastMensaje = new DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.toastMensaje)).BeginInit();
            this.SuspendLayout();
            // 
            // pdfVisor
            // 
            this.pdfVisor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pdfVisor.Location = new System.Drawing.Point(0, 0);
            this.pdfVisor.Name = "pdfVisor";
            this.pdfVisor.Size = new System.Drawing.Size(1008, 561);
            this.pdfVisor.TabIndex = 0;
            // 
            // alertMensaje
            // 
            this.alertMensaje.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.alertMensaje.AppearanceCaption.Options.UseFont = true;
            this.alertMensaje.AppearanceCaption.Options.UseTextOptions = true;
            this.alertMensaje.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.alertMensaje.AppearanceText.Font = new System.Drawing.Font("Tahoma", 9F);
            this.alertMensaje.AppearanceText.Options.UseFont = true;
            this.alertMensaje.AppearanceText.Options.UseTextOptions = true;
            this.alertMensaje.AppearanceText.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.alertMensaje.AutoFormDelay = 4000;
            this.alertMensaje.ControlBoxPosition = DevExpress.XtraBars.Alerter.AlertFormControlBoxPosition.Right;
            this.alertMensaje.ShowPinButton = false;
            // 
            // toastMensaje
            // 
            this.toastMensaje.ApplicationIconPath = "";
            this.toastMensaje.ApplicationId = "8bd96da6-487e-4618-8479-3129f28bb6f4";
            this.toastMensaje.ApplicationName = "SyncBD";
            this.toastMensaje.CreateApplicationShortcut = DevExpress.Utils.DefaultBoolean.True;
            this.toastMensaje.Notifications.AddRange(new DevExpress.XtraBars.ToastNotifications.IToastNotificationProperties[] {
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("c70f4e9a-079b-4392-bb24-fadcd7d6637f", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x002", "Imposible determinar ajustes de los servidores", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("e7fde073-cab6-4894-be34-3df31dad0cc1", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x003", "Error en lectura de configuraciones previas...", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7c4746bd-9688-48e5-98c5-3fa2f722aac0", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x004", "Ajustes inválidos o datos incorrectos", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("d13c592d-f20c-49a3-85b4-aae22336845f", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x005", "Datos de configuracion inválidos o de formato incorrecto.", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("fec336e8-324b-488e-8a20-60fc4f584782", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x001", "Ajustes previos dañados o incorrectos", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("ff6c06c6-fd79-4194-b90c-6f013533ebd5", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x006", "Imposible leer los ajustes previos del sistema", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("8a944fa0-ffb7-4073-b868-f2c55f8f47e3", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x007", "No se completo el guardado de ajustes", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("2090d200-7ac9-4ca8-9c55-026ef9af8e3b", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x008", "Imposible la restauración de ajustes", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7ae421fc-fa8b-4585-8166-f17641bde57b", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x009", "Ocurrió un problema al obtener el historial", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("38dc66bf-5cf8-4644-8b4f-48179785bdb2", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x010", "Imposible encontrar el destino del historial", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("a598dbf0-8fda-483c-b658-a17c4b71ce3c", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x011", "Ocurrió un error en la transferencia de archivos", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("503a9e59-8116-44c1-9eb8-57af2664e621", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x012", "Se detuvo de forma inesperada la transferencia de archivos", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("30213a7f-c354-4e9c-80bd-d539a236bc75", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x013", "Existe un error en en el sistema que impide el correcto funcionamiento", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7fbfedb5-3de5-4b6d-b506-69ad5af656a7", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x014", "Conexión inestable, sincronización cancelada.", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("ec1fddba-1bf7-4b86-b063-5a5c4e126f94", global::SyncBD.Properties.Resources.Cancel_96px, "Error 0x015", "Imposible acceder a los registros de Windows™", "Por favor dirijase al manual de usuario para detalles", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText04),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("60d5b5f9-1a0d-4cbf-b476-5416dc7e1aa1", global::SyncBD.Properties.Resources.Attention_96px, "Servidor remoto sin configurar...", "No hay configuraciones del servidor remoto", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7d1f937f-505c-4e27-a3ca-91b15dfe3b7f", global::SyncBD.Properties.Resources.Info_96px, "Iniciando conexión...", "Sincronizando con servidor remoto", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7d71d6f5-9065-462e-be8e-b7689b1286b4", global::SyncBD.Properties.Resources.Ok_96px, "Sincronización finalizada", "Se ha completado la sincronización con el servidor Remoto", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("491ef6d1-c744-4e74-9c71-892871442416", global::SyncBD.Properties.Resources.Ok_96px, "Archivos transferidos", "Se han transferido los archivos al directorio copia", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("da753166-2726-4f36-abc3-447013054625", global::SyncBD.Properties.Resources.Attention_96px, "No se transfirieron los archivos", "Ocurrió un imprevisto al transferir los archivos", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("573fd277-ec9b-4b37-b5ba-5c1a63fea667", global::SyncBD.Properties.Resources.Attention_96px, "Sin directorio de guardado", "No ha configurado la dirección de guardado de archivos", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("b9a553cc-13bb-42d2-b22d-8d5fa3c50f96", global::SyncBD.Properties.Resources.Ok_96px, "Sin copia de respaldo", "Debido a que no hay cambios, puede utilizar la última carpeta copiada", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("1153088b-b9a3-44e5-97a3-c564af28c277", global::SyncBD.Properties.Resources.Attention_96px, "Sincronización incompleta", "No se pudo completar la sincronización con el servidor Remoto", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("162af2bb-3646-4169-b4b8-2f62029ca924", global::SyncBD.Properties.Resources.Attention_96px, "Servidor inaccesible", "Se perdió la comunicación con el servidor Remoto, reintente por favor", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("750a957c-b003-44a9-8719-94e4ea8937f4", global::SyncBD.Properties.Resources.Attention_96px, "Faltan datos de conexión", "El servidor remoto no tiene configuración", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("9a9ab089-a67b-4adf-9889-814a01d0272c", global::SyncBD.Properties.Resources.Attention_96px, "Servidor local sin configurar...", "No hay configuraciones del servidor local", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("9f42577f-c0a3-418f-9335-29a3f21da934", global::SyncBD.Properties.Resources.Info_96px, "Iniciando conexión...", "Sincronizando con servidor local", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("c11b976a-cfdc-4083-9189-95333ae7e817", global::SyncBD.Properties.Resources.Ok_96px, "Sincronización finalizada", "Se ha completado la sincronización con el servidor Local", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7df226a8-c995-4f75-aef5-18c5ee37df87", global::SyncBD.Properties.Resources.Attention_96px, "Verificando ajustes", "Reintente sincronización", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7b0a8260-5b68-4d49-99bf-dd017b3a97ad", global::SyncBD.Properties.Resources.Attention_96px, "Servidor inaccesible", "Se perdió la comunicación con el servidor Local, reintente por favor", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("9f7815a5-0024-423b-9f0e-6997cc7dc8ca", global::SyncBD.Properties.Resources.Attention_96px, "Faltan datos de conexión", "El servidor local no tiene configuración", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("c125ca7a-d06c-4929-9db7-7092be53d953", global::SyncBD.Properties.Resources.Ok_96px, "Historial borrado", "Se ha borrado el todo el historial correctamente", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("ba24a13a-bd76-4921-bd25-943daa99bf83", global::SyncBD.Properties.Resources.Attention_96px, "No se borró historial", "No fue posible borrar todo el historial", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("cf44c41e-cd73-43d9-be56-e455864c91e3", global::SyncBD.Properties.Resources.Info_96px, "Ocultando SyncBD", "SyncBD trabajará en segundo plano, restaurelo en la barra de tareas", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("89ca573d-e9e1-4618-9fdb-7341782c9cff", global::SyncBD.Properties.Resources.Ok_96px, "Historial borrado", "Se ha borrado el historial del servidor Remoto", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("a0a8af7c-0d28-4a95-9a33-4c15f51134f4", global::SyncBD.Properties.Resources.Attention_96px, "No se borró historial", "No fue posible borrar el historial del servidor Remoto", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("fc942a97-627b-43fe-ba36-f8566fe155be", global::SyncBD.Properties.Resources.Ok_96px, "Historial borrado", "Se ha borrado el historial del servidor Local", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("b10313c7-e306-4c05-978d-9ffc018f3abc", global::SyncBD.Properties.Resources.Attention_96px, "No se borró historial", "No fue posible borrar el historial del servidor Local", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("0f721c13-71e8-4e2a-9dd3-c4081d343420", global::SyncBD.Properties.Resources.Ok_96px, "Sincronización cancelada", "Se canceló la sincronización correctamente", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("e8dcaa69-9418-44cb-95ee-c439c62be70e", global::SyncBD.Properties.Resources.Attention_96px, "Imposible cancelar", "Imposible cancelar la sincronización", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("357ab85e-a57b-4aa1-a88d-39b1c0d1eb3e", global::SyncBD.Properties.Resources.Attention_96px, "No se detectó manual", "Al parecer no existe el manual de usuario", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("e1855e0f-a4fc-479a-a5e7-d3f9f3631fab", global::SyncBD.Properties.Resources.Cancel_96px, "Error al mostrar manual (0x016)", "Ocurrió un error al procesar el manual.", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03)});
            // 
            // VisorPDF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 561);
            this.Controls.Add(this.pdfVisor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VisorPDF";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Visor de manuales";
            this.Load += new System.EventHandler(this.VisorPDF_Load);
            ((System.ComponentModel.ISupportInitialize)(this.toastMensaje)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPdfViewer.PdfViewer pdfVisor;
        private DevExpress.XtraBars.Alerter.AlertControl alertMensaje;
        private DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager toastMensaje;
    }
}