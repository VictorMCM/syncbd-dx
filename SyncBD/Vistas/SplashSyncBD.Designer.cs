﻿namespace SyncBD.Vistas
{
    partial class SplashSyncBD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.tmpCambio = new System.Windows.Forms.Timer(this.components);
            this.toastMensaje = new DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager(this.components);
            this.alertMensaje = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            this.lblCopyright = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toastMensaje)).BeginInit();
            this.SuspendLayout();
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(23, 109);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(404, 12);
            this.marqueeProgressBarControl1.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(364, 87);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(63, 16);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "Iniciando...";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::SyncBD.Properties.Resources.SolucionesDigitales;
            this.pictureEdit1.Location = new System.Drawing.Point(23, 12);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.AllowFocused = false;
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(123, 55);
            this.pictureEdit1.TabIndex = 8;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit3.EditValue = global::SyncBD.Properties.Resources.SyncBD;
            this.pictureEdit3.Location = new System.Drawing.Point(304, 12);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.AllowFocused = false;
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ShowMenu = false;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit3.Size = new System.Drawing.Size(123, 55);
            this.pictureEdit3.TabIndex = 10;
            // 
            // tmpCambio
            // 
            this.tmpCambio.Interval = 4000;
            this.tmpCambio.Tick += new System.EventHandler(this.tmpCambio_Tick);
            // 
            // toastMensaje
            // 
            this.toastMensaje.ApplicationIconPath = "";
            this.toastMensaje.ApplicationId = "8bd96da6-487e-4618-8479-3129f28bb6f4";
            this.toastMensaje.ApplicationName = "SyncBD";
            this.toastMensaje.CreateApplicationShortcut = DevExpress.Utils.DefaultBoolean.True;
            this.toastMensaje.Notifications.AddRange(new DevExpress.XtraBars.ToastNotifications.IToastNotificationProperties[] {
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("c70f4e9a-079b-4392-bb24-fadcd7d6637f", global::SyncBD.Properties.Resources.Cancel_24px, "Error 0x002", "Por favor dirijase al manual de usuario para detalles", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("e7fde073-cab6-4894-be34-3df31dad0cc1", global::SyncBD.Properties.Resources.Cancel_24px, "Error 0x003", "Por favor dirijase al manual de usuario para detalles", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("7c4746bd-9688-48e5-98c5-3fa2f722aac0", global::SyncBD.Properties.Resources.Cancel_24px, "Error 0x004", "Por favor dirijase al manual de usuario para detalles", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("d13c592d-f20c-49a3-85b4-aae22336845f", global::SyncBD.Properties.Resources.Cancel_24px, "Error 0x005", "Por favor dirijase al manual de usuario para detalles", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03),
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("fec336e8-324b-488e-8a20-60fc4f584782", global::SyncBD.Properties.Resources.Cancel_24px, "Error 0x001", "Por favor dirijase al manual de usuario para detalles", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03)});
            // 
            // alertMensaje
            // 
            this.alertMensaje.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.alertMensaje.AppearanceCaption.Options.UseFont = true;
            this.alertMensaje.AppearanceCaption.Options.UseTextOptions = true;
            this.alertMensaje.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.alertMensaje.AppearanceText.Font = new System.Drawing.Font("Tahoma", 9F);
            this.alertMensaje.AppearanceText.Options.UseFont = true;
            this.alertMensaje.AppearanceText.Options.UseTextOptions = true;
            this.alertMensaje.AppearanceText.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.alertMensaje.AutoFormDelay = 4000;
            this.alertMensaje.ControlBoxPosition = DevExpress.XtraBars.Alerter.AlertFormControlBoxPosition.Right;
            this.alertMensaje.ShowPinButton = false;
            // 
            // lblCopyright
            // 
            this.lblCopyright.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblCopyright.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblCopyright.Appearance.Options.UseFont = true;
            this.lblCopyright.Appearance.Options.UseForeColor = true;
            this.lblCopyright.Location = new System.Drawing.Point(23, 87);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(230, 16);
            this.lblCopyright.TabIndex = 11;
            this.lblCopyright.Text = "2018 © Todos los derechos reservados.";
            // 
            // SplashSyncBD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 133);
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.pictureEdit3);
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.marqueeProgressBarControl1);
            this.Name = "SplashSyncBD";
            this.Text = "Iniciando SyncBD";
            this.Shown += new System.EventHandler(this.SplashSyncBD_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toastMensaje)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private System.Windows.Forms.Timer tmpCambio;
        private DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager toastMensaje;
        private DevExpress.XtraBars.Alerter.AlertControl alertMensaje;
        private DevExpress.XtraEditors.LabelControl lblCopyright;
    }
}
