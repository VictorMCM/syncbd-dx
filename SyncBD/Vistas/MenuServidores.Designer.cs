﻿namespace SyncBD.Vistas
{
    partial class MenuServidores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuServidores));
            this.pnlPrincipal = new DevExpress.XtraEditors.PanelControl();
            this.btnAyuda = new DevExpress.XtraEditors.SimpleButton();
            this.chkAmbos = new DevExpress.XtraEditors.CheckEdit();
            this.chkLocal = new DevExpress.XtraEditors.CheckEdit();
            this.chkRemoto = new DevExpress.XtraEditors.CheckEdit();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.btnContinuar = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pnlIzquierdo = new DevExpress.XtraEditors.PanelControl();
            this.petLogo = new DevExpress.XtraEditors.PictureEdit();
            this.toastMensaje = new DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager(this.components);
            this.alertMensaje = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPrincipal)).BeginInit();
            this.pnlPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAmbos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLocal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRemoto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlIzquierdo)).BeginInit();
            this.pnlIzquierdo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.petLogo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toastMensaje)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlPrincipal
            // 
            this.pnlPrincipal.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlPrincipal.Controls.Add(this.btnAyuda);
            this.pnlPrincipal.Controls.Add(this.chkAmbos);
            this.pnlPrincipal.Controls.Add(this.chkLocal);
            this.pnlPrincipal.Controls.Add(this.chkRemoto);
            this.pnlPrincipal.Controls.Add(this.btnSalir);
            this.pnlPrincipal.Controls.Add(this.btnContinuar);
            this.pnlPrincipal.Controls.Add(this.labelControl2);
            this.pnlPrincipal.Controls.Add(this.labelControl3);
            this.pnlPrincipal.Controls.Add(this.labelControl1);
            this.pnlPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPrincipal.Location = new System.Drawing.Point(180, 0);
            this.pnlPrincipal.Name = "pnlPrincipal";
            this.pnlPrincipal.Size = new System.Drawing.Size(604, 561);
            this.pnlPrincipal.TabIndex = 3;
            // 
            // btnAyuda
            // 
            this.btnAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAyuda.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnAyuda.Appearance.Options.UseFont = true;
            this.btnAyuda.ImageOptions.Image = global::SyncBD.Properties.Resources.Help_16px;
            this.btnAyuda.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAyuda.Location = new System.Drawing.Point(8, 510);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(139, 39);
            this.btnAyuda.TabIndex = 6;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // chkAmbos
            // 
            this.chkAmbos.Location = new System.Drawing.Point(196, 366);
            this.chkAmbos.Name = "chkAmbos";
            this.chkAmbos.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkAmbos.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.chkAmbos.Properties.Appearance.Options.UseFont = true;
            this.chkAmbos.Properties.Appearance.Options.UseForeColor = true;
            this.chkAmbos.Properties.Caption = "Configurar ambos servidores";
            this.chkAmbos.Size = new System.Drawing.Size(253, 23);
            toolTipTitleItem1.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_16px;
            toolTipTitleItem1.Text = "Configurar ambos servidores";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Al seleccionar esta opción podrá configurar los dos servidores, primero el servid" +
    "or Remoto y posteriormente el Local.";
            toolTipTitleItem2.ImageOptions.Image = global::SyncBD.Properties.Resources.Attention_16px;
            toolTipTitleItem2.LeftIndent = 6;
            toolTipTitleItem2.Text = "Puede omitir la configuración de un servidor, pero si omite los dos, no podrá con" +
    "tinuar.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.Items.Add(toolTipSeparatorItem1);
            superToolTip1.Items.Add(toolTipTitleItem2);
            this.chkAmbos.SuperTip = superToolTip1;
            this.chkAmbos.TabIndex = 3;
            this.chkAmbos.CheckedChanged += new System.EventHandler(this.chkAmbos_CheckedChanged);
            // 
            // chkLocal
            // 
            this.chkLocal.Location = new System.Drawing.Point(196, 314);
            this.chkLocal.Name = "chkLocal";
            this.chkLocal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkLocal.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.chkLocal.Properties.Appearance.Options.UseFont = true;
            this.chkLocal.Properties.Appearance.Options.UseForeColor = true;
            this.chkLocal.Properties.Caption = "Configurar el servidor Local";
            this.chkLocal.Size = new System.Drawing.Size(253, 23);
            toolTipTitleItem3.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_16px;
            toolTipTitleItem3.Text = "Configurar servidor Local";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Al seleccionar esta opción solo configurará el servidor Local, asegurese de conta" +
    "r con los datos de acceso.";
            superToolTip2.Items.Add(toolTipTitleItem3);
            superToolTip2.Items.Add(toolTipItem2);
            this.chkLocal.SuperTip = superToolTip2;
            this.chkLocal.TabIndex = 2;
            this.chkLocal.CheckedChanged += new System.EventHandler(this.chkLocal_CheckedChanged);
            // 
            // chkRemoto
            // 
            this.chkRemoto.Location = new System.Drawing.Point(196, 262);
            this.chkRemoto.Name = "chkRemoto";
            this.chkRemoto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkRemoto.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.chkRemoto.Properties.Appearance.Options.UseFont = true;
            this.chkRemoto.Properties.Appearance.Options.UseForeColor = true;
            this.chkRemoto.Properties.Caption = "Configurar el servidor Remoto";
            this.chkRemoto.Size = new System.Drawing.Size(253, 23);
            toolTipTitleItem4.ImageOptions.Image = global::SyncBD.Properties.Resources.Info_16px;
            toolTipTitleItem4.Text = "Configurar servidor Remoto";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Al seleccionar esta opción solo configurará el servidor Remoto, asegurese de cont" +
    "ar con los datos de acceso.";
            superToolTip3.Items.Add(toolTipTitleItem4);
            superToolTip3.Items.Add(toolTipItem3);
            this.chkRemoto.SuperTip = superToolTip3;
            this.chkRemoto.TabIndex = 1;
            this.chkRemoto.CheckedChanged += new System.EventHandler(this.chkRemoto_CheckedChanged);
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.ImageOptions.Image = global::SyncBD.Properties.Resources.Cancel_16px;
            this.btnSalir.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnSalir.Location = new System.Drawing.Point(308, 510);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(139, 39);
            this.btnSalir.TabIndex = 5;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnContinuar
            // 
            this.btnContinuar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnContinuar.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnContinuar.Appearance.Options.UseFont = true;
            this.btnContinuar.ImageOptions.Image = global::SyncBD.Properties.Resources.Ok_16px;
            this.btnContinuar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnContinuar.Location = new System.Drawing.Point(453, 510);
            this.btnContinuar.Name = "btnContinuar";
            this.btnContinuar.Size = new System.Drawing.Size(139, 39);
            this.btnContinuar.TabIndex = 4;
            this.btnContinuar.Text = "Continuar";
            this.btnContinuar.Click += new System.EventHandler(this.btnContinuar_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(10, 160);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(539, 38);
            this.labelControl2.TabIndex = 11;
            this.labelControl2.Text = "Para que pueda continuar será necesario que seleccione al menos una de las\r\nsigui" +
    "entes opciones disponibles:";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(10, 87);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(532, 57);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = resources.GetString("labelControl3.Text");
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(10, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(294, 29);
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "Configuración de servidores";
            // 
            // pnlIzquierdo
            // 
            this.pnlIzquierdo.Controls.Add(this.petLogo);
            this.pnlIzquierdo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlIzquierdo.Location = new System.Drawing.Point(0, 0);
            this.pnlIzquierdo.Name = "pnlIzquierdo";
            this.pnlIzquierdo.Size = new System.Drawing.Size(180, 561);
            this.pnlIzquierdo.TabIndex = 2;
            // 
            // petLogo
            // 
            this.petLogo.Cursor = System.Windows.Forms.Cursors.Default;
            this.petLogo.EditValue = global::SyncBD.Properties.Resources.SyncBD;
            this.petLogo.Location = new System.Drawing.Point(5, 37);
            this.petLogo.Name = "petLogo";
            this.petLogo.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.petLogo.Properties.Appearance.Options.UseBackColor = true;
            this.petLogo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.petLogo.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.petLogo.Properties.ShowMenu = false;
            this.petLogo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.petLogo.Size = new System.Drawing.Size(170, 96);
            this.petLogo.TabIndex = 0;
            // 
            // toastMensaje
            // 
            this.toastMensaje.ApplicationIconPath = "";
            this.toastMensaje.ApplicationId = "8bd96da6-487e-4618-8479-3129f28bb6f4";
            this.toastMensaje.ApplicationName = "SyncBD";
            this.toastMensaje.CreateApplicationShortcut = DevExpress.Utils.DefaultBoolean.True;
            this.toastMensaje.Notifications.AddRange(new DevExpress.XtraBars.ToastNotifications.IToastNotificationProperties[] {
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("c70f4e9a-079b-4392-bb24-fadcd7d6637f", global::SyncBD.Properties.Resources.Cancel_24px, "Error 0x002", "Por favor dirijase al manual de usuario para detalles", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" +
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.ImageAndText03)});
            // 
            // alertMensaje
            // 
            this.alertMensaje.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.alertMensaje.AppearanceCaption.Options.UseFont = true;
            this.alertMensaje.AppearanceCaption.Options.UseTextOptions = true;
            this.alertMensaje.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.alertMensaje.AppearanceText.Font = new System.Drawing.Font("Tahoma", 9F);
            this.alertMensaje.AppearanceText.Options.UseFont = true;
            this.alertMensaje.AppearanceText.Options.UseTextOptions = true;
            this.alertMensaje.AppearanceText.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.alertMensaje.AutoFormDelay = 4000;
            this.alertMensaje.ControlBoxPosition = DevExpress.XtraBars.Alerter.AlertFormControlBoxPosition.Right;
            this.alertMensaje.ShowPinButton = false;
            // 
            // MenuServidores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pnlPrincipal);
            this.Controls.Add(this.pnlIzquierdo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "MenuServidores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SyncBD - Asistente de configuración";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MenuServidores_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pnlPrincipal)).EndInit();
            this.pnlPrincipal.ResumeLayout(false);
            this.pnlPrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAmbos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLocal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRemoto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlIzquierdo)).EndInit();
            this.pnlIzquierdo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.petLogo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toastMensaje)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlPrincipal;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.SimpleButton btnContinuar;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl pnlIzquierdo;
        private DevExpress.XtraEditors.PictureEdit petLogo;
        private DevExpress.XtraEditors.CheckEdit chkAmbos;
        private DevExpress.XtraEditors.CheckEdit chkLocal;
        private DevExpress.XtraEditors.CheckEdit chkRemoto;
        private DevExpress.XtraEditors.SimpleButton btnAyuda;
        private DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager toastMensaje;
        private DevExpress.XtraBars.Alerter.AlertControl alertMensaje;
    }
}