﻿using DevExpress.XtraEditors;
using Logger;
using SyncBD.Controladores;
using System;
using System.Windows.Forms;

namespace SyncBD.Vistas
{
    public partial class FinAsistente : XtraForm
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        private static FinAsistente instancia;

        public static FinAsistente ObtenerInstancia(Visualizacion forma)
        {
            if (instancia == null || instancia.IsDisposed)
            {
                instancia = new FinAsistente(forma);
            }

            instancia.BringToFront();

            return instancia;
        }

        private FinAsistente(Visualizacion forma)
        {
            InitializeComponent();

            LeerConfiguracionesServidores();

            Determinar(forma);
        }

        Nucleo nucleo = new Nucleo();

        Seguridad seguridad = new Seguridad();

        ToLog log = new ToLog();

        Servidores servidorRemoto;

        Servidores servidorLocal;

        public enum Visualizacion
        {
            Remoto,
            Local,
            Ambos
        }

        private void LeerConfiguracionesServidores()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones != null)
                {
                    if (configuraciones.Remoto.Servidor != null)
                    {
                        servidorRemoto = new Servidores
                        {
                            Remoto = new Remoto
                            {
                                Servidor = seguridad.Desencriptar(configuraciones.Remoto.Servidor),
                                Usuario = seguridad.Desencriptar(configuraciones.Remoto.Usuario),
                                Directorio = seguridad.Desencriptar(configuraciones.Remoto.Directorio)
                            }
                        };
                    }

                    if (configuraciones.Local.Servidor != null)
                    {
                        servidorLocal = new Servidores
                        {
                            Local = new Local
                            {
                                Servidor = seguridad.Desencriptar(configuraciones.Local.Servidor),
                                Usuario = seguridad.Desencriptar(configuraciones.Local.Usuario),
                                Directorio = seguridad.Desencriptar(configuraciones.Local.Directorio)
                            }
                        };
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void Determinar(Visualizacion forma)
        {
            switch (forma)
            {
                case Visualizacion.Remoto:
                    gbcLocal.Visible = false;

                    lblServidor1.Text = servidorRemoto.Remoto.Servidor;
                    lblUsuario1.Text = servidorRemoto.Remoto.Usuario;
                    lblDirectorio1.Text = servidorRemoto.Remoto.Directorio;

                    break;

                case Visualizacion.Local:
                    gbcLocal.Visible = false;
                    gbcRemoto.Text = "Datos del servidor local";

                    lblServidor1.Text = servidorLocal.Local.Servidor;
                    lblUsuario1.Text = servidorLocal.Local.Usuario;
                    lblDirectorio1.Text = servidorLocal.Local.Directorio;

                    break;

                case Visualizacion.Ambos:
                    gbcRemoto.Visible = true;
                    gbcLocal.Visible = true;

                    lblServidor1.Text = servidorRemoto.Remoto.Servidor;
                    lblUsuario1.Text = servidorRemoto.Remoto.Usuario;
                    lblDirectorio1.Text = servidorRemoto.Remoto.Directorio;

                    lblServidor2.Text = servidorLocal.Local.Servidor;
                    lblUsuario2.Text = servidorLocal.Local.Usuario;
                    lblDirectorio2.Text = servidorLocal.Local.Directorio;

                    break;

                default:
                    gbcRemoto.Visible = true;
                    gbcLocal.Visible = true;

                    lblServidor1.Text = servidorRemoto.Remoto.Servidor;
                    lblUsuario1.Text = servidorRemoto.Remoto.Usuario;
                    lblDirectorio1.Text = servidorRemoto.Remoto.Directorio;

                    lblServidor2.Text = servidorLocal.Local.Servidor;
                    lblUsuario2.Text = servidorLocal.Local.Usuario;
                    lblDirectorio2.Text = servidorLocal.Local.Directorio;

                    break;
            }
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            Principal mostrar = Principal.ObtenerInstancia();

            Hide();

            log.AppendLog("[Configuraciones] Mostrando la ventana principal");

            mostrar.ShowDialog();
        }

        private void FinAsistente_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;

                Principal mostrar = Principal.ObtenerInstancia();

                Hide();

                log.AppendLog("[Configuraciones] Mostrando la ventana principal");

                mostrar.ShowDialog();
            }
        }
    }
}