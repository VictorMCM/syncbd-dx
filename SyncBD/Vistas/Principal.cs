﻿using DevExpress.LookAndFeel;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using Logger;
using Microsoft.VisualBasic.FileIO;
using Microsoft.Win32;
using SyncBD.Controladores;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinSCP;

namespace SyncBD.Vistas
{
    public partial class Principal : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        private static Principal instancia;

        public static Principal ObtenerInstancia()
        {
            if (instancia == null || instancia.IsDisposed)
            {
                instancia = new Principal();
            }

            instancia.BringToFront();

            return instancia;
        }

        public string ObtenerVersion { get { return Assembly.GetExecutingAssembly().GetName().Version.ToString(); } }

        private Principal()
        {
            InitializeComponent();

            nucleo.ComprobarGeneral();

            nucleo.ComprobarRegistros();

            LeerAjustes();
        }

        private string dirArchivosRemotosOriginal = Application.StartupPath + @"\Archivos_Remotos";

        TimeSpan relojLocal = new TimeSpan(0, 0, 0);
        TimeSpan relojRemoto = new TimeSpan(0, 0, 0);
        private int archivosRemotos = 0;
        private int archivosLocales = 0;
        private bool cancelarTransferenciaRemoto = false;
        private bool cancelarTransferenciaLocal = false;

        Nucleo nucleo = new Nucleo();
        Seguridad seguridad = new Seguridad();
        EnlaceModelo enlace = new EnlaceModelo();
        ToLog log = new ToLog();

        #region Eventos del menu

        private void xtabPrincipal_CloseButtonClick(object sender, EventArgs e)
        {
            ClosePageButtonEventArgs evento = e as ClosePageButtonEventArgs;
            
            if(evento.Page.Text == "Ayuda")
            {
                (evento.Page as XtraTabPage).PageVisible = false;
            }

            if (evento.Page.Text == "Ajustes")
            {
                (evento.Page as XtraTabPage).PageVisible = false;
            }

            if (evento.Page.Text == "Servidor Remoto")
            {
                (evento.Page as XtraTabPage).PageVisible = false;
            }

            if (evento.Page.Text == "Servidor Local")
            {
                (evento.Page as XtraTabPage).PageVisible = false;
            }
        }

        private void btnMenuRemoto_ItemClick(object sender, ItemClickEventArgs e)
        {
            xtabRemoto.PageVisible = true;

            xtabPrincipal.SelectedTabPage = xtabPrincipal.TabPages[0];
        }

        private void btnMenuLocal_ItemClick(object sender, ItemClickEventArgs e)
        {
            xtabLocal.PageVisible = true;

            xtabPrincipal.SelectedTabPage = xtabPrincipal.TabPages[1];
        }

        private void btnMenuAjustes_ItemClick(object sender, ItemClickEventArgs e)
        {
            xtabAjustes.PageVisible = true;

            xtabPrincipal.SelectedTabPage = xtabPrincipal.TabPages[2];
        }

        private void btnMenuAyuda_ItemClick(object sender, ItemClickEventArgs e)
        {
            xtabAyuda.PageVisible = true;

            xtabPrincipal.SelectedTabPage = xtabPrincipal.TabPages[3];
        }

        #endregion

        #region Metodos internos

        /// <summary>
        /// Muestra una pantalla modal cuando esta ocupado el sistema
        /// </summary>
        private void Ocupar(SplashScreenManager splash)
        {
            if (!splash.IsSplashFormVisible)
            {
                splash.ShowWaitForm();
            }
        }

        /// <summary>
        /// Quita la pantalla modal de que el sistema esta ocupado
        /// </summary>
        private void Liberar(SplashScreenManager splash)
        {
            if (splash.IsSplashFormVisible)
            {
                splash.CloseWaitForm();
            }
        }
        
        /// <summary>
        /// Muestra una notificacion dependiendo el Sistema Operativo utilizado
        /// </summary>
        /// <param name="titulo">Titulo que tendrá la notificacion</param>
        /// <param name="texto">Texto o cuerpo de la notificación</param>
        /// <param name="imagen">Imagen que se pondrá en la notificación, null si no se desea poner ninguna (Cambia según sistema operativo)</param>
        /// <param name="toast">Número de la notificación del toastnotificactionmanager</param>
        private void MostrarNotificacion(string titulo, string texto, Image imagen, int toast)
        {
            try
            {
                if (nucleo.ObtenerVersionSO().Contains("WINDOWS 8") || nucleo.ObtenerVersionSO().Contains("WINDOWS 10"))
                {
                    if(toast == 33)
                    {
                        alertMensaje.Show(this, titulo, texto, "", imagen);
                    }

                    else
                    {
                        try
                        {
                            toastMensaje.ShowNotification(toastMensaje.Notifications[toast]);
                        }
                        catch (Exception)
                        {
                            alertMensaje.Show(this, titulo, texto, "", imagen);
                        }
                    }
                    
                }

                else
                {
                    alertMensaje.Show(this, titulo, texto, "", imagen);
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Valida la version y la asigna en los campos para visualizarlo
        /// </summary>
        private void VerificarVersion()
        {
            try
            {
                bsiVersion.Caption = "SyncBD v. " + ObtenerVersion.Substring(0, ObtenerVersion.Length - 4);
                lblCopyright.Text = "Soluciones Digitales y de Procesos S.A. de C.V. © " + DateTime.Now.ToString("yyyy");
                lblVersion.Text = "Versión del producto: " + ObtenerVersion;

                log.AppendLog("[Versión] SyncBD - " + ObtenerVersion);
            }
            catch (Exception)
            {
                
            }
        }

        /// <summary>
        /// Obtiene los ajustes del programa configura los controles acorde a las configuraciones
        /// </summary>
        private void LeerAjustes()
        {
            try
            {
                VerificarVersion();

                Ajustes ajustes = nucleo.LeerAjustes();
                
                if (ajustes != null)
                {
                    chkIniciarConWindows.Checked = ajustes.IniciarConWindows;
                    chkMinimizarAlIniciar.Checked = ajustes.MinimizarAlIniciar;
                    btnRutaCopiaArchivos.Text = ajustes.CopiarA;
                    txtTiempoDesconexion.Text = ajustes.TiempoDesconexion > 0 ? (ajustes.TiempoDesconexion / 1000).ToString()  : "9999";
                    txtTiempoReconexion.Text = ajustes.TiempoReConexion > 0 ? (ajustes.TiempoReConexion / 1000).ToString() : "3";

                    List<string> temas = new List<string>();

                    temas.Clear();

                    temas.Add("Windows 7 Clásico");
                    temas.Add("Office 2010 Azul");
                    temas.Add("Office 2010 Oscuro");
                    temas.Add("Office 2010 Plateado");
                    temas.Add("Office 2013");
                    temas.Add("Office 2013 Gris Oscuro");
                    temas.Add("Office 2013 Gris Claro");
                    temas.Add("Office 2016 Colorido");
                    temas.Add("Office 2016 Oscuro");
                    temas.Add("Office 2016 Negro");
                    temas.Add("Moderno");
                    temas.Add("Café");
                    temas.Add("Azul cielo");
                    temas.Add("Azul opaco");
                    temas.Add("Océano");
                    temas.Add("Lila");
                    temas.Add("Invierno");
                    temas.Add("Amor y Amistad");
                    temas.Add("Apple");
                    temas.Add("Verano");
                    temas.Add("Halloween");
                    temas.Add("Oscuro");
                    temas.Add("Primavera");
                    temas.Add("Neblina");
                    temas.Add("Alto Contraste");
                    temas.Add("Windows 7 Moderno");
                    temas.Add("Oscuro Fino");
                    temas.Add("Oscuro Extra");
                    temas.Add("Blanco y Azul");
                    temas.Add("Pintura Blanca");
                    temas.Add("Caramelo");
                    temas.Add("Azul Elegante");
                    temas.Add("Lila Elegante");
                    temas.Add("Azul Brilloso");
                    temas.Add("Negro Brilloso");
                    temas.Add("Office 2007 Azul");
                    temas.Add("Office 2007 Oscuro");
                    temas.Add("Office 2007 Plateado");
                    temas.Add("Office 2007 Verde");
                    temas.Add("Office 2007 Rosa");
                    temas.Add("Lunar");
                    temas.Add("Oscuro Perfecto");
                    temas.Add("Pintura Azul");
                    temas.Add("Moderno Oscuro");
                    temas.Add("Moderno Claro");
                    
                    if (temas != null)
                    {
                        foreach (string tema in temas)
                        {
                            cbxApariencia.Properties.Items.Add(tema);
                        }

                        if(cbxApariencia.Properties.Items.Contains(ajustes.Apariencia))
                        {
                            cbxApariencia.SelectedItem = ajustes.Apariencia;
                        }

                        else
                        {
                            cbxApariencia.SelectedItem = "Moderno";
                        }
                    }

                    log.AppendLog("[Principal] Se leyeron los ajustes correctamente");
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x006, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x006", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 5);
            }
        }

        /// <summary>
        /// Guarda los ajustes en el archivo JSON
        /// </summary>
        private void GuardarAjustes()
        {
            try
            {
                nucleo.GuardarAjustes(new Ajustes
                {
                    Apariencia = cbxApariencia.SelectedIndex < 0 ? "Moderno" : cbxApariencia.SelectedItem.ToString(),
                    CopiarA = btnRutaCopiaArchivos.Text != "" ? btnRutaCopiaArchivos.Text : string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"\SyncBD"),
                    Idioma = "",
                    TiempoDesconexion = Convert.ToInt32(txtTiempoDesconexion.Text != "" ? txtTiempoDesconexion.Text : "9999") * 1000,
                    TiempoReConexion = Convert.ToInt32(txtTiempoReconexion.Text != "" ? txtTiempoReconexion.Text : "3") * 1000,
                    DiferenteSincronizacion = false,
                    RutaGuardadoPermanente = false,
                    MinimizarAlIniciar = chkMinimizarAlIniciar.Checked,
                    IniciarConWindows = chkIniciarConWindows.Checked
                });

                log.AppendLog("[Principal] Se guardaron los ajustes correctamente");
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x007, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x007", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 6);
            }
        }

        /// <summary>
        /// Restaura todos los ajustes a forma predeterminada
        /// </summary>
        private void RestaurarAjustes()
        {
            try
            {
                if (!Directory.Exists(string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"\SyncBD")))
                {
                    Directory.CreateDirectory(string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"\SyncBD"));
                }

                nucleo.GuardarAjustes(new Ajustes
                {
                    Apariencia = "Moderno",
                    CopiarA = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"\SyncBD"),
                    Idioma = "",
                    TiempoDesconexion = 9999000,
                    TiempoReConexion = 3000,
                    DiferenteSincronizacion = false,
                    RutaGuardadoPermanente = false,
                    MinimizarAlIniciar = false,
                    IniciarConWindows = false
                });

                log.AppendLog("[Principal] Se restauraron los ajustes correctamente");
                
                LeerAjustes();
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x008, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x008", "Por favor dirijase al manual de usuario para detalles", SyncBD.Properties.Resources.Cancel_32px, 7);
            }
        }

        /// <summary>
        /// Verfica que el equipo cuente con conexion a internet
        /// </summary>
        /// <returns>Devuelve true cuando existe conexion a internet</returns>
        private bool VerificarConexionInternet()
        {
            try
            {
                Ping peticion = new Ping();

                PingReply respuesta = peticion.Send("www.google.com.mx");

                log.AppendLog("[Internet] Conexión a internet detectada");

                return true;
            }
            catch (Exception)
            {
                log.AppendLog("[Internet] Sin conexión a internet");

                return false;
            }
        }

        /// <summary>
        /// Obtiene los movimientos de la base de datos para llenar los datagridview (Ajusta el ancho de las columnas)
        /// </summary>
        private void CargarMovimientos()
        {
            try
            {
                enlace.ListaMovimientosServidorRemoto(gdcHistorialRemoto, gdvHistorialRemoto);
                enlace.ListaMovimientosServidorLocal(gdcHistorialLocal, gdvHistorialLocal);
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x009, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x009", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 8);
            }
        }

        /// <summary>
        /// Determina la visibilidad de los botones de sincronizacion con servidores dependiendo si existen configuraciones o no
        /// </summary>
        private void VerificarVisibilidadServidores()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones != null)
                {
                    if (!string.IsNullOrEmpty(configuraciones.Remoto.Servidor))
                    {
                        btnMenuRemoto.Visibility = BarItemVisibility.Always;

                        btnConfigurarRemoto.Visible = true;

                        xtabRemoto.PageVisible = true;

                        xtabPrincipal.SelectedTabPage = xtabPrincipal.TabPages[0];

                        btnVerRemotoStrip.Visible = true;
                    }

                    else
                    {
                        btnMenuRemoto.Visibility = BarItemVisibility.Never;

                        btnConfigurarRemoto.Visible = false;

                        btnVerRemotoStrip.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(configuraciones.Local.Servidor))
                    {
                        btnMenuLocal.Visibility = BarItemVisibility.Always;

                        btnConfigurarLocal.Visible = true;

                        xtabLocal.PageVisible = true;

                        xtabPrincipal.SelectedTabPage = xtabPrincipal.TabPages[1];

                        btnVerLocalStrip.Visible = true;
                    }

                    else
                    {
                        btnMenuLocal.Visibility = BarItemVisibility.Never;

                        btnConfigurarLocal.Visible = false;

                        btnVerLocalStrip.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(configuraciones.Local.Servidor) && !string.IsNullOrEmpty(configuraciones.Remoto.Servidor))
                    {
                        btnMenuRemoto.Visibility = BarItemVisibility.Always;

                        btnConfigurarRemoto.Visible = true;

                        btnMenuLocal.Visibility = BarItemVisibility.Always;

                        btnConfigurarLocal.Visible = true;

                        xtabRemoto.PageVisible = true;

                        xtabPrincipal.SelectedTabPage = xtabPrincipal.TabPages[0];

                        btnVerRemotoStrip.Visible = true;

                        btnVerLocalStrip.Visible = true;
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Determina que botones, etiquetas y demás controles, serán visibles cuando esta activa o no la sincronización del servidor remoto
        /// </summary>
        /// <param name="visible">True para que haga visible todos los controles, restaurados en su forma original</param>
        private void VerificarVisibilidadBotonesRemoto(bool visible)
        {
            //Cuando los controles de informacion deben estar ocultos y todo normal
            if (visible)
            {
                relojRemoto = new TimeSpan(0, 0, 0);
                tmpRemotoTiempoTranscurrido.Enabled = false;
                petAccionesRemoto.Visible = true;
                lblAccionesRemoto.Visible = true;
                gbcAccionesRemoto.Visible = true;

                petInfoRemoto.Visible = false;
                lblInfoRemoto.Visible = false;
                gbcPanelInformativoRemoto.Visible = false;
                lblTiempoRemoto.Text = "Tiempo transcurrido:";
                lblVelocidadRemoto.Text = "Velocidad de transferencia:";
                lblProgresoTotalRemoto.Text = "0% total completado";
                pbrProgresoTotalRemoto.Position = 0;
                lblProgresoArchivoRemoto.Text = "0% completado";
                pbrProgresoArchivoRemoto.Position = 0;
                lblNombreArchivoRemoto.Text = "";

                petAccionesLocal.Visible = true;
                lblAccionesLocal.Visible = true;
                gbcAccionesLocal.Visible = true;

                btnRemotoStrip.Visible = true;
                btnLocalStrip.Visible = true;
                btnRemotoCancelarStrip.Visible = false;
                btnLocalCancelarStrip.Visible = false;
            }

            else
            {
                petInfoRemoto.Visible = true;
                lblInfoRemoto.Visible = true;
                gbcPanelInformativoRemoto.Visible = true;
                lblTiempoRemoto.Text = "Tiempo transcurrido:";
                lblVelocidadRemoto.Text = "Velocidad de transferencia:";
                lblProgresoTotalRemoto.Text = "0% total completado";
                pbrProgresoTotalRemoto.Position = 0;
                lblProgresoArchivoRemoto.Text = "0% completado";
                pbrProgresoArchivoRemoto.Position = 0;
                lblNombreArchivoRemoto.Text = "";

                petAccionesRemoto.Visible = false;
                lblAccionesRemoto.Visible = false;
                gbcAccionesRemoto.Visible = false;

                tmpRemotoTiempoTranscurrido.Enabled = true;

                petAccionesLocal.Visible = false;
                lblAccionesLocal.Visible = false;
                gbcAccionesLocal.Visible = false;

                btnRemotoStrip.Visible = false;
                btnLocalStrip.Visible = false;
                btnRemotoCancelarStrip.Visible = true;
                btnLocalCancelarStrip.Visible = false;
            }
        }

        /// <summary>
        /// Determina que botones, etiquetas y demás controles, serán visibles cuando esta activa o no la sincronización del servidor local
        /// </summary>
        /// <param name="visible">True para que haga visible todos los controles, restaurados en su forma original</param>
        private void VerificarVisibilidadBotonesLocal(bool visible)
        {
            //Cuando los controles de informacion deben estar ocultos y todo normal
            if (visible)
            {
                relojLocal = new TimeSpan(0, 0, 0);
                tmpLocalTiempoTranscurrido.Enabled = false;
                petAccionesLocal.Visible = true;
                lblAccionesLocal.Visible = true;
                gbcAccionesLocal.Visible = true;

                petInfoLocal.Visible = false;
                lblInfoLocal.Visible = false;
                gbcPanelInformativoLocal.Visible = false;
                lblTiempoLocal.Text = "Tiempo transcurrido:";
                lblVelocidadLocal.Text = "Velocidad de transferencia:";
                lblProgresoTotalLocal.Text = "0% total completado";
                pbrProgresoTotalLocal.Position = 0;
                lblProgresoArchivoLocal.Text = "0% completado";
                pbrProgresoArchivoLocal.Position = 0;
                lblNombreArchivoLocal.Text = "";

                petAccionesRemoto.Visible = true;
                lblAccionesRemoto.Visible = true;
                gbcAccionesRemoto.Visible = true;

                btnRemotoStrip.Visible = true;
                btnLocalStrip.Visible = true;
                btnRemotoCancelarStrip.Visible = false;
                btnLocalCancelarStrip.Visible = false;
            }

            else
            {
                petInfoLocal.Visible = true;
                lblInfoLocal.Visible = true;
                gbcPanelInformativoLocal.Visible = true;
                lblTiempoLocal.Text = "Tiempo transcurrido:";
                lblVelocidadLocal.Text = "Velocidad de transferencia:";
                lblProgresoTotalLocal.Text = "0% total completado";
                pbrProgresoTotalLocal.Position = 0;
                lblProgresoArchivoLocal.Text = "0% completado";
                pbrProgresoArchivoLocal.Position = 0;
                lblNombreArchivoLocal.Text = "";

                petAccionesLocal.Visible = false;
                lblAccionesLocal.Visible = false;
                gbcAccionesLocal.Visible = false;

                tmpLocalTiempoTranscurrido.Enabled = true;

                petAccionesRemoto.Visible = false;
                lblAccionesRemoto.Visible = false;
                gbcAccionesRemoto.Visible = false;

                btnRemotoStrip.Visible = false;
                btnLocalStrip.Visible = false;
                btnRemotoCancelarStrip.Visible = false;
                btnLocalCancelarStrip.Visible = true;
            }
        }

        /// <summary>
        /// Verifica la existencia de la base de datos en la carpeta "General", sino existe, procede a copiarla de Origin.db
        /// </summary>
        private void ValidarHistorial()
        {
            try
            {
                if (!Directory.Exists(string.Concat(Application.StartupPath, @"\General")))
                {
                    Directory.CreateDirectory(string.Concat(Application.StartupPath, @"\General"));
                }

                if (!File.Exists(string.Concat(Application.StartupPath, @"\General\Historial.db")))
                {
                    FileSystem.CopyFile(string.Concat(Application.StartupPath, @"\Origin.db"), string.Concat(Application.StartupPath, @"\General\Historial.db"), true);
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x010, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x010", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 9);
            }
        }

        /// <summary>
        /// Copia todos los archivos y subcarpetas de un directorio en especifico
        /// </summary>
        /// <param name="pathOrigen">Directorio a transferir</param>
        /// <param name="pathDestino">Destino del directorio a transferir</param>
        /// <returns>True cuando se copió la carpeta correctamente</returns>
        private bool Transferir(string pathOrigen, string pathDestino)
        {
            try
            {
                if (!Directory.Exists(pathDestino))
                {
                    Directory.CreateDirectory(pathDestino);
                }

                FileSystem.CopyDirectory(pathOrigen, pathDestino, UIOption.AllDialogs, UICancelOption.ThrowException);

                return true;
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x011, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x011", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 10);

                return false;
            }
        }
        
        #endregion

        private void Principal_Shown(object sender, EventArgs e)
        {
            try
            {
                ValidarHistorial();

                CargarMovimientos();

                VerificarVisibilidadServidores();

                log.AppendLog("[Principal] Se verifico y cargo historial");

                Ajustes ajustes = nucleo.LeerAjustes();

                if (ajustes != null)
                {
                    if (ajustes.MinimizarAlIniciar)
                    {
                        WindowState = FormWindowState.Minimized;
                    }
                }

                if (VerificarConexionInternet())
                {
                    Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                    if (configuraciones != null)
                    {
                        if (!string.IsNullOrEmpty(configuraciones.Remoto.Servidor))
                        {
                            SincronizarConRemoto();
                        }

                        else if (!string.IsNullOrEmpty(configuraciones.Local.Servidor))
                        {
                            SincronizarConLocal();
                        }
                    }
                }

                else
                {
                    Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                    if (configuraciones != null)
                    {
                        if (!string.IsNullOrEmpty(configuraciones.Local.Servidor))
                        {
                            SincronizarConLocal();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x013, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x013", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 12);
            }
        }

        #region Metodos de transferencia remoto

        /// <summary>
        /// Detecta errores durante la transferencia con el servidor remoto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sesion_Failed(object sender, FailedEventArgs e)
        {
            try
            {
                XtraMessageBox.Show(UserLookAndFeel.Default, e.Error.Message.ToString(), "SyncBD | Error de transferencia con servidor Remoto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Progreso total y velocidad de conexión con el servidor remoto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sesion_FileTransferProgress(object sender, FileTransferProgressEventArgs e)
        {
            try
            {
                //El archivo debe descargarse
                if (!cancelarTransferenciaRemoto)
                {
                    if (pbrProgresoTotalRemoto.Position > 0)
                    {
                        pbrProgresoTotalRemoto.Invoke(new Action(() => pbrProgresoTotalRemoto.Position = 0));
                    }

                    if (pbrProgresoArchivoRemoto.Position > 0)
                    {
                        pbrProgresoArchivoRemoto.Invoke(new Action(() => pbrProgresoArchivoRemoto.Position = 0));
                    }

                    lblNombreArchivoRemoto.Invoke(new Action(() => lblNombreArchivoRemoto.Text = Path.GetFileName(e.FileName)));
                    lblVelocidadRemoto.Invoke(new Action(() => lblVelocidadRemoto.Text = string.Format("Velocidad de transferencia: {0} Kb/s.", (e.CPS / 1024).ToString())));

                    lblProgresoTotalRemoto.Invoke(new Action(() => lblProgresoTotalRemoto.Text = string.Format("{0}% total completado", (e.OverallProgress * 100))));
                    lblProgresoArchivoRemoto.Invoke(new Action(() => lblProgresoArchivoRemoto.Text = string.Format("{0}% completado", (e.FileProgress * 100))));

                    pbrProgresoTotalRemoto.Invoke(new Action(() => pbrProgresoTotalRemoto.Increment(Convert.ToInt32(e.OverallProgress * 100))));
                    pbrProgresoArchivoRemoto.Invoke(new Action(() => pbrProgresoArchivoRemoto.Increment(Convert.ToInt32(e.FileProgress * 100))));
                }

                else
                {
                    Ocupar(SSMCancelando);

                    e.Cancel = cancelarTransferenciaRemoto;

                    if (e.Cancel == cancelarTransferenciaRemoto)
                    {
                        Liberar(SSMCancelando);

                        log.AppendLog("[Transferencia Remota] El usuario cancelo la transferencia");

                        cancelarTransferenciaRemoto = false;

                        Servidores servidores = nucleo.LeerConfiguracionServidores();

                        enlace.CrearMovimientoRemoto(new MovimientoRemoto { Fecha = DateTime.Now, Archivos = archivosRemotos, Desde = seguridad.Desencriptar(servidores.Remoto.Servidor), Hacia = "Equipo Local", Duracion = relojRemoto.ToString(), Estado = "Cancelada" });

                        VerificarVisibilidadBotonesRemoto(true);

                        MostrarNotificacion("Sincronización cancelada", "Se canceló la sincronización correctamente", Properties.Resources.Ok_32px, 38);
                    }

                    else
                    {
                        Liberar(SSMCancelando);

                        log.AppendLog("[Transferencia Remota] Imposible cancelar transferencia");

                        MostrarNotificacion("Imposible cancelar", "Imposible cancelar la sincronización", Properties.Resources.Attention_32px, 39);
                    }
                }
            }
            catch (Exception)
            {
                Liberar(SSMCancelando);
            }
        }

        /// <summary>
        /// Indica información del archivo transferido correctamente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sesion_FileTransferred(object sender, TransferEventArgs e)
        {
            try
            {
                //log.AppendLog(string.Format("[Descarga completa] {0}\nDesde {1} A {2}", e.FileName, e.Side, e.Destination));
                archivosRemotos++;
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Sesión que enlaza con el servidor remoto para realizar la sincronización
        /// </summary>
        /// <param name="cliente">Datos de la sesion para conectar con el servidor (Requiere previa validación)</param>
        /// <param name="servidor">Datos del servidor remoto al que se conectará</param>
        /// <returns>Devuelve "Sincronización con el servidor remoto completada" cuando termina correctamente</returns>
        private bool SincronizarDatosRemotos(SessionOptions cliente, Servidores servidor)
        {
            try
            {
                if (!Directory.Exists(dirArchivosRemotosOriginal))
                {
                    Directory.CreateDirectory(dirArchivosRemotosOriginal);
                }

                bool resultado = false;

                Ajustes ajustes = nucleo.LeerAjustes();

                using (Session sesion = new Session())
                {
                    sesion.DebugLogLevel = 2;

                    sesion.DebugLogPath = Nucleo.logRemoto;

                    sesion.Failed += Sesion_Failed;
                    
                    sesion.FileTransferProgress += Sesion_FileTransferProgress;

                    sesion.FileTransferred += Sesion_FileTransferred;

                    sesion.ReconnectTimeInMilliseconds = (Convert.ToInt32(ajustes != null ? ajustes.TiempoReConexion.ToString() : "3")) * 1000;

                    sesion.Open(cliente);

                    SynchronizationResult resultadoSincronizacion = sesion.SynchronizeDirectories(SynchronizationMode.Local, dirArchivosRemotosOriginal, seguridad.Desencriptar(servidor.Remoto.Directorio), true, false, SynchronizationCriteria.Either);
                    
                    resultadoSincronizacion.Check();
                    
                    resultado = resultadoSincronizacion.IsSuccess;
                }

                return resultado;
            }
            catch (Exception ex)
            {
                VerificarVisibilidadBotonesRemoto(true);

                log.AppendLog("[Error] Se produjo el error con Clave: 0x014, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x014", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 13);

                return false;
            }
        }
        
        /// <summary>
        /// Comienza la sincronización con el servidor remoto de forma asincrona
        /// </summary>
        private async void SincronizarConRemoto()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones.Remoto.Servidor != "")
                {
                    if (string.IsNullOrEmpty(configuraciones.Remoto.Servidor))
                    {
                        MostrarNotificacion("Servidor remoto sin configurar...", "No hay configuraciones del servidor remoto", Properties.Resources.Attention_32px, 15);
                    }

                    else
                    {
                        SessionOptions cliente = nucleo.ValidarServidorRemoto(configuraciones);

                        if (cliente != null)
                        {
                            VerificarVisibilidadBotonesRemoto(false);

                            MostrarNotificacion("Iniciando conexión...", "Sincronizando con servidor remoto", Properties.Resources.Info_32px, 16);

                            archivosRemotos = 0;

                            Task<bool> comenzarSincronizacion = new Task<bool>(() => SincronizarDatosRemotos(cliente, configuraciones));
                            comenzarSincronizacion.Start();

                            //Cuando termina la sincronizacion
                            if (await comenzarSincronizacion == true)
                            {
                                MostrarNotificacion("Sincronización finalizada", "Se ha completado la sincronización con el servidor Remoto", Properties.Resources.Ok_32px, 17);
                                
                                Ajustes ajustes = nucleo.LeerAjustes();

                                //En caso de que existan archivos o la carpeta de copias de respaldo no tenga ninguna
                                if (archivosRemotos > 0 || !Directory.EnumerateDirectories(ajustes.CopiarA).Any())
                                {
                                    //Si la direccion de copia de respaldo no es nula
                                    if (ajustes.CopiarA != null)
                                    {
                                        Ocupar(SSMDirVerificacion);

                                        //Evalua si el directorio Archivos_remotos es demasiado grande (Devuelve true cuando sobrepasa de 5 Gb.)
                                        if (nucleo.ObtenerPesoDirectorio(dirArchivosRemotosOriginal))
                                        {
                                            Liberar(SSMDirVerificacion);

                                            if(XtraMessageBox.Show(UserLookAndFeel.Default, "Los archivos remotos sobrepasan 5 Gb. y realizar una copia de respaldo podria llevar algo de tiempo\n\n¿Aún así desea realizar la copia de respaldo?", "SyncBD | Archivos demasiado pesados", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                                            {
                                                string nombreCarpeta = "SyncBD_" + DateTime.Now.ToString("yyyyMMddHHmmss");

                                                if (!Directory.Exists(string.Concat(ajustes.CopiarA, @"\", nombreCarpeta)))
                                                {
                                                    Directory.CreateDirectory(string.Concat(ajustes.CopiarA, @"\", nombreCarpeta));
                                                }

                                                if (Transferir(dirArchivosRemotosOriginal, string.Concat(ajustes.CopiarA, @"\", nombreCarpeta)))
                                                {
                                                    MostrarNotificacion("Archivos transferidos", "Se han transferido los archivos al directorio copia", Properties.Resources.Ok_32px, 18);

                                                    enlace.CrearMovimientoRemoto(new MovimientoRemoto { Fecha = DateTime.Now, Archivos = archivosRemotos, Desde = seguridad.Desencriptar(configuraciones.Remoto.Servidor), Hacia = "Equipo Local", Duracion = relojRemoto.ToString(), Estado = "Exitosa" });
                                                }

                                                else
                                                {
                                                    MostrarNotificacion("No se transfirieron los archivos", "Ocurrió un imprevisto al transferir los archivos", Properties.Resources.Attention_32px, 19);

                                                    enlace.CrearMovimientoRemoto(new MovimientoRemoto { Fecha = DateTime.Now, Archivos = archivosRemotos, Desde = seguridad.Desencriptar(configuraciones.Remoto.Servidor), Hacia = "Equipo Local", Duracion = relojRemoto.ToString(), Estado = "Exitosa [1]" });
                                                }
                                            }

                                            else
                                            {
                                                enlace.CrearMovimientoRemoto(new MovimientoRemoto { Fecha = DateTime.Now, Archivos = archivosRemotos, Desde = seguridad.Desencriptar(configuraciones.Remoto.Servidor), Hacia = "Equipo Local", Duracion = relojRemoto.ToString(), Estado = "Exitosa [1]" });
                                            }
                                        }
                                        //Cuando no sobrepasa los 5Gb.
                                        else
                                        {
                                            Liberar(SSMDirVerificacion);

                                            string nombreCarpeta = "SyncBD_" + DateTime.Now.ToString("yyyyMMddHHmmss");

                                            if (!Directory.Exists(string.Concat(ajustes.CopiarA, @"\", nombreCarpeta)))
                                            {
                                                Directory.CreateDirectory(string.Concat(ajustes.CopiarA, @"\", nombreCarpeta));
                                            }

                                            if (Transferir(dirArchivosRemotosOriginal, string.Concat(ajustes.CopiarA, @"\", nombreCarpeta)))
                                            {
                                                MostrarNotificacion("Archivos transferidos", "Se han transferido los archivos al directorio copia", Properties.Resources.Ok_32px, 18);

                                                enlace.CrearMovimientoRemoto(new MovimientoRemoto { Fecha = DateTime.Now, Archivos = archivosRemotos, Desde = seguridad.Desencriptar(configuraciones.Remoto.Servidor), Hacia = "Equipo Local", Duracion = relojRemoto.ToString(), Estado = "Exitosa" });
                                            }

                                            else
                                            {
                                                MostrarNotificacion("No se transfirieron los archivos", "Ocurrió un imprevisto al transferir los archivos", Properties.Resources.Attention_32px, 19);

                                                enlace.CrearMovimientoRemoto(new MovimientoRemoto { Fecha = DateTime.Now, Archivos = archivosRemotos, Desde = seguridad.Desencriptar(configuraciones.Remoto.Servidor), Hacia = "Equipo Local", Duracion = relojRemoto.ToString(), Estado = "Exitosa [1]" });
                                            }
                                        }
                                    }
                                    //Cuando no existe configuracion de directorio respaldo
                                    else
                                    {
                                        MostrarNotificacion("Sin dirección de guardado", "No ha configurado la dirección de guardado de archivos", Properties.Resources.Attention_32px, 20);

                                        enlace.CrearMovimientoRemoto(new MovimientoRemoto { Fecha = DateTime.Now, Archivos = archivosRemotos, Desde = seguridad.Desencriptar(configuraciones.Remoto.Servidor), Hacia = "Equipo Local", Duracion = relojRemoto.ToString(), Estado = "Exitosa [2]" });
                                    }
                                }

                                else
                                {
                                    enlace.CrearMovimientoRemoto(new MovimientoRemoto { Fecha = DateTime.Now, Archivos = archivosRemotos, Desde = seguridad.Desencriptar(configuraciones.Remoto.Servidor), Hacia = "Equipo Local", Duracion = relojRemoto.ToString(), Estado = "Sin cambios" });

                                    MostrarNotificacion("Sin copia de respaldo", "Debido a que no hay cambios, puede utilizar la última carpeta copiada", Properties.Resources.Ok_32px, 21);
                                }
                            }

                            else
                            {
                                enlace.CrearMovimientoRemoto(new MovimientoRemoto { Fecha = DateTime.Now, Archivos = archivosRemotos, Desde = seguridad.Desencriptar(configuraciones.Remoto.Servidor), Hacia = "Equipo Local", Duracion = relojRemoto.ToString(), Estado = "Incompleta" });

                                MostrarNotificacion("Sincronización incompleta", "No se pudo completar la sincronización con el servidor Remoto", Properties.Resources.Attention_32px, 22);
                            }
                        }

                        else
                        {
                            MostrarNotificacion("Servidor inaccesible", "Se perdió la comunicación con el servidor Remoto, reintente por favor", Properties.Resources.Attention_32px, 23);
                        }
                    }

                    CargarMovimientos();
                }

                else
                {
                    MostrarNotificacion("Faltan datos de conexión", "El servidor remoto no tiene configuración", Properties.Resources.Attention_32px, 24);
                }

                VerificarVisibilidadBotonesRemoto(true);
            }
            catch (Exception ex)
            {
                VerificarVisibilidadBotonesRemoto(true);
                
                log.AppendLog("[Error] Se produjo el error con Clave: 0x014, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x014", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 13);
            }
        }

        #endregion

        #region Metodos de transferencia local

        /// <summary>
        /// Detecta errores durante la transferencia con el servidor local
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sesion_Failed1(object sender, FailedEventArgs e)
        {
            try
            {
                XtraMessageBox.Show(UserLookAndFeel.Default, e.Error.Message.ToString(), "SyncBD | Error de transferencia con servidor Local", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Progreso total y velocidad de conexión con el servidor local
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sesion_FileTransferProgress1(object sender, FileTransferProgressEventArgs e)
        {
            try
            {
                if (!cancelarTransferenciaLocal)
                {
                    if (pbrProgresoTotalLocal.Position > 0)
                    {
                        pbrProgresoTotalLocal.Invoke(new Action(() => pbrProgresoTotalLocal.Position = 0));
                    }

                    if (pbrProgresoArchivoLocal.Position > 0)
                    {
                        pbrProgresoArchivoLocal.Invoke(new Action(() => pbrProgresoArchivoLocal.Position = 0));
                    }

                    lblNombreArchivoLocal.Invoke(new Action(() => lblNombreArchivoLocal.Text = Path.GetFileName(e.FileName)));
                    lblVelocidadLocal.Invoke(new Action(() => lblVelocidadLocal.Text = string.Format("Velocidad de transferencia: {0} Kb/s.", (e.CPS / 1024).ToString())));

                    lblProgresoTotalLocal.Invoke(new Action(() => lblProgresoTotalLocal.Text = string.Format("{0}% total completado", (e.OverallProgress * 100))));
                    lblProgresoArchivoLocal.Invoke(new Action(() => lblProgresoArchivoLocal.Text = string.Format("{0}% completado", (e.FileProgress * 100))));

                    pbrProgresoTotalLocal.Invoke(new Action(() => pbrProgresoTotalLocal.Increment(Convert.ToInt32(e.OverallProgress * 100))));
                    pbrProgresoArchivoLocal.Invoke(new Action(() => pbrProgresoArchivoLocal.Increment(Convert.ToInt32(e.FileProgress * 100))));
                }

                else
                {
                    Ocupar(SSMCancelando);

                    e.Cancel = cancelarTransferenciaLocal;

                    if(e.Cancel == cancelarTransferenciaLocal)
                    {
                        Liberar(SSMCancelando);

                        log.AppendLog("[Transferencia Local] El usuario cancelo la transferencia del archivo: " + e.FileName);

                        cancelarTransferenciaLocal = false;

                        Servidores servidores = nucleo.LeerConfiguracionServidores();

                        enlace.CrearMovimientoRemoto(new MovimientoRemoto { Fecha = DateTime.Now, Archivos = archivosRemotos, Desde = "Equipo Local", Hacia = seguridad.Desencriptar(servidores.Local.Servidor), Duracion = relojRemoto.ToString(), Estado = "Cancelada" });

                        VerificarVisibilidadBotonesLocal(true);

                        MostrarNotificacion("Sincronización cancelada", "Se canceló la sincronización correctamente", Properties.Resources.Ok_32px, 38);
                    }

                    else
                    {
                        Liberar(SSMCancelando);

                        log.AppendLog("[Transferencia Local] Imposible cancelar transferencia");

                        MostrarNotificacion("Imposible cancelar", "Imposible cancelar la sincronización", Properties.Resources.Attention_32px, 39);
                    }
                }
            }
            catch (Exception)
            {
                Liberar(SSMCancelando);
            }
        }

        /// <summary>
        /// Indica información del archivo transferido correctamente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sesion_FileTransferred1(object sender, TransferEventArgs e)
        {
            try
            {
                archivosLocales++;
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Sesión que enlaza con el servidor remoto para realizar la sincronización
        /// </summary>
        /// <param name="cliente">Datos de la sesion para conectar con el servidor (Requiere previa validación)</param>
        /// <param name="servidor">Datos del servidor remoto al que se conectará</param>
        /// <returns>Devuelve "Sincronización con servidor local completada" cuando termina correctamente</returns>
        private bool SincronizarDatosLocales(SessionOptions cliente, Servidores servidor)
        {
            try
            {
                if (!Directory.Exists(dirArchivosRemotosOriginal))
                {
                    Directory.CreateDirectory(dirArchivosRemotosOriginal);
                }

                bool resultado = false;

                Ajustes ajustes = nucleo.LeerAjustes();

                using (Session sesion = new Session())
                {
                    sesion.DebugLogLevel = 2;

                    sesion.DebugLogPath = Nucleo.logLocal;

                    sesion.Failed += Sesion_Failed1;

                    sesion.FileTransferProgress += Sesion_FileTransferProgress1;

                    sesion.FileTransferred += Sesion_FileTransferred1;
                    
                    sesion.ReconnectTimeInMilliseconds = (Convert.ToInt32(ajustes != null ? ajustes.TiempoReConexion.ToString() : "3")) * 1000;

                    sesion.Open(cliente);

                    SynchronizationResult resultadoSincronizacion = sesion.SynchronizeDirectories(SynchronizationMode.Remote, dirArchivosRemotosOriginal, seguridad.Desencriptar(servidor.Local.Directorio), false, false, SynchronizationCriteria.Either);

                    resultadoSincronizacion.Check();

                    resultado = resultadoSincronizacion.IsSuccess;
                }

                return resultado;

            }
            catch (Exception ex)
            {
                VerificarVisibilidadBotonesRemoto(true);

                log.AppendLog("[Error] Se produjo el error con Clave: 0x014, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x014", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 13);

                return false;
            }
        }
        
        /// <summary>
        /// Comienza la sincronización con el servidor local de forma asincrona
        /// </summary>
        private async void SincronizarConLocal()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones.Local.Servidor != null)
                {
                    if (string.IsNullOrEmpty(configuraciones.Local.Servidor))
                    {
                        MostrarNotificacion("Servidor local sin configurar...", "No hay configuraciones del servidor local", Properties.Resources.Attention_32px, 25);
                    }

                    else
                    {
                        SessionOptions cliente = nucleo.ValidarServidorLocal(configuraciones);

                        if (cliente != null)
                        {
                            VerificarVisibilidadBotonesLocal(false);

                            MostrarNotificacion("Iniciando conexión...", "Sincronizando con servidor local", Properties.Resources.Info_32px, 26);

                            archivosLocales = 0;

                            Task<bool> comenzarSincronizacion = new Task<bool>(() => SincronizarDatosLocales(cliente, configuraciones));
                            comenzarSincronizacion.Start();

                            if (await comenzarSincronizacion == true)
                            {
                                MostrarNotificacion("Sincronización finalizada", "Se ha completado la sincronización con el servidor Local", Properties.Resources.Ok_32px, 27);

                                enlace.CrearMovimientoLocal(new MovimientoLocal { Fecha = DateTime.Now, Archivos = archivosLocales, Desde = "Equipo Local", Hacia = seguridad.Desencriptar(configuraciones.Local.Servidor), Duracion = relojLocal.ToString(), Estado = "Exitosa" });
                            }

                            else
                            {
                                MostrarNotificacion("Verificando ajustes", "Reintente sincronización", Properties.Resources.Attention_32px, 28);
                            }
                        }

                        else
                        {
                            MostrarNotificacion("Servidor inaccesible", "Se perdió la comunicación con el servidor Local, reintente por favor", Properties.Resources.Attention_32px, 29);
                        }
                    }
                }

                else
                {
                    MostrarNotificacion("Faltan datos de conexión", "El servidor local no tiene configuración", Properties.Resources.Attention_32px, 30);
                }

                VerificarVisibilidadBotonesLocal(true);

                CargarMovimientos();
            }
            catch (Exception ex)
            {
                VerificarVisibilidadBotonesLocal(true);

                log.AppendLog("[Error] Se produjo el error con Clave: 0x014, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x014", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 13);
            }
        }

        #endregion

        #region Eventos de controles

        private void btnRutaCopiaArchivos_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                if (fbdGuardarEn.ShowDialog() == DialogResult.OK)
                {
                    btnRutaCopiaArchivos.Text = fbdGuardarEn.SelectedPath;

                    GuardarAjustes();
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnBorrarHistoriales_Click(object sender, EventArgs e)
        {
            try
            {
                if(XtraMessageBox.Show(UserLookAndFeel.Default, "Está acción es permanente y no se podrá cancelar.\n\n¿Realmente desea borrar el historial de sincronizaciones?", "SyncBD | Confirme acción", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (enlace.BorrarHistorial())
                    {
                        CargarMovimientos();

                        MostrarNotificacion("Historial borrado", "Se ha borrado el todo el historial correctamente", Properties.Resources.Ok_32px, 31);
                    }

                    else
                    {
                        CargarMovimientos();

                        MostrarNotificacion("No se borró historial", "No fue posible borrar todo el historial", Properties.Resources.Attention_32px, 32);
                    }
                }
            }
            catch (Exception)
            {
                
            }
        }

        private void btnConfigurarRemoto_Click(object sender, EventArgs e)
        {
            try
            {
                ConfigRemoto mostrar = ConfigRemoto.ObtenerInstancia();

                Hide();

                mostrar.ShowDialog();

                if (ConfigRemoto.resultado == DialogResult.OK)
                {
                    CargarMovimientos();

                    VerificarVisibilidadServidores();
                    
                    Show();

                    mostrar.Dispose();
                }

                else
                {
                    Show();

                    mostrar.Dispose();

                    CargarMovimientos();

                    VerificarVisibilidadServidores();

                    XtraMessageBox.Show(UserLookAndFeel.Default, "¡Acción cancelada!, no se modificaron los ajustes para el servidor remoto", "SyncBD | Sin modificaciones", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnConfigurarLocal_Click(object sender, EventArgs e)
        {
            try
            {
                ConfigLocal mostrar = ConfigLocal.ObtenerInstancia();

                Hide();

                mostrar.ShowDialog();

                if (ConfigLocal.resultado == DialogResult.OK)
                {
                    CargarMovimientos();

                    VerificarVisibilidadServidores();

                    Show();

                    mostrar.Dispose();
                }

                else
                {
                    Show();

                    mostrar.Dispose();

                    CargarMovimientos();

                    VerificarVisibilidadServidores();

                    XtraMessageBox.Show(UserLookAndFeel.Default, "¡Acción cancelada!, no se modificaron los ajustes para el servidor local", "SyncBD | Sin modificaciones", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Permite cargar archivos al servidor local (Borra los archivos del servidor remoto, para colocar estos archivos)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCargarArchivosDesde_Click(object sender, EventArgs e)
        {
            try
            {
                if(XtraMessageBox.Show(UserLookAndFeel.Default, "¿Desea sincronizar los archivos desde una carpeta externa o medio extraíble?\n\nSi es así, asegurese de conectar ahora el dispositivo. (Este proceso borrará los archivos del servidor remoto)", "SyncBD | Sincronización externa", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    fbdGuardarEn.Title = "Escoga una carpeta ha cargar...";
                    fbdGuardarEn.Description = "Seleccione la carpeta que desee cargar al servidor local...";
                    fbdGuardarEn.ShowNewFolderButton = false;

                    if (fbdGuardarEn.ShowDialog() == DialogResult.OK)
                    {
                        if (Directory.Exists(dirArchivosRemotosOriginal))
                        {
                            DirectoryInfo datosDirectorio = new DirectoryInfo(dirArchivosRemotosOriginal);

                            foreach (FileInfo archivo in datosDirectorio.EnumerateFiles())
                            {
                                archivo.Delete();
                            }

                            foreach (DirectoryInfo directorio in datosDirectorio.GetDirectories())
                            {
                                directorio.Delete(true);
                            }
                        }

                        if (Transferir(fbdGuardarEn.SelectedPath, dirArchivosRemotosOriginal))
                        {
                            if(XtraMessageBox.Show(UserLookAndFeel.Default, "El programa a asegurado los archivos, ahora es momento de cargarlos al servidor local.\n\n¿Desea continuar el proceso de carga?", "SyncBD | Archivos asegurados, esperando confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                            {
                                if (!VerificarConexionInternet())
                                {
                                    SincronizarConLocal();
                                }

                                else
                                {
                                    SincronizarConLocal();
                                }
                            }
                        }

                        else
                        {
                            XtraMessageBox.Show(UserLookAndFeel.Default, "Se cancelo el proceso de sincronización desde medio externo, verifique que no tenga archivos abiertos o en ejecución que pertenezcan a dicha carpeta", "SyncBD | Acción cancelada", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnRemotoStrip_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificarConexionInternet())
                {
                    SincronizarConRemoto();
                }

                else
                {
                    if(XtraMessageBox.Show(UserLookAndFeel.Default, "No cuenta con conexión a internet, pero puede intentar en caso de que el servidor \"Remoto\" este conectado de forma local.\n\n¿Desea continuar?", "SyncBD | Sin conexión a internet", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        SincronizarConRemoto();
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnLocalStrip_Click(object sender, EventArgs e)
        {
            try
            {
                if (!VerificarConexionInternet())
                {
                    SincronizarConLocal();
                }

                else
                {
                    if (XtraMessageBox.Show(UserLookAndFeel.Default, "Al estar conectado a internet, SyncBD da preferencia a conexiones con el servidor \"Remoto\",cancelando acciones con el\nservidor \"Local\", pero aún así puede acceder a este último, si se encuentra accesible.\n\n¿Desea continuar?", "SyncBD | Se detectó conexión a internet", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        SincronizarConLocal();
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnSincronizarRemoto_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificarConexionInternet())
                {
                    SincronizarConRemoto();
                }

                else
                {
                    if (XtraMessageBox.Show(UserLookAndFeel.Default, "No cuenta con conexión a internet, pero puede intentar en caso de que el servidor \"Remoto\" este conectado de forma local.\n\n¿Desea continuar?", "SyncBD | Sin conexión a internet", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        SincronizarConRemoto();
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnSincronizarLocal_Click(object sender, EventArgs e)
        {
            try
            {
                if (!VerificarConexionInternet())
                {
                    SincronizarConLocal();
                }

                else
                {
                    if (XtraMessageBox.Show(UserLookAndFeel.Default, "Al estar conectado a internet, SyncBD da preferencia a conexiones con el servidor \"Remoto\",cancelando acciones con el\nservidor \"Local\", pero aún así puede acceder a este último, si se encuentra accesible.\n\n¿Desea continuar?", "SyncBD | Se detectó conexión a internet", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        SincronizarConLocal();
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void chkIniciarConWindows_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                string dirRegistro = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";

                if (chkIniciarConWindows.Checked)
                {
                    using (RegistryKey registro = Registry.CurrentUser.OpenSubKey(dirRegistro, true))
                    {
                        if (registro.OpenSubKey("SyncBD", true) == null)
                        {
                            registro.SetValue("SyncBD", Application.ExecutablePath);
                        }
                        else
                        {
                            registro.SetValue("SyncBD", Application.ExecutablePath);
                        }
                    }
                }

                else
                {
                    using (RegistryKey registro = Registry.CurrentUser.OpenSubKey(dirRegistro, true))
                    {
                        if (registro.OpenSubKey("SyncBD", true) == null)
                        {
                            registro.DeleteValue("SyncBD");
                        }
                    }
                }

                GuardarAjustes();
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x015, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x015", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 14);
            }
        }

        private void chkMinimizarAlIniciar_CheckedChanged(object sender, EventArgs e)
        {
            GuardarAjustes();
        }

        private void btnReestablecerAjustes_Click(object sender, EventArgs e)
        {
            if(XtraMessageBox.Show(UserLookAndFeel.Default, "¿En realidad desea reestablecer todos los ajustes?", "SyncBD | Confirme acción", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                RestaurarAjustes();
            }
        }

        private void Principal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.WindowsShutDown || e.CloseReason == CloseReason.TaskManagerClosing)
            {
                if (XtraMessageBox.Show(UserLookAndFeel.Default, "¿Desea cancelar las transferencias y cerrar el programa?\nSe conservará lo que halla descargado o cargado hasta el momento", "SyncBD | Confirme cierre", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    Application.Exit();
                }

                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void tmpLocalTiempoTranscurrido_Tick(object sender, EventArgs e)
        {
            try
            {
                relojLocal = relojLocal.Add(TimeSpan.FromSeconds(1));

                lblTiempoLocal.Text = "Tiempo transcurrido: " + relojLocal.ToString();
            }
            catch (Exception)
            {
                
            }
        }

        private void tmpRemotoTiempoTranscurrido_Tick(object sender, EventArgs e)
        {
            try
            {
                relojRemoto = relojRemoto.Add(TimeSpan.FromSeconds(1));

                lblTiempoRemoto.Text = "Tiempo transcurrido: " + relojRemoto.ToString();
            }
            catch (Exception)
            {
                
            }
        }

        private void btnAbrirCarpetaFinal_Click(object sender, EventArgs e)
        {
            try
            {
                Ajustes ajustes = nucleo.LeerAjustes();

                Process.Start("explorer", ajustes.CopiarA);
            }
            catch (Exception)
            {

            }
        }

        private void Principal_Resize(object sender, EventArgs e)
        {
            try
            {
                if (WindowState == FormWindowState.Minimized)
                {
                    //MostrarNotificacion("Ocultando SyncBD", "SyncBD trabajará en segundo plano, restaurelo en la barra de tareas", Properties.Resources.Info_32px, 33);

                    nicBarraTareas.Visible = true;
                    ShowInTaskbar = false;
                    Hide();
                }

                else if (WindowState == FormWindowState.Normal || WindowState == FormWindowState.Maximized)
                {
                    nicBarraTareas.Visible = false;
                }
            }
            catch (Exception)
            {

            }
        }

        private void nicBarraTareas_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Show();
                WindowState = FormWindowState.Normal;
                ShowInTaskbar = true;
            }
            catch (Exception)
            {
                
            }
        }

        private void btnRestaurarStrip_Click(object sender, EventArgs e)
        {
            try
            {
                Show();
                WindowState = FormWindowState.Normal;
                ShowInTaskbar = true;
                BringToFront();
            }
            catch (Exception)
            {

            }
        }

        private void btnSalirStrip_Click(object sender, EventArgs e)
        {
            try
            {
                FormClosingEventArgs args = new FormClosingEventArgs(CloseReason.UserClosing, false);

                Principal_FormClosing(sender, args);
            }
            catch (Exception)
            {

            }
        }

        private void btnMenuSalir_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                FormClosingEventArgs args = new FormClosingEventArgs(CloseReason.UserClosing, false);

                Principal_FormClosing(sender, args);
            }
            catch (Exception)
            {

            }
        }

        private void cbxApariencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxApariencia.SelectedIndex >= 0)
            {
                GuardarAjustes();
                
                UserLookAndFeel.Default.SkinName = nucleo.DeterminarTema(cbxApariencia.SelectedItem.ToString());
            }
        }

        private void btnSubBorrarHistorialRemoto_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show(UserLookAndFeel.Default, "Está acción es permanente y no se podrá cancelar.\n\n¿Realmente desea borrar el historial del servidor remoto?", "SyncBD | Confirme acción", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (enlace.BorrarHistorialRemoto())
                    {
                        CargarMovimientos();

                        MostrarNotificacion("Historial borrado", "Se ha borrado el historial del servidor Remoto", Properties.Resources.Ok_32px, 34);
                    }

                    else
                    {
                        CargarMovimientos();

                        MostrarNotificacion("No se borró historial", "No fue posible borrar el historial del servidor Remoto", Properties.Resources.Attention_32px, 35);
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnSubBorrarHistorialLocal_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show(UserLookAndFeel.Default, "Está acción es permanente y no se podrá cancelar.\n\n¿Realmente desea borrar el historial del servidor local?", "SyncBD | Confirme acción", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (enlace.BorrarHistorialLocal())
                    {
                        CargarMovimientos();

                        MostrarNotificacion("Historial borrado", "Se ha borrado el historial del servidor Local", Properties.Resources.Ok_32px, 36);
                    }

                    else
                    {
                        CargarMovimientos();

                        MostrarNotificacion("No se borró historial", "No fue posible borrar el historial del servidor Local", Properties.Resources.Attention_32px, 37);
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnAbortarRemoto_Click(object sender, EventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show(UserLookAndFeel.Default, "¿En verdad desea detener la transferencia actual?\n\nSe conservarán los archivos descargados", "SyncBD | Confirme acción", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    cancelarTransferenciaRemoto = true;
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnAbortarLocal_Click(object sender, EventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show(UserLookAndFeel.Default, "¿En verdad desea detener la transferencia actual?\n\nLos archivos cargados no se borrarán", "SyncBD | Confirme acción", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    cancelarTransferenciaLocal = true;
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnRemotoCancelarStrip_Click(object sender, EventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show(UserLookAndFeel.Default, "¿En verdad desea detener la transferencia actual?\n\nSe conservarán los archivos descargados", "SyncBD | Confirme acción", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    cancelarTransferenciaRemoto = true;
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnLocalCancelarStrip_Click(object sender, EventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show(UserLookAndFeel.Default, "¿En verdad desea detener la transferencia actual?\n\nLos archivos cargados no se borrarán", "SyncBD | Confirme acción", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    cancelarTransferenciaLocal = true;
                }
            }
            catch (Exception)
            {

            }
        }

        private void txtTiempoDesconexion_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtTiempoDesconexion.Text) <= 0)
                {
                    txtTiempoDesconexion.Text = "9999";

                    GuardarAjustes();
                }

                else
                {
                    GuardarAjustes();
                }
            }
            catch (Exception)
            {

            }
        }

        private void txtTiempoReconexion_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtTiempoReconexion.Text) <= 0)
                {
                    txtTiempoReconexion.Text = "3";

                    GuardarAjustes();
                }

                else
                {
                    GuardarAjustes();
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnConsultarManual_Click(object sender, EventArgs e)
        {
            try
            {
                VisorPDF mostrar = VisorPDF.ObtenerInstancia();

                Hide();

                mostrar.ShowDialog();

                Show();

                mostrar.Dispose();
            }
            catch (Exception)
            {
                
            }
        }

        private void btnCrearArchivoSoporte_Click(object sender, EventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show(UserLookAndFeel.Default, "¿Desea crear un archivo con información relevante para soporte técnico?", "SyncBD | Archivo para soporte técnico", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    fbdGuardarEn.Title = "Escoga una carpeta donde guardar archivo...";
                    fbdGuardarEn.Description = "Seleccione la carpeta donde se guardará el archivo para soporte...";
                    fbdGuardarEn.ShowNewFolderButton = true;

                    if (fbdGuardarEn.ShowDialog() == DialogResult.OK)
                    {
                        if(nucleo.CrearArchivoSoporte(fbdGuardarEn.SelectedPath))
                        {
                            XtraMessageBox.Show(UserLookAndFeel.Default, "El programa a creado el archivo para soporte técnico.\n\nRecuerde enviarlo al encargado que lo halla solicitado.", "SyncBD | Archivo para soporte creado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                        else
                        {
                            XtraMessageBox.Show(UserLookAndFeel.Default, "El programa no pudo crear el archivo para soporte técnico.\n\nSi el problema persiste, reinstale el programa", "SyncBD | Archivo para soporte no creado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error al crear archivo para soporte, Mensaje interno: " + ex.Message);

                XtraMessageBox.Show(UserLookAndFeel.Default, "El programa no pudo crear el archivo para soporte técnico.\n\nSi el problema persiste, reinstale el programa", "SyncBD | Archivo para soporte no creado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion


    }
}