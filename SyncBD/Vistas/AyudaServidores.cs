﻿using DevExpress.XtraEditors;
using System;
using System.Windows.Forms;

namespace SyncBD.Vistas
{
    public partial class AyudaServidores : XtraForm
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        private static AyudaServidores instancia;

        public static AyudaServidores ObtenerInstancia()
        {
            if (instancia == null || instancia.IsDisposed)
            {
                instancia = new AyudaServidores();
            }

            instancia.BringToFront();

            return instancia;
        }

        private AyudaServidores()
        {
            InitializeComponent();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}