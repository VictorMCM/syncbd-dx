﻿using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using Logger;
using SyncBD.Controladores;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SyncBD.Vistas
{
    public partial class MenuServidores : XtraForm
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        private static MenuServidores instancia;

        public static MenuServidores ObtenerInstancia()
        {
            if (instancia == null || instancia.IsDisposed)
            {
                instancia = new MenuServidores();
            }

            instancia.BringToFront();

            return instancia;
        }

        private MenuServidores()
        {
            InitializeComponent();
        }

        Nucleo nucleo = new Nucleo();
        ToLog log = new ToLog();

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            AyudaServidores mostrar = AyudaServidores.ObtenerInstancia();

            Hide();

            mostrar.ShowDialog();

            Show();

            mostrar.Dispose();

            mostrar.Close();
        }

        private void MenuServidores_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.WindowsShutDown || e.CloseReason == CloseReason.TaskManagerClosing)
            {
                if (XtraMessageBox.Show(UserLookAndFeel.Default, "¿Desea cancelar las configuraciones y cerrar el programa?\nRecuerde que el asistente empezará nuevamente al iniciar el programa", "SyncBD | Confirme cierre", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                {
                    e.Cancel = true;
                }

                else
                {
                    Application.Exit();
                }
            }
        }

        private void chkRemoto_CheckedChanged(object sender, EventArgs e)
        {
            if(chkRemoto.Checked)
            {
                chkLocal.Checked = false;
                chkAmbos.Checked = false;
            }
        }

        private void chkLocal_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLocal.Checked)
            {
                chkRemoto.Checked = false;
                chkAmbos.Checked = false;
            }
        }

        private void chkAmbos_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAmbos.Checked)
            {
                chkRemoto.Checked = false;
                chkLocal.Checked = false;
            }
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!chkRemoto.Checked && !chkLocal.Checked && !chkAmbos.Checked)
                {
                    XtraMessageBox.Show(UserLookAndFeel.Default, "Por favor seleccione al menos una opción para poder continuar con la configuración", "SyncBD | Se requiere configuración", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    chkRemoto.Focus();
                }

                else
                {
                    if (chkRemoto.Checked)
                    {
                        log.AppendLog("[Configuraciones] Mostrando ventana de configuración del servidor Remoto");

                        ConfigRemoto mostrar = ConfigRemoto.ObtenerInstancia();

                        Hide();

                        mostrar.ShowDialog();

                        if (ConfigRemoto.resultado == DialogResult.OK)
                        {
                            mostrar.Dispose();

                            FinAsistente finalizar = FinAsistente.ObtenerInstancia(FinAsistente.Visualizacion.Remoto);

                            finalizar.ShowDialog();
                        }

                        else
                        {
                            Show();

                            mostrar.Dispose();

                            XtraMessageBox.Show(UserLookAndFeel.Default, "Para poder continuar es necesario que configure al menos un servidor", "SyncBD | Se requiere configuración", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }

                    if (chkLocal.Checked)
                    {
                        log.AppendLog("[Configuraciones] Mostrando ventana de configuración del servidor Local");

                        ConfigLocal mostrar = ConfigLocal.ObtenerInstancia();

                        Hide();

                        mostrar.ShowDialog();

                        if (ConfigLocal.resultado == DialogResult.OK)
                        {
                            mostrar.Dispose();

                            FinAsistente finalizar = FinAsistente.ObtenerInstancia(FinAsistente.Visualizacion.Local);

                            finalizar.ShowDialog();
                        }

                        else
                        {
                            Show();

                            mostrar.Dispose();

                            XtraMessageBox.Show(UserLookAndFeel.Default, "Para poder continuar es necesario que configure al menos un servidor", "SyncBD | Se requiere configuración", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }

                    if (chkAmbos.Checked)
                    {
                        log.AppendLog("[Configuraciones] Mostrando ventanas de configuración");

                        ConfigRemoto mostrarRemoto = ConfigRemoto.ObtenerInstancia();

                        Hide();

                        DialogResult remoto;

                        DialogResult local;

                        mostrarRemoto.ShowDialog();

                        if (ConfigRemoto.resultado == DialogResult.OK)
                        {
                            remoto = DialogResult.OK;

                            mostrarRemoto.Dispose();
                        }

                        else
                        {
                            remoto = DialogResult.Cancel;

                            mostrarRemoto.Dispose();
                        }

                        ConfigLocal mostrarLocal = ConfigLocal.ObtenerInstancia();

                        mostrarLocal.ShowDialog();

                        if (ConfigLocal.resultado == DialogResult.OK)
                        {
                            local = DialogResult.OK;

                            mostrarLocal.Dispose();
                        }

                        else
                        {
                            local = DialogResult.Cancel;

                            mostrarLocal.Dispose();
                        }

                        if (local == DialogResult.OK || remoto == DialogResult.OK)
                        {
                            FinAsistente finalizar = FinAsistente.ObtenerInstancia(FinAsistente.Visualizacion.Ambos);

                            finalizar.ShowDialog();
                        }

                        else
                        {
                            Show();

                            log.AppendLog("[Configuraciones] El usuario no configuró ningún servidor, imposible continuar");

                            XtraMessageBox.Show(UserLookAndFeel.Default, "Para poder continuar es necesario que configure al menos un servidor", "SyncBD | Se requiere configuración", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x002, Mensaje interno: " + ex.Message);

                MostrarNotificacion("Error 0x002", "Por favor dirijase al manual de usuario para detalles", Properties.Resources.Cancel_32px, 0);
            }
        }

        private void MostrarNotificacion(string titulo, string texto, Image imagen, int toast)
        {
            try
            {
                if (nucleo.ObtenerVersionSO().Contains("WINDOWS 8") || nucleo.ObtenerVersionSO().Contains("WINDOWS 10"))
                {
                    try
                    {
                        toastMensaje.ShowNotification(toastMensaje.Notifications[toast]);
                    }
                    catch (Exception)
                    {
                        alertMensaje.Show(this, titulo, texto, "", imagen);
                    }
                }

                else
                {
                    alertMensaje.Show(this, titulo, texto, "", imagen);
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            try
            {
                FormClosingEventArgs args = new FormClosingEventArgs(CloseReason.UserClosing, false);

                MenuServidores_FormClosing(sender, args);
            }
            catch (Exception)
            {

            }
        }
    }
}