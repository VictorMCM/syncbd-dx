﻿namespace SyncBD.Vistas
{
    partial class AyudaConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AyudaConfig));
            this.pnlPrincipal = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cbxTopicos = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblTitulo = new DevExpress.XtraEditors.LabelControl();
            this.btnRegresar = new DevExpress.XtraEditors.SimpleButton();
            this.lblCuerpo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pnlIzquierdo = new DevExpress.XtraEditors.PanelControl();
            this.petLogo = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPrincipal)).BeginInit();
            this.pnlPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxTopicos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlIzquierdo)).BeginInit();
            this.pnlIzquierdo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.petLogo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlPrincipal
            // 
            this.pnlPrincipal.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlPrincipal.Controls.Add(this.labelControl4);
            this.pnlPrincipal.Controls.Add(this.cbxTopicos);
            this.pnlPrincipal.Controls.Add(this.lblTitulo);
            this.pnlPrincipal.Controls.Add(this.btnRegresar);
            this.pnlPrincipal.Controls.Add(this.lblCuerpo);
            this.pnlPrincipal.Controls.Add(this.labelControl3);
            this.pnlPrincipal.Controls.Add(this.labelControl1);
            this.pnlPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPrincipal.Location = new System.Drawing.Point(180, 0);
            this.pnlPrincipal.Name = "pnlPrincipal";
            this.pnlPrincipal.Size = new System.Drawing.Size(604, 561);
            this.pnlPrincipal.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(10, 153);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(83, 19);
            this.labelControl4.TabIndex = 23;
            this.labelControl4.Text = "Detalles de:";
            // 
            // cbxTopicos
            // 
            this.cbxTopicos.EditValue = "Servidor";
            this.cbxTopicos.Location = new System.Drawing.Point(99, 150);
            this.cbxTopicos.Name = "cbxTopicos";
            this.cbxTopicos.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbxTopicos.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.cbxTopicos.Properties.Appearance.Options.UseFont = true;
            this.cbxTopicos.Properties.Appearance.Options.UseForeColor = true;
            this.cbxTopicos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxTopicos.Properties.Items.AddRange(new object[] {
            "Servidor",
            "Usuario y/o contraseña",
            "Puerto",
            "Directorio",
            "Encriptación",
            "Autenticación",
            "Modo activo",
            "Protocolo"});
            this.cbxTopicos.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxTopicos.Size = new System.Drawing.Size(224, 26);
            this.cbxTopicos.TabIndex = 1;
            this.cbxTopicos.SelectedIndexChanged += new System.EventHandler(this.cbxProtocolo_SelectedIndexChanged);
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Underline);
            this.lblTitulo.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblTitulo.Appearance.Options.UseFont = true;
            this.lblTitulo.Appearance.Options.UseForeColor = true;
            this.lblTitulo.Location = new System.Drawing.Point(10, 213);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(178, 19);
            this.lblTitulo.TabIndex = 15;
            this.lblTitulo.Text = "Información de: Servidor";
            // 
            // btnRegresar
            // 
            this.btnRegresar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRegresar.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnRegresar.Appearance.Options.UseFont = true;
            this.btnRegresar.ImageOptions.Image = global::SyncBD.Properties.Resources.Back_16px;
            this.btnRegresar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnRegresar.Location = new System.Drawing.Point(455, 510);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(139, 39);
            this.btnRegresar.TabIndex = 2;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // lblCuerpo
            // 
            this.lblCuerpo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCuerpo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblCuerpo.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblCuerpo.Appearance.Options.UseFont = true;
            this.lblCuerpo.Appearance.Options.UseForeColor = true;
            this.lblCuerpo.Location = new System.Drawing.Point(10, 238);
            this.lblCuerpo.Name = "lblCuerpo";
            this.lblCuerpo.Size = new System.Drawing.Size(527, 133);
            this.lblCuerpo.TabIndex = 11;
            this.lblCuerpo.Text = resources.GetString("lblCuerpo.Text");
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(10, 87);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(583, 38);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "Si tiene algún problema para configurar su servidor de datos o no conoce\r\nalguna " +
    "de las funciones, seleccione un tópico de la lista para mostrar más detalles:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(10, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(427, 29);
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "Ayuda para la configuración del servidor";
            // 
            // pnlIzquierdo
            // 
            this.pnlIzquierdo.Controls.Add(this.petLogo);
            this.pnlIzquierdo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlIzquierdo.Location = new System.Drawing.Point(0, 0);
            this.pnlIzquierdo.Name = "pnlIzquierdo";
            this.pnlIzquierdo.Size = new System.Drawing.Size(180, 561);
            this.pnlIzquierdo.TabIndex = 4;
            // 
            // petLogo
            // 
            this.petLogo.Cursor = System.Windows.Forms.Cursors.Default;
            this.petLogo.EditValue = global::SyncBD.Properties.Resources.SyncBD;
            this.petLogo.Location = new System.Drawing.Point(5, 37);
            this.petLogo.Name = "petLogo";
            this.petLogo.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.petLogo.Properties.Appearance.Options.UseBackColor = true;
            this.petLogo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.petLogo.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.petLogo.Properties.ShowMenu = false;
            this.petLogo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.petLogo.Size = new System.Drawing.Size(170, 96);
            this.petLogo.TabIndex = 0;
            // 
            // AyudaConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pnlPrincipal);
            this.Controls.Add(this.pnlIzquierdo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "AyudaConfig";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SyncBD - Asistente de configuración";
            ((System.ComponentModel.ISupportInitialize)(this.pnlPrincipal)).EndInit();
            this.pnlPrincipal.ResumeLayout(false);
            this.pnlPrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxTopicos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlIzquierdo)).EndInit();
            this.pnlIzquierdo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.petLogo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlPrincipal;
        private DevExpress.XtraEditors.LabelControl lblTitulo;
        private DevExpress.XtraEditors.SimpleButton btnRegresar;
        private DevExpress.XtraEditors.LabelControl lblCuerpo;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl pnlIzquierdo;
        private DevExpress.XtraEditors.PictureEdit petLogo;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit cbxTopicos;
    }
}