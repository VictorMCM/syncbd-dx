﻿using DevExpress.XtraEditors;
using Logger;
using SyncBD.Controladores;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SyncBD.Vistas
{
    public partial class VisorPDF : XtraForm
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        private static VisorPDF instancia;

        public static VisorPDF ObtenerInstancia()
        {
            if (instancia == null || instancia.IsDisposed)
            {
                instancia = new VisorPDF();
            }

            instancia.BringToFront();

            return instancia;
        }

        private VisorPDF()
        {
            InitializeComponent();
        }

        Nucleo nucleo = new Nucleo();

        ToLog log = new ToLog();

        /// <summary>
        /// Muestra una notificacion dependiendo el Sistema Operativo utilizado
        /// </summary>
        /// <param name="titulo">Titulo que tendrá la notificacion</param>
        /// <param name="texto">Texto o cuerpo de la notificación</param>
        /// <param name="imagen">Imagen que se pondrá en la notificación, null si no se desea poner ninguna (Cambia según sistema operativo)</param>
        /// <param name="toast">Número de la notificación del toastnotificactionmanager</param>
        private void MostrarNotificacion(string titulo, string texto, Image imagen, int toast)
        {
            try
            {
                if (nucleo.ObtenerVersionSO().Contains("WINDOWS 8") || nucleo.ObtenerVersionSO().Contains("WINDOWS 10"))
                {
                    if (toast == 33)
                    {
                        alertMensaje.Show(this, titulo, texto, "", imagen);
                    }

                    else
                    {
                        try
                        {
                            toastMensaje.ShowNotification(toastMensaje.Notifications[toast]);
                        }
                        catch (Exception)
                        {
                            alertMensaje.Show(this, titulo, texto, "", imagen);
                        }
                    }

                }

                else
                {
                    alertMensaje.Show(this, titulo, texto, "", imagen);
                }
            }
            catch (Exception)
            {

            }
        }

        private void VisorPDF_Load(object sender, EventArgs e)
        {
            try
            {
                if (nucleo.ComprobarManual() != null)
                {
                    pdfVisor.LoadDocument(nucleo.ComprobarManual());
                }

                else
                {
                    MostrarNotificacion("No se detectó manual", "Al parecer no existe el manual de usuario", Properties.Resources.Attention_32px, 40);

                    Close();
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x016, Mensaje interno: " + ex.Message);

                MostrarNotificacion("No se detectó manual", "Al parecer no existe el manual de usuario", Properties.Resources.Cancel_32px, 41);

                Close();
            }
        }
    }
}