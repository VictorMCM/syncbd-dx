CREATE TABLE MovimientosLocal
(
	Movimiento integer primary key autoincrement,
	Fecha Date not null,
	Archivos integer not null,
	Desde text not null,
	Hacia text not null,
	Duracion text not null,
	Estado text not null
);

CREATE TABLE MovimientosRemoto
( 
	Movimiento integer primary key autoincrement,
	Fecha Date not null,
	Archivos integer not null,
	Desde text not null,
	Hacia text not null,
	Duracion text not null,
	Estado text not null
);